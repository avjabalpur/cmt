﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using cmtApp.Entity;
using cmtApp.Common;


namespace cmtApp.Mappers
{
  public class Mapper
  {
      public CustomerPo MapCustomer(SqlDataReader reader)
      {
          return new CustomerPo
          {
              ARDocId = Utility.GetGuidData(reader, 0),
              DocType = Utility.GetStringData(reader, 1),
              DocNo = Utility.GetIntegerData(reader, 2),
              CustNo = Utility.GetStringData(reader, 3),
              CustId = Utility.GetGuidData(reader, 4),
              CustName = Utility.GetStringData(reader, 5),
              TermsCode = Utility.GetStringData(reader, 6),
              CustomerPONo = Utility.GetStringData(reader, 7),
              InvoiceDate = Utility.GetDateTimeData(reader, 8),
              DueDate = Utility.GetDateTimeData(reader, 9),
              OriginalBalance = Utility.GetDecimalData(reader, 10),
              CurrentBalance = Utility.GetDecimalData(reader, 11),
              AvailablCmtscount = Utility.GetDecimalData(reader, 12),
              Currency = Utility.GetStringData(reader, 13),
              TradingPartnerId = Utility.GetStringData(reader, 14)
          };
      }
  }
}