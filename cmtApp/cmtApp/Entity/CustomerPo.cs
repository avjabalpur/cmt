﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cmtApp.Entity
{
    public class CustomerPo
    {
        public Guid ARDocId { get; set; }
        public string DocType { get; set; }
        public int DocNo { get; set; }
        public string CustNo { get; set; }
        public Guid CustId { get; set; }
        public string CustName { get; set; }
        public string TermsCode { get; set; }
        public string CustomerPONo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }
        public Decimal OriginalBalance { get; set; }
        public Decimal CurrentBalance { get; set; }
        public Decimal AvailablCmtscount { get; set; }
        public string Currency { get; set; }
        public string TradingPartnerId { get; set; }
        
    }
}

