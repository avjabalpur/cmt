﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cmtApp.Entity
{
    public class Price
    {

        public string itemNumber { get; set; }

        public string customerNumber { get; set; }

        public string shipToNumber { get; set; }

        public string whseCode { get; set; }

        public int qty { get; set; }

        public double customerPrice { get; set; }

        public string procNo { get; set; }
    }
}