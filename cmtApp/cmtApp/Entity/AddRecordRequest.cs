﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cmtApp.Entity
{

    public class AddCmtRecordRequest
    {
        public List<AddRecordRequest> AddRecordRequest { get; set; }
    }

    public class AddRecordRequest
    {
        public string TableName { get; set; }
        public List<AddRecordColumns> AddRecordColumns { get; set; }
    }

    public class AddRecordColumns
    {
        public string ColumnName { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }
}