﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cmtApp.Entity
{
    public class BillTo
    {
        public string CustomerID { get; set; }

        public string BillToName { get; set; }

        public string BillToAddress1 { get; set; }

        public string BillToAddress2 { get; set; }

        public string BillToZipCodeId { get; set; }

        public string BillToZip4 { get; set; }

        public string BillToTelNo { get; set; }

        public string BillToFaxNo { get; set; }

        public string BillToWebAddress { get; set; }

        public string BillToEmailAddress { get; set; }

        public ContactPerson ContactPerson { get; set; }

        public Address Address { get; set; }
    }


    public class ContactPerson
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

    }


    public class Address
    {
        public string ExternalAddressId { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string AddressRemarks { get; set; }
            
    }
}