﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cmtApp.Entity
{
    public class ShipToDept
    {
        public string ShipToID { get; set; }

        public string ShipToDeptID { get; set; }

        public string Dept { get; set; }

        public string Description { get; set; }

        public string Attn { get; set; }

        public string TelNo { get; set; }

        public string FaxNo { get; set; }

        public string IsMailed { get; set; }

        public string EmailAddress { get; set; }

    }
}