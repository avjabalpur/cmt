﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cmtApp.Entity
{
    public class OrderLine
    {

        public Guid OEDocDtlId { get; set; }

        public Guid OEDocID { get; set; }

        public Guid DeptId { get; set; }

        public string CustPO { get; set; }

        public DateTime ShipByDate { get; set; }

        public string WebProductId { get; set; }

        public string ItemNumber { get; set; }

        public string ItemDescription { get; set; }

        public int SortOrder { get; set; }

        public Guid ItemCustomerNumber { get; set; }

        public decimal Quantity { get; set; }

        public string QtyUM { get; set; }

        public decimal QtyConv { get; set; }

        public string AmtUM { get; set; }

        public decimal AmtConv { get; set; }

        public decimal ProductPrice { get; set; }

        public string ProductPriceCurrency { get; set; }

        public string UploadStatus { get; set; }
    }
}