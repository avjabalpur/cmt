﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cmtApp.Entity
{
    public class OrderHeader
    {
        public string WebUserId { get; set; }
        public Guid OEDocID { get; set; }
        public Guid CustomerNumber { get; set; }
        public string ShipToID { get; set; }
        public string CustDeptID { get; set; }
        public string WhseId { get; set; }
        public string ReleaseNo { get; set; }
        public DateTime OrderTime { get; set; }
        public string OrderType { get; set; }
        public string OrderNote { get; set; }
        public string DeliveryType { get; set; }
        public string SpecialInstructions { get; set; }
        public string PaymentType { get; set; }
        public decimal OrderCost { get; set; }
        public string OrderCurrency { get; set; }
        public decimal CarriageCost { get; set; }
        public decimal SalesTax { get; set; }
        public string PaypalToken { get; set; }
        public decimal TotalItemQuantity { get; set; }
        public string ShipComplete { get; set; }
        public string TaxExampt { get; set; }
        public string Cpo { get; set; }
        public string ShippingChargeType { get; set; }
        public string CollectAccount { get; set; }
        public string ShippingMethod { get; set; }
        public string HazardMaterial { get; set; }
        public string OversizeProduct { get; set; }
        public string ServiceCode { get; set; }
        public string UploadStatus { get; set; }
            
    }

    public class orderXmlRequest
    {
        public string XmlRequest { get; set; }
    }
}

                                           