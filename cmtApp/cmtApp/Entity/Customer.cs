﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cmtApp.Entity
{
    public class Customer
    {

		public string TemplateCustomerID { get; set; }

        public string TemplateCustNo { get; set; }

        public string CustName { get; set; }

        public string XRefName { get; set; }

        public string CustomerNumber { get; set; }

        public string CustomerID { get; set; }

        public string ShipToID { get; set; }

        public string ShipToDeptID { get; set; }
                
        public Ship Ship { get; set; }

        public ShipToDept ShipToDept { get; set; }

        public BillTo BillTo { get; set; }

        public string ErrorMessage { get; set; }
    }
}
