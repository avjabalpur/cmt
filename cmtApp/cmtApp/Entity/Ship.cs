﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cmtApp.Entity
{

    /// <remarks/>
    public class Ship
    {

        public string TemplateCustomerID { get; set; }

        public string CustId { get; set; }

        public string TemplateCustNo { get; set; }

        public string ShipToID { get; set; }

        public string ShipNo { get; set; }

        public string ShipToName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string ZipCodeDtlId { get; set; }

        public string Zip4 { get; set; }

        public string TelNo { get; set; }

        public string FaxNo { get; set; }

        public string WebAddress { get; set; }

        public string TaxExemptNo { get; set; }

        public string Comments { get; set; }

        public string EmailAddress { get; set; }

        public string FedExAcct { get; set; }

        public string UpsAcct { get; set; }

        public string DefaultCarrier { get; set; }

        public string DefulatCarrierServiceCode { get; set; }

        public string ErrorMessage { get; set; }

        public ShipToDept ShipToDept { get; set; }


        public ContactPerson ContactPerson { get;  set; }
        public Address Address { get; set; }
    }


}
