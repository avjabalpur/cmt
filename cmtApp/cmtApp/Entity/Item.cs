﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cmtApp.Entity
{
    /// <remarks/>
    public class Item
    {
        public string whseCode { get; set; }

        public string ItemNo { get; set; }

        public string qtyAvailable { get; set; }
    }
}