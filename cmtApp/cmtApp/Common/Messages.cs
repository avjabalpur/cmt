﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cmtApp.Common
{
    public class Messages
    {
        public struct LoginMessages
        {
            public const string UserNotFound = "Invalid user Name";
            public const string PasswordNotMatch = "Password does not match";
            public const string InvalidLoginKey = "Please give the proper user name and password";
            public const string InvalidUserName = "Please give the proper user name";
            public const string InvalidPassword = "Please give the porper password";
            public const string InvalidUserNameAndPassword = "The combination of user name and password does not match";
        }
    }
}