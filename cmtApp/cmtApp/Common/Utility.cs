﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using cmtApp.Entity;
using System.Linq;
using System.IO;

namespace cmtApp.Common
{
  /// <summary>
  /// 
  /// </summary>
  public class Utility
  {
    public static SqlConnection GetConnection()
    {
      return new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
    }

    public static Guid ConvertStringToGuid(string guid)
    {
      Guid id;
      Guid.TryParse(guid, out id);
      return id;
    }

    public static string DecodeString(string str)
    {
      try
      {
        return Encoding.UTF8.GetString(Convert.FromBase64String(str));
      }
      catch (Exception)
      {
        throw;
      }
    }

    #region mapDataFunction
    public static string GetStringData(SqlDataReader reader, int columnNumber)
    {
      return (!reader.IsDBNull(columnNumber)) ? reader.GetString(columnNumber).Trim() : string.Empty;
    }

    public static char GetCharData(SqlDataReader reader, int columnNumber)
    {
      return (!reader.IsDBNull(columnNumber)) ? reader.GetChar(columnNumber) : default(char);
    }

    public static DateTime GetDateTimeData(SqlDataReader reader, int columnNumber)
    {
      return (!reader.IsDBNull(columnNumber)) ? reader.GetDateTime(columnNumber) : default(DateTime);
    }

    public static int GetIntegerData(SqlDataReader reader, int columnNumber)
    {
      return (!reader.IsDBNull(columnNumber)) ? reader.GetInt32(columnNumber) : default(Int32);
    }

    public static short GetSortData(SqlDataReader reader, int columnNumber)
    {
      return (!reader.IsDBNull(columnNumber)) ? reader.GetInt16(columnNumber) : default(Int16);
    }

    public static decimal GetDecimalData(SqlDataReader reader, int columnNumber)
    {
      return (!reader.IsDBNull(columnNumber)) ? reader.GetDecimal(columnNumber) : default(Decimal);
    }

    public static Guid GetGuidData(SqlDataReader reader, int columnNumber)
    {
      return (!reader.IsDBNull(columnNumber)) ? reader.GetGuid(columnNumber) : default(Guid);
    }

    public static byte[] GetByteData(SqlDataReader reader, int columnNumber)
    {
      return (byte[])((!reader.IsDBNull(columnNumber)) ? reader.GetValue(columnNumber) : default(byte[]));
    }

    public static bool GetBoolData(SqlDataReader reader, int columnNumber)
    {
      return (!reader.IsDBNull(columnNumber)) ? reader.GetBoolean(columnNumber) : default(Boolean);
    }

    public static DateTime? checkDefaultDateTime(DateTime? dateTime)
    {
      return (dateTime != new DateTime()) ? dateTime : null;
    }

    public static Guid? checkDefaultGuid(Guid? guid)
    {
      return (guid != new Guid()) ? guid : null;
    }

    #endregion


    public List<Table> GetTableColumns()
    {
      var tabs = new List<Table>();
      var tables = getTablesName();
      foreach (var table in tables)
      {
        var tab = new Table();
        tab.TableName = table;
        tab.Columns = new List<Column>();
        using (var con = GetConnection())
        {
          con.Open();
          //var command = new SqlCommand("SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + table + "'", con);
          var command = new SqlCommand("SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + table + "' AND DATA_TYPE <> 'uniqueidentifier'", con);
          using (var reader = command.ExecuteReader())
          {
            while (reader.Read())
            {
              tab.Columns.Add(new Column
              {
                Name = reader.GetString(0),
                Type = reader.GetString(1),
                IsNullable = (reader.GetString(2).ToString() == "YES") ? true : false
              });
            }
          }
        }
        tabs.Add(tab);
      }

      return tabs;
    }

    private List<string> getTablesName()
    {
      return new List<string> { 
                "IT_DOC_HDR",
                "IT_DOC_DTL",
                "IT_DOC_SHIPTO"
            };
    }

    public bool SaveCmtDetails(AddCmtRecordRequest addCmtRecordRequest)
    {
      //var guid = new Guid();
      if (addCmtRecordRequest == null)
      {
        throw new Exception("Request cannot be null");
      }
      bool hasParantTable = false;
      foreach (var item in addCmtRecordRequest.AddRecordRequest)
      {
        if (item.TableName == "IT_DOC_HDR")
        {
          hasParantTable = true;
        }
      }
      if (!hasParantTable)
      {
        throw new Exception("Parant table value IT_DOC_HDR cannot be null");
      }
      Guid docId = Guid.NewGuid();
      StringBuilder sqlCommand = new StringBuilder();
      foreach (var addRecordRequest in addCmtRecordRequest.AddRecordRequest)
      {
        if (addRecordRequest.TableName == "IT_DOC_HDR")
        {
          sqlCommand = new StringBuilder();
          var fields = getFieldToBeAdd(getRequiredField(addRecordRequest.TableName), addRecordRequest.AddRecordColumns);
          sqlCommand.Append("insert IT_DOC_HDR (IT_DOC_ID, ");
          sqlCommand.Append(getColumnsForInsert(fields));
          sqlCommand.Append(" ) Values ('" + docId + "', ");
          sqlCommand.Append(getValueForInsert(fields));
          sqlCommand.Append(" )");
        }
        else if (addRecordRequest.TableName == "IT_DOC_DTL")
        {
          sqlCommand = new StringBuilder();
          Guid docDtlId = Guid.NewGuid();
          var fields = getFieldToBeAdd(getRequiredField(addRecordRequest.TableName), addRecordRequest.AddRecordColumns);
          sqlCommand.Append("insert IT_DOC_DTL (IT_DOC_DTL_ID , IT_DOC_ID, ");
          sqlCommand.Append(getColumnsForInsert(fields));
          sqlCommand.Append(" ) Values ('" + docDtlId + "', '" + docId + "',");
          sqlCommand.Append(getValueForInsert(fields));
          sqlCommand.Append(" )");
        }

        else if (addRecordRequest.TableName == "IT_DOC_SHIPTO")
        {
          sqlCommand = new StringBuilder();
          Guid docDtlId = Guid.NewGuid();
          var fields = getFieldToBeAdd(getRequiredField(addRecordRequest.TableName), addRecordRequest.AddRecordColumns);
          sqlCommand.Append("insert IT_DOC_SHIPTO (IT_DOC_ID, ");
          sqlCommand.Append(getColumnsForInsert(fields));
          sqlCommand.Append(" ) Values ('" + docId + "', ");
          sqlCommand.Append(getValueForInsert(fields));
          sqlCommand.Append(" )");
        }

        using (var con = GetConnection())
        {
          con.Open();
          var command = new SqlCommand(sqlCommand.ToString(), con);
          command.ExecuteNonQuery();
        }
      }
      return true;
    }

    private string getValueForInsert(List<AddRecordColumns> fields)
    {
      var values = string.Empty;
      foreach (var field in fields)
      {
        values = values + "  '" + field.Value + "', ";
      }
      return values.Substring(0, values.Length - 2);
    }

    private string getColumnsForInsert(List<AddRecordColumns> fields)
    {
      try
      {
        var column = string.Empty;
        foreach (var field in fields)
        {
          if (!string.IsNullOrEmpty(field.Type))
          {
            if (!isValidValueForColumn(field.Value, field.Type))
            {
              throw new Exception("There are error with column " + field.ColumnName + " and Value " + field.Value + " please give " + field.Type + "Value and try again");
            }
          }

          column = column + "  " + field.ColumnName + ", ";

        }
        return column.Substring(0, column.Length - 2);
      }
      catch (Exception)
      {
        throw;
      }
    }

    private bool isValidValueForColumn(string value, string type)
    {
      object o = value;
      return (o.GetType() == convertSqlTypeToCSharp(type));
      //return true;
    }

    private Type convertSqlTypeToCSharp(string type)
    {
      switch (type.ToLower())
      {
        case "binary":
        case "image":
        case "sql_variant":
          return typeof(byte[]);
        case "tinyint":
          return typeof(byte);
        case "bigint":
        case "int":
          return typeof(int);
        case "bit":
          return typeof(bool);
        case "char":
        case "nchar":
          return typeof(char);

        case "ntext":
        case "nvarchar":
        case "text":
        case "varchar":
        case "xml":
          return typeof(string);
        case "datetime":
        case "smalldatetime":
          return typeof(DateTime);
        case "decimal":
        case "money":
        case "numeric":
        case "real":
          return typeof(decimal);
        case "float":
        case "smallmoney":
          return typeof(float);
        case "smallint":
          return typeof(short);
        case "uniqueidentifier":
          return typeof(Guid);
        default:
          throw (new Exception("Invalid SQL Server data type specified"));
      }
    }

    private List<AddRecordColumns> getFieldToBeAdd(Dictionary<string, string> requiredFields, List<AddRecordColumns> columns)
    {
      foreach (var requiredField in requiredFields)
      {
        var requiredColumn = new AddRecordColumns
        {
          ColumnName = requiredField.Key,
          Value = requiredField.Value
        };
        var col = columns.Find(x => x.ColumnName == requiredColumn.ColumnName);
        if (col == null)
        {
          columns.Add(requiredColumn);
        }
      }
      return columns;
    }

    private Dictionary<string, string> getRequiredField(string tableName)
    {
      var defaultField = new Dictionary<string, string>();
      switch (tableName)
      {
        case "IT_DOC_HDR":
          defaultField.Add("DOC_CODE", "E");
          defaultField.Add("DOC_DATE", DateTime.Now.ToString());
          defaultField.Add("STATUS_ID", "C");

          break;
        case "IT_DOC_DTL":
          // defaultField.Add("IT_DOC_ID", "");
          defaultField.Add("SORT_ORDER", "1");
          break;

        case "IT_DOC_SHIPTO":

          defaultField.Add("SHIP_NO", "Amazon");
          defaultField.Add("SHIP_NAME", "Jeffrey Lim");
          defaultField.Add("ZIP_CODE_DTL_ID", "F88BD47A-C495-454A-A7F6-F3626214B34B");
          break;
      }
      return defaultField;
    }

    private static string GetCsType(string column)
    {
      switch (column.ToLower())
      {
        case "binary":
          return "byte[]";
        case "bigint":
          return "long";
        case "bit":
          return "bool";
        case "char":
          return "string";
        case "datetime":
          return "DateTime";
        case "decimal":
          return "decimal";
        case "float":
          return "float";
        case "image":
          return "byte[]";
        case "int":
          return "int";
        case "money":
          return "decimal";
        case "nchar":
          return "string";
        case "ntext":
          return "string";
        case "nvarchar":
          return "string";
        case "numeric":
          return "decimal";
        case "real":
          return "decimal";
        case "smalldatetime":
          return "DateTime";
        case "smallint":
          return "short";
        case "smallmoney":
          return "float";
        case "sql_variant":
          return "byte[]";
        case "sysname":
          return "string";
        case "text":
          return "string";
        case "timestamp":
          return "DateTime";
        case "tinyint":
          return "byte";
        case "varbinary":
          return "byte[]";
        case "varchar":
          return "string";
        case "uniqueidentifier":
          return "Guid";
        case "xml":
          return "string";
        default:  // Unknow data type
          throw (new Exception("Invalid SQL Server data type specified: " + column));
      }
    }

    public static bool CreateOutPutFile(string fileContent, string fileName)
    {
        var path = ConfigurationManager.AppSettings["myCmtPath"];
        var combinPath = Path.Combine(path, fileName);
        var fs = new FileStream(combinPath, FileMode.Append, FileAccess.Write);
        var sw = new StreamWriter(fs);
        sw.Write(fileContent);
        sw.Flush();
        sw.Close();
        return true;

    }

    /// <summary>
    /// need to put this call in window service
    /// </summary>
    public static string ReadCmtContientFromFile()
    {
        var path = ConfigurationManager.AppSettings["myPatnerPath"];
        var fs = new FileStream(path + "\\850.Cmt", FileMode.Open, FileAccess.Read);
        var sr = new StreamReader(fs);
        sr.BaseStream.Seek(0, SeekOrigin.Begin);
        return sr.ReadToEnd();
    }
  }
}