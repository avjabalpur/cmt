﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace cmtApp.Common
{
    public class CmtSqlClass
    {
        
        public static SqlConnection GetConnection()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public static bool ExecuteProcedure(string procedureName, Dictionary<string, string> sqlParams)
        {
            using (var con = GetConnection())
            {
                con.Open();
                var command = new SqlCommand(procedureName, con);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                if (sqlParams.Count > 0)
                {
                    foreach (var sqlParam in sqlParams)
                    {
                        command.Parameters.AddWithValue("@" + sqlParam.Key, sqlParam.Value);
                    }
                }
                return command.ExecuteNonQuery() > 0;
            }
        }

        public static DataSet ExecuteProcedureWithReturnDataSet(string procedureName, Dictionary<string, string> sqlParams)
        {
            var ds = new DataSet();
            using (var con = GetConnection())
            {
                var command = new SqlCommand(procedureName, con);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                if (sqlParams.Count > 0)
                {
                    foreach (var sqlParam in sqlParams)
                    {
                        command.Parameters.AddWithValue("@" + sqlParam.Key, sqlParam.Value);
                    }
                }
                var da = new SqlDataAdapter(command);
                da.Fill(ds);
                return ds;

            }
        }
    }
}