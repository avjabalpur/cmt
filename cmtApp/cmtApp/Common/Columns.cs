﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cmtApp.Common
{
    public class Table
    {
        public string TableName { get; set; }
        public List<Column> Columns { get; set; }
    }

    public class Column
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public bool IsNullable { get; set; }
    }
}
