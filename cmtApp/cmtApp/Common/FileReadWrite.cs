﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace cmtApp.Common
{
    public class FileReadWrite
    {

        public static string ReadCmtContientFromFile(string path)
        {
            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                var sr = new StreamReader(fs);
                sr.BaseStream.Seek(0, SeekOrigin.Begin);
                return sr.ReadToEnd();
            }
        }

        public static void WriteCmtContientFromFile(string path, string CmtResponse)
        {
            using (var fs = new FileStream(path, FileMode.Create))
            {
                var sw = new StreamWriter(fs);
                sw.Write(CmtResponse);
                sw.Flush();
                sw.Close();
            }
        }

        public static string ReadContientFromCSV(string path) {

            return "";
        }
    }
}