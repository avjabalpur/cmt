﻿using cmtApp.Entity;
using cmtApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;

namespace cmtApp.Controllers
{
    [RoutePrefix("api/cmt")]
    public class CmtController : ApiController
    {
        CmtModel cmtClass = new CmtModel();

        [Route("test")]
        [HttpGet]
        public Customer GetCustomer()
        {
            try
            {
                return new Customer();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Route("customer")]
        [HttpPost]
        public Customer SaveCustomer(Customer customer)
        {
            try
            {

                return cmtClass.InsertNewCustomer(customer);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Route("ship")]
        [HttpPost]
        public Ship SaveShipTo(Ship ship)
        {
            try
            {
                return cmtClass.InsertNewShipTo(ship);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Route("pricing/{itemNumber}/{customerNumber}/{shipToNumber}/{whseCode}/{qty}")]
        [HttpPost]
        public Price GetPricing(string itemNumber, string customerNumber, string shipToNumber, string whseCode, int qty)
        {
            try
            {
                //p_cds_oe_pricing_engine_wsAPI 
                return cmtClass.getPricingIntegration(itemNumber, customerNumber, shipToNumber, whseCode, qty);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        [Route("stock/{itemNo}")]
        [HttpPost]
        public List<Item> GetwerhouseStockQty(int itemNo)
        {
            try
            {
                //p_cds_oe_pricing_engine_wsAPI 
                return cmtClass.getwerhouseStockQty(itemNo);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        //http://localhost:51214/api/SalesOrder
        [Route("sales-order")]
        [HttpPost]
        public OrderHeader postSalesOrderInsert(orderXmlRequest xmlRequest)
        {
            var xmlString = xmlRequest.XmlRequest;

            try
            {

                XDocument doc = XDocument.Parse(xmlString);

                OrderHeader orderHead = new OrderHeader();
                if (doc.Root != null)
                {
                    orderHead = (from r in doc.Root.Elements("order_header")
                                 select new OrderHeader
                                 {
                                     WebUserId = (string)r.Element("web_user_id"),
                                     CustomerNumber = (!string.IsNullOrEmpty((string)r.Element("customer_number"))) ? (Guid)r.Element("customer_number") : new Guid(),
                                     OrderTime = (!string.IsNullOrEmpty((string)r.Element("order_time"))) ? (DateTime)r.Element("order_time") : new DateTime(),
                                     OrderType = (string)r.Element("order_type"),
                                     OrderNote = (string)r.Element("order_note"),
                                     DeliveryType = (string)r.Element("delivery_type"),
                                     SpecialInstructions = (string)r.Element("special_instructions"),
                                     PaymentType = (string)r.Element("payment_type"),
                                     OrderCost = (!string.IsNullOrEmpty((string)r.Element("order_cost"))) ? (decimal)r.Element("order_cost") : new decimal(),
                                     OrderCurrency = (string)r.Element("order_currency"),
                                     CarriageCost = (!string.IsNullOrEmpty((string)r.Element("carriage_cost"))) ? (decimal)r.Element("carriage_cost") : new decimal(),
                                     SalesTax = (!string.IsNullOrEmpty((string)r.Element("sales_tax"))) ? (decimal)r.Element("sales_tax") : new decimal(),
                                     PaypalToken = (string)r.Element("paypal_token"),
                                     TotalItemQuantity = (!string.IsNullOrEmpty((string)r.Element("total_item_quantity"))) ? (decimal)r.Element("total_item_quantity") : new decimal(),
                                     ShipComplete = (string)r.Element("ship_complete"),
                                     TaxExampt = (string)r.Element("tax_exampt"),
                                     Cpo = (string)r.Element("cpo"),
                                     ShippingChargeType = (string)r.Element("shipping_charge_type"),
                                     CollectAccount = (string)r.Element("collect_account"),
                                     ShippingMethod = (string)r.Element("shipping_method"),
                                     HazardMaterial = (string)r.Element("hazard_material"),
                                     OversizeProduct = (string)r.Element("oversize_product")
                                 }).First();
                }

                /* Code Need to be refactor */


                var xElement = doc.Root.Elements("billto");
                BillTo billTo = new BillTo();

                if (xElement != null)
                    foreach (var child in xElement.Elements())
                    {

                        if (child.Name.LocalName == "contact_person")
                        {
                            var billto = XDocument.Parse(child.ToString());

                            billTo.ContactPerson = (from r in billto.Elements("contact_person")
                                                    select new ContactPerson
                                                    {
                                                        FirstName = (string)r.Element("first_name"),
                                                        LastName = (string)r.Element("last_name"),
                                                        FullName = (string)r.Element("full_name"),
                                                        Email = (string)r.Element("email"),
                                                        Phone = (string)r.Element("phone"),
                                                        Fax = (string)r.Element("fax")

                                                    }).First();
                        }

                        if (child.Name.LocalName == "address")
                        {
                            var billto = XDocument.Parse(child.ToString());

                            billTo.Address = (from r in billto.Elements("address")
                                              select new Address
                                              {
                                                  ExternalAddressId = (string)r.Element("external_address_id"),
                                                  AddressLine1 = (string)r.Element("address_line_1"),
                                                  AddressLine2 = (string)r.Element("address_line_2"),
                                                  Country = (string)r.Element("country"),
                                                  City = (string)r.Element("city"),
                                                  State = (string)r.Element("state"),
                                                  PostalCode = (string)r.Element("postal_code"),
                                                  AddressRemarks = (string)r.Element("address_remarks")
                                              }).First();
                        }

                    }

                xElement = doc.Root.Elements("shippto");
                Ship ship = new Ship();

                if (xElement != null)
                    foreach (var child in xElement.Elements())
                    {

                        if (child.Name.LocalName == "contact_person")
                        {
                            var billto = XDocument.Parse(child.ToString());

                            ship.ContactPerson = (from r in billto.Elements("contact_person")
                                                  select new ContactPerson
                                                  {
                                                      FirstName = (string)r.Element("first_name"),
                                                      LastName = (string)r.Element("last_name"),
                                                      FullName = (string)r.Element("full_name"),
                                                      Email = (string)r.Element("email"),
                                                      Phone = (string)r.Element("phone"),
                                                      Fax = (string)r.Element("fax")

                                                  }).First();
                        }

                        if (child.Name.LocalName == "address")
                        {
                            var billto = XDocument.Parse(child.ToString());

                            ship.Address = (from r in billto.Elements("address")
                                            select new Address
                                            {
                                                ExternalAddressId = (string)r.Element("external_address_id"),
                                                AddressLine1 = (string)r.Element("address_line_1"),
                                                AddressLine2 = (string)r.Element("address_line_2"),
                                                Country = (string)r.Element("country"),
                                                City = (string)r.Element("city"),
                                                State = (string)r.Element("state"),
                                                PostalCode = (string)r.Element("postal_code"),
                                                AddressRemarks = (string)r.Element("address_remarks")
                                            }).First();
                        }
                    }


                PickUp pickUp;
                pickUp = (from r in doc.Root.Elements("pickup")
                          select new PickUp
                          {
                              BranchCode = (string)r.Element("branch_code"),
                              Country = (string)r.Element("country"),
                              State = (string)r.Element("state"),
                              City = (string)r.Element("city"),
                              Address = (string)r.Element("address"),
                              Phone = (string)r.Element("phone"),
                              postalCode = (string)r.Element("postal_code")
                          }).First();


                xElement = doc.Root.Elements("order_lines");
                List<OrderLine> orderLines = null;

                if (xElement != null)
                    foreach (var child in xElement.Elements())
                    {

                        var order_line = XDocument.Parse(child.ToString());


                        orderLines = (from r in order_line.Elements("order_line")
                                      select new OrderLine
                                      {
                                          WebProductId = (string)r.Element("web_product_id"),
                                          ItemNumber = (string)r.Element("item_number"),
                                          ItemCustomerNumber = (!string.IsNullOrEmpty((string)r.Element("item_customer_number"))) ? (Guid)r.Element("item_customer_number") : new Guid(),
                                          Quantity = (!string.IsNullOrEmpty((string)r.Element("quantity"))) ? (decimal)r.Element("quantity") : new decimal(),
                                          ProductPrice = (!string.IsNullOrEmpty((string)r.Element("product_price"))) ? (decimal)r.Element("product_price") : new decimal(),
                                          ProductPriceCurrency = (string)r.Element("product_price_currency")

                                      }).ToList();

                    }


                /*Genrate Request object*/

                Customer cust = new Customer();
                cust.Ship = new Ship();
                cust.CustName = cust.Ship.ShipToName = ship.ContactPerson.FirstName + " " + ship.ContactPerson.LastName;
                cust.Ship.EmailAddress = ship.ContactPerson.Email;
                cust.Ship.TelNo = ship.ContactPerson.Phone;
                cust.Ship.FaxNo = ship.ContactPerson.Fax;
                cust.Ship.Address1 = ship.Address.AddressLine1;
                cust.Ship.Address2 = ship.Address.AddressLine2 + " " + ship.Address.City + " " + ship.Address.State + " " + ship.Address.Country;
                cust.Ship.Zip4 = ship.Address.PostalCode;


                ////if customer is new
                if (orderHead.CustomerNumber.ToString() == "00000000-0000-0000-0000-000000000000")
                {
                    // insert customer
                    cust = cmtClass.InsertNewCustomer(cust);
                    orderHead.CustomerNumber = Guid.Parse(cust.CustomerNumber);
                    orderHead.ShipToID = cust.ShipToID;
                }

                // p_CDSCreateSalesOrderHdr
                //p_CDSCreateSalesOrderDtl
                orderHead = cmtClass.InsertNewOrderHeader(orderHead);
                if (orderLines.Count > 0)
                {
                    foreach (var lines in orderLines)
                    {
                        lines.OEDocID = orderHead.OEDocID;
                        cmtClass.InsertNewOrderDetail(lines);
                    }
                }

                return orderHead;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}