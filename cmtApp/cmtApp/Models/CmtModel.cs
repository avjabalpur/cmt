﻿using cmtApp.Common;
using cmtApp.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace cmtApp.Models
{
    public class CmtModel : Utility
    {
        private SqlConnection con { get; set; }

        #region InsertNewCustomer

        public Customer InsertNewCustomer(Customer customer)
        {

            string resultVal = string.Empty;

            try
            {
                using (con = Utility.GetConnection())
                {
                    var cmd = new SqlCommand("p_AR_Cust_Insert", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();
                    customer.TemplateCustomerID = GetCustID(customer.TemplateCustNo);

                    //Place check for TemplateCustomerID being null here
                    if (customer.TemplateCustomerID == null)
                    {
                        customer.ErrorMessage = "Error - Template customer number does not exist.";
                        return customer;
                    }


                    cmd.Parameters.Add("@TEMPLATE_CUST_NO", SqlDbType.VarChar);
                    cmd.Parameters["@TEMPLATE_CUST_NO"].Value = customer.TemplateCustNo;
                    cmd.Parameters.Add("@CUST_NAME", SqlDbType.VarChar);
                    cmd.Parameters["@CUST_NAME"].Value = customer.CustName;
                    cmd.Parameters.Add("@XREF_NAME", SqlDbType.VarChar);
                    cmd.Parameters["@XREF_NAME"].Value = customer.XRefName;

                    //Defines the return parameter sent back from the sql statement
                    cmd.Parameters.Add("@CUST_NO", SqlDbType.VarChar, 8);
                    cmd.Parameters["@CUST_NO"].Direction = System.Data.ParameterDirection.Output;
                    cmd.Parameters.Add("@CUST_ID", SqlDbType.UniqueIdentifier);
                    cmd.Parameters["@CUST_ID"].Direction = System.Data.ParameterDirection.Output;

                    //executes the stored procedure and returns a value indicating the number of effected records which should be 1 when successful and 0 when not
                    int retVal = cmd.ExecuteNonQuery();

                    //stores the cust_id returned from the stored procedure, will be used to identify the customer when the ship to record is created
                    customer.CustomerNumber = cmd.Parameters["@CUST_NO"].Value.ToString();
                    customer.CustomerID = cmd.Parameters["@CUST_ID"].Value.ToString();


                    con.Close();
                    con.Dispose();
                    cmd.Dispose();

                    if (retVal == 1)
                    {
                        customer.Ship.TemplateCustNo = customer.CustomerNumber;
                        customer.Ship.CustId = customer.CustomerID;
                        customer.ShipToID = ARShipTo_Insert(customer.Ship);
                    }
                    else
                    {
                        customer.ErrorMessage = "Error creating customer record";
                        return customer;
                    }

                    if (customer.ShipToID.StartsWith("Error"))
                    {
                        customer.ErrorMessage = "Error Creating Ship To record";
                        return customer;
                    }
                    else
                        resultVal = ARCustStandard_Insert(customer.TemplateCustomerID, customer.CustomerID, customer.ShipToID);
                    if (resultVal == "SUCCESS")
                    {
                        customer.BillTo.CustomerID = customer.CustomerID;
                        resultVal = ARBillTo_Insert(customer.BillTo);
                    }
                    else
                    {
                        customer.ErrorMessage = "Error creating Bill-To, please contact Technical Support!";
                        return customer;
                    }

                    if (String.Compare(resultVal, "SUCCESS") == 0 && customer.ShipToDept.Dept != null && String.Compare(customer.ShipToDept.Dept, "") != 0)
                    {
                        customer.ShipToDept.ShipToID = customer.ShipToID;
                        customer.ShipToDeptID = ARShipToDept_Insert(customer.ShipToDept);
                    }

                    if (resultVal == "SUCCESS")
                    {
                        return customer;
                    }
                    else
                    {
                        customer.ErrorMessage = "Unknown Error, please contact Technical Support!";
                        return customer;
                    }
                }
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | InsertNewCustomer). " + sqlex.GetBaseException());
                //return CustomerID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | InsertNewCustomer). " + ex.Message);
                //return CustomerID;
            }
            finally
            {

            }
        }

        #endregion

        #region InsertNewShipTo

        public Ship InsertNewShipTo(Ship ship)
        {
            try
            {

                string TemplateCustomerID = GetCustID(ship.TemplateCustomerID);

                ship.ShipToID = ARShipTo_Insert(ship);

                if (ship.ShipToID != null && String.Compare(ship.ShipToID, "") != 0 && ship.ShipToDept.Dept != null && String.Compare(ship.ShipToDept.Dept, "") != 0)
                {
                    ship.ShipToDept.ShipToID = ship.ShipToID;
                    ship.ShipToDept.ShipToDeptID = ARShipToDept_Insert(ship.ShipToDept);
                }

                if (ship.ShipToID != null && String.Compare(ship.ShipToID, "") != 0)
                {
                    return ship;
                }
                else
                {
                    ship.ErrorMessage = "Unknown Error, please contact Technical Support!";
                    return ship;
                }
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error insert new ship-to record (class | Method; wsCustomer | InsertNewShipTo). " + sqlex.GetBaseException());
                //return CustomerID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error insert new ship-to record (class | Method; wsCustomer | InsertNewShipTo). " + ex.Message);
                //return CustomerID;
            }
            finally
            {

            }
        }
        #endregion

        #region GetCustID
        private string GetCustID(string CUST_NO)
        {
            string TemplateCustomerID = string.Empty;
            SqlCommand cmd;
            using (con = Utility.GetConnection())
            {


                try
                {
                    cmd = new SqlCommand("p_AR_CUST_Select_CUST_ID", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@CUST_NO", SqlDbType.VarChar);
                    cmd.Parameters["@CUST_NO"].Value = CUST_NO;
                    cmd.Parameters.Add("@CUST_ID", SqlDbType.UniqueIdentifier);
                    cmd.Parameters["@CUST_ID"].Direction = System.Data.ParameterDirection.Output;

                    int i = cmd.ExecuteNonQuery();

                    //stores the cust_id returned from the stored procedure, will be used to identify the template customer record
                    TemplateCustomerID = cmd.Parameters["@CUST_ID"].Value.ToString();


                    cmd.Dispose();

                    return TemplateCustomerID;
                }
                catch (SqlException sqlex)
                {
                    throw new Exception("Error 100 - SQL error getting template customer ID (class | Method; wsCustomer | GetCustID). " + sqlex.GetBaseException());
                }
                catch (Exception ex)
                {
                    throw new Exception("Error 101 - error getting template customer ID (Class | Method; wsCustomer | GetCustID). " + ex.Message);
                }
                finally
                {

                }
            }
        }
        #endregion

        #region ARShip2_Insert()
        private string ARShipTo_Insert(Ship ship)
        {

            string ShipToID = string.Empty;

            try
            {

                using (con = Utility.GetConnection())
                {
                    var cmd = new SqlCommand("p_AR_ShipTo_Insert", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();


                    //cmd.Parameters.Add("@TEMPLATE_CUST_ID", SqlDbType.UniqueIdentifier);
                    cmd.Parameters.Add("@TEMPLATE_CUST_ID", SqlDbType.VarChar);
                    cmd.Parameters["@TEMPLATE_CUST_ID"].Value = ship.TemplateCustomerID;
                    cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                    cmd.Parameters["@CUST_ID"].Value = ship.CustId;
                    cmd.Parameters.Add("@SHIP_NO", SqlDbType.VarChar);
                    cmd.Parameters["@SHIP_NO"].Value = ship.ShipNo;
                    cmd.Parameters.Add("@SHIPTO_NAME", SqlDbType.VarChar);
                    cmd.Parameters["@SHIPTO_NAME"].Value = ship.ShipToName;
                    cmd.Parameters.Add("@ADDRESS1", SqlDbType.VarChar);
                    cmd.Parameters["@ADDRESS1"].Value = ship.Address1;
                    cmd.Parameters.Add("@ADDRESS2", SqlDbType.VarChar);
                    cmd.Parameters["@ADDRESS2"].Value = ship.Address2;
                    cmd.Parameters.Add("@ZIP_CODE_DTL_ID", SqlDbType.VarChar);
                    cmd.Parameters["@ZIP_CODE_DTL_ID"].Value = ship.ZipCodeDtlId;

                    cmd.Parameters.Add("@ZIP4", SqlDbType.VarChar);
                    cmd.Parameters["@ZIP4"].Value = ship.Zip4;

                    cmd.Parameters.Add("@TEL_NO", SqlDbType.VarChar);
                    cmd.Parameters["@TEL_NO"].Value = ship.TelNo;
                    cmd.Parameters.Add("@FAX_NO", SqlDbType.VarChar);
                    cmd.Parameters["@FAX_NO"].Value = ship.FaxNo;
                    cmd.Parameters.Add("@WEB_ADDRESS", SqlDbType.VarChar);
                    cmd.Parameters["@WEB_ADDRESS"].Value = ship.WebAddress;
                    cmd.Parameters.Add("@TAX_EXEMPT_NO", SqlDbType.VarChar);
                    cmd.Parameters["@TAX_EXEMPT_NO"].Value = ship.TaxExemptNo;
                    cmd.Parameters.Add("@COMMENTS", SqlDbType.VarChar);
                    cmd.Parameters["@COMMENTS"].Value = ship.Comments;
                    cmd.Parameters.Add("@EMAIL_ADDRESS", SqlDbType.VarChar);
                    cmd.Parameters["@EMAIL_ADDRESS"].Value = ship.EmailAddress;
                    cmd.Parameters.Add("@FED_EX_ACCT", SqlDbType.VarChar);
                    cmd.Parameters["@FED_EX_ACCT"].Value = ship.FedExAcct;
                    cmd.Parameters.Add("@UPS_ACCT", SqlDbType.VarChar);
                    cmd.Parameters["@UPS_ACCT"].Value = ship.UpsAcct;
                    cmd.Parameters.Add("@DEFAULT_CARRIER", SqlDbType.VarChar);
                    cmd.Parameters["@DEFAULT_CARRIER"].Value = ship.DefaultCarrier;
                    cmd.Parameters.Add("@DEFAULT_CARRIER_SERVICE_CODE", SqlDbType.VarChar);
                    cmd.Parameters["@DEFAULT_CARRIER_SERVICE_CODE"].Value = ship.DefulatCarrierServiceCode;

                    //defines the return parameter sent back from the sql statement
                    cmd.Parameters.Add("@SHIP2_ID", SqlDbType.UniqueIdentifier);
                    cmd.Parameters["@SHIP2_ID"].Direction = System.Data.ParameterDirection.Output;

                    int i = cmd.ExecuteNonQuery();


                    //stores the cust_id returned from the stored procedure, will be used to identify the customer when the ship to record is created
                    ShipToID = cmd.Parameters["@SHIP2_ID"].Value.ToString();

                    con.Close();
                    con.Dispose();
                    cmd.Dispose();

                    return ShipToID;
                }
            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error retrieving customer details (class | Method; wsCustomer | ARShipTo_Insert). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - getting customer details (Class | Method; wsCustomer | ARShipTo_Insert). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion

        #region ARCustStandard_Insert()
        private string ARCustStandard_Insert(string templateCustomerID, string customerID, string shipToId)
        {


            try
            {

                using (con = Utility.GetConnection())
                {
                    var cmd = new SqlCommand("p_AR_CUST_STANDARD_Insert", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();

                    cmd.Parameters.Add("@TEMPLATE_CUST_ID", SqlDbType.VarChar);
                    cmd.Parameters["@TEMPLATE_CUST_ID"].Value = templateCustomerID;
                    cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                    cmd.Parameters["@CUST_ID"].Value = customerID;
                    cmd.Parameters.Add("@SHIP2_ID", SqlDbType.VarChar);
                    cmd.Parameters["@SHIP2_ID"].Value = shipToId;

                    int i = cmd.ExecuteNonQuery();

                    con.Close();
                    con.Dispose();
                    cmd.Dispose();

                    return "SUCCESS";
                }
            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error creating customer standard record (class | Method; wsCustomer | ARCustStandard_Insert). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - creating customer standard record (Class | Method; wsCustomer | ARCustStandard_Insert). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion

        #region ARBillTo_Insert
        private string ARBillTo_Insert(BillTo billTo)
        {


            try
            {

                using (con = Utility.GetConnection())
                {
                    var cmd = new SqlCommand("p_AR_BillTo_Insert", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                    cmd.Parameters["@CUST_ID"].Value = billTo.CustomerID;
                    cmd.Parameters.Add("@BILL_TO_NAME", SqlDbType.VarChar);
                    cmd.Parameters["@BILL_TO_NAME"].Value = billTo.BillToName;
                    cmd.Parameters.Add("@BILL_TO_ADDRESS1", SqlDbType.VarChar);
                    cmd.Parameters["@BILL_TO_ADDRESS1"].Value = billTo.BillToAddress1;
                    cmd.Parameters.Add("@BILL_TO_ADDRESS2", SqlDbType.VarChar);
                    cmd.Parameters["@BILL_TO_ADDRESS2"].Value = billTo.BillToAddress2;
                    cmd.Parameters.Add("@BILL_TO_ZIP_CODE_DTL_ID", SqlDbType.VarChar);
                    cmd.Parameters["@BILL_TO_ZIP_CODE_DTL_ID"].Value = billTo.BillToZipCodeId;
                    cmd.Parameters.Add("@BILL_TO_ZIP4", SqlDbType.VarChar);
                    cmd.Parameters["@BILL_TO_ZIP4"].Value = billTo.BillToZip4;
                    cmd.Parameters.Add("@BILL_TO_TEL_NO", SqlDbType.VarChar);
                    cmd.Parameters["@BILL_TO_TEL_NO"].Value = billTo.BillToTelNo;
                    cmd.Parameters.Add("@BILL_TO_FAX_NO", SqlDbType.VarChar);
                    cmd.Parameters["@BILL_TO_FAX_NO"].Value = billTo.BillToFaxNo;
                    cmd.Parameters.Add("@BILL_TO_WEB_ADDRESS", SqlDbType.VarChar);
                    cmd.Parameters["@BILL_TO_WEB_ADDRESS"].Value = billTo.BillToWebAddress;
                    cmd.Parameters.Add("@BILL_TO_EMAIL_ADDRESS", SqlDbType.VarChar);
                    cmd.Parameters["@BILL_TO_EMAIL_ADDRESS"].Value = billTo.BillToEmailAddress;


                    int i = cmd.ExecuteNonQuery();

                    con.Close();
                    con.Dispose();
                    cmd.Dispose();

                    return "SUCCESS";
                }
            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error creating bill-to customer record (class | Method; wsCustomer | ARBillTo_Insert). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - creating bill-to customer record (Class | Method; wsCustomer | ARBillTo_Insert). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion

        #region ARShipToDept_Insert
        public string ARShipToDept_Insert(ShipToDept shipToDept)
        {
            string ShipToDeptID = string.Empty;



            try
            {
                //If IS_MAILED is not set to either Y or N, then set it to N because it is a required field
                if (String.Compare(shipToDept.IsMailed, "Y") != 0 && String.Compare(shipToDept.IsMailed, "N") != 0)
                {
                    shipToDept.IsMailed = "Y";
                }

                using (con = Utility.GetConnection())
                {
                    var cmd = new SqlCommand("p_AR_ShipTo_Dept_Insert", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();



                    cmd.Parameters.Add("@SHIP2_ID", SqlDbType.VarChar);
                    cmd.Parameters["@SHIP2_ID"].Value = shipToDept.ShipToID;
                    cmd.Parameters.Add("@DEPT", SqlDbType.VarChar);
                    cmd.Parameters["@DEPT"].Value = shipToDept.Dept;
                    cmd.Parameters.Add("@DESCRIPTION", SqlDbType.VarChar);
                    cmd.Parameters["@DESCRIPTION"].Value = shipToDept.Description;
                    cmd.Parameters.Add("@ATTN", SqlDbType.VarChar);
                    cmd.Parameters["@ATTN"].Value = shipToDept.Attn;
                    cmd.Parameters.Add("@TEL_NO", SqlDbType.VarChar);
                    cmd.Parameters["@TEL_NO"].Value = shipToDept.TelNo;
                    cmd.Parameters.Add("@FAX_NO", SqlDbType.VarChar);
                    cmd.Parameters["@FAX_NO"].Value = shipToDept.FaxNo;
                    cmd.Parameters.Add("@IS_MAILED", SqlDbType.Char);
                    cmd.Parameters["@IS_MAILED"].Value = shipToDept.IsMailed;
                    cmd.Parameters.Add("@EMAIL_ADDRESS", SqlDbType.VarChar);
                    cmd.Parameters["@EMAIL_ADDRESS"].Value = shipToDept.EmailAddress;

                    //defines the return parameter sent back from the sql statement
                    cmd.Parameters.Add("@DEPT_ID", SqlDbType.UniqueIdentifier);
                    cmd.Parameters["@DEPT_ID"].Direction = System.Data.ParameterDirection.Output;

                    int i = cmd.ExecuteNonQuery();


                    //stores the dept_id returned from the stored procedure, will be sent back to the calling application so they have the uniqueidentifier for the record
                    ShipToDeptID = cmd.Parameters["@DEPT_ID"].Value.ToString();

                    con.Close();
                    con.Dispose();
                    cmd.Dispose();

                    return ShipToDeptID;
                }
            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error creating shipto department record (class | Method; wsCustomer | InsertNewShipToDept). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - creating shipto department record (Class | Method; wsCustomer | InsertNewShipToDept). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion

        #region InsertNewCustomer

        public List<Item> getwerhouseStockQty(int itemNo)
        {

            string resultVal = string.Empty;


            try
            {
                using (con = Utility.GetConnection())
                {
                    var cmd = new SqlCommand("p_AR_Cust_Insert", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();
                    var details = new List<Item>();


                    //Place check for TemplateCustomerID being null here
                    if (itemNo == 0)
                    {
                        throw new Exception("please provider item no. ");
                    }

                    cmd = new SqlCommand("p_cds_get_whse_qty_available", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ItemNo", SqlDbType.VarChar);
                    cmd.Parameters["@ItemNo"].Value = itemNo;

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Item item = new Item();
                            item.whseCode = Utility.GetStringData(reader, 0);
                            item.ItemNo = Utility.GetStringData(reader, 1);
                            item.qtyAvailable = Utility.GetStringData(reader, 2);
                            details.Add(item);
                        }
                        return details;
                    }


                }
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | InsertNewCustomer). " + sqlex.GetBaseException());
                //return CustomerID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | InsertNewCustomer). " + ex.Message);
                //return CustomerID;
            }
            finally
            {

            }
        }

        #endregion

        #region getPricingIntegration
        public Price getPricingIntegration(string itemNumber, string customerNumber, string shipToNumber, string whseCode, int qty)
        {
            string TemplateCustomerID = string.Empty;
            SqlCommand cmd;
            using (con = Utility.GetConnection())
            {


                try
                {
                    cmd = new SqlCommand("p_cds_oe_pricing_engine_wsAPI", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@itemNumber", SqlDbType.VarChar);
                    cmd.Parameters["@itemNumber"].Value = itemNumber;
                    cmd.Parameters.Add("@customerNumber", SqlDbType.VarChar);
                    cmd.Parameters["@customerNumber"].Value = customerNumber;
                    cmd.Parameters.Add("@shipToNumber", SqlDbType.VarChar);
                    cmd.Parameters["@shipToNumber"].Value = shipToNumber;
                    cmd.Parameters.Add("@whseCode", SqlDbType.VarChar);
                    cmd.Parameters["@whseCode"].Value = shipToNumber;

                    cmd.Parameters.Add("@qty", SqlDbType.Int);
                    cmd.Parameters["@qty"].Value = shipToNumber;




                    cmd.Parameters["@customer_price"].Direction = System.Data.ParameterDirection.Output;

                    cmd.Parameters["@proc_no"].Direction = System.Data.ParameterDirection.Output;



                    int i = cmd.ExecuteNonQuery();
                    Price price = new Price();
                    double x;
                    Double.TryParse(cmd.Parameters["@customer_price"].Value.ToString(), out x);
                    price.customerPrice = x;

                    price.procNo = cmd.Parameters["@proc_no"].Value.ToString();
                    cmd.Dispose();

                    return price;
                }
                catch (SqlException sqlex)
                {
                    throw new Exception("Error 100 - SQL error getting template customer ID (class | Method; wsCustomer | GetCustID). " + sqlex.GetBaseException());
                }
                catch (Exception ex)
                {
                    throw new Exception("Error 101 - error getting template customer ID (Class | Method; wsCustomer | GetCustID). " + ex.Message);
                }
                finally
                {

                }
            }
        }
        #endregion

        #region [p_CDSCreateSalesOrderHdr]

        public OrderHeader InsertNewOrderHeader(OrderHeader order)
        {

            string resultVal = string.Empty;


            try
            {
                using (con = Utility.GetConnection())
                {
                    var cmd = new SqlCommand("p_CDSCreateSalesOrderHdr", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();

                    cmd.Parameters.Add("@OEDocID", SqlDbType.UniqueIdentifier);
                    cmd.Parameters["@OEDocID"].Value = order.OEDocID;
                    cmd.Parameters.Add("@CustID", SqlDbType.VarChar);
                    cmd.Parameters["@CustID"].Value = order.CustomerNumber;
                    cmd.Parameters.Add("@ShipToID", SqlDbType.VarChar);
                    cmd.Parameters["@ShipToID"].Value = order.ShipToID;
                    cmd.Parameters.Add("@CustDeptID", SqlDbType.VarChar, 8);
                    cmd.Parameters["@CustDeptID"].Value = order.CustDeptID;
                    cmd.Parameters.Add("@WhseId", SqlDbType.VarChar);
                    cmd.Parameters["@WhseId"].Value = order.WhseId;
                    cmd.Parameters.Add("@SubOrderType", SqlDbType.VarChar);
                    cmd.Parameters["@SubOrderType"].Value = order.OrderType;
                    cmd.Parameters.Add("@WillCallDate", SqlDbType.Date);
                    cmd.Parameters["@WillCallDate"].Value = order.OrderTime;
                    cmd.Parameters.Add("@SourceCode", SqlDbType.VarChar, 8);
                    cmd.Parameters["@SourceCode"].Value = "";
                    cmd.Parameters.Add("@CustPO", SqlDbType.VarChar);
                    cmd.Parameters["@CustPO"].Value = "";
                    cmd.Parameters.Add("@OrderDate", SqlDbType.VarChar);
                    cmd.Parameters["@OrderDate"].Value = order.OrderTime;
                    cmd.Parameters.Add("@UserID", SqlDbType.VarChar);
                    cmd.Parameters["@UserID"].Value = order.WebUserId;
                    cmd.Parameters.Add("@ReleaseNo", SqlDbType.VarChar, 8);
                    cmd.Parameters["@ReleaseNo"].Value = order.ReleaseNo;
                    cmd.Parameters.Add("@COMPANY_ID", SqlDbType.VarChar);
                    cmd.Parameters["@COMPANY_ID"].Value = "";
                    cmd.Parameters.Add("@OrderedBy", SqlDbType.VarChar);
                    cmd.Parameters["@OrderedBy"].Value = "";
                    cmd.Parameters.Add("@EarliestShipDate", SqlDbType.VarChar, 8);
                    cmd.Parameters["@EarliestShipDate"].Value = new DateTime();
                    cmd.Parameters.Add("@LatestShipDate", SqlDbType.VarChar);
                    cmd.Parameters["@LatestShipDate"].Value = new DateTime();
                    cmd.Parameters.Add("@Carrier", SqlDbType.VarChar);
                    cmd.Parameters["@Carrier"].Value = "";
                    cmd.Parameters.Add("@ServiceCode", SqlDbType.VarChar);
                    cmd.Parameters["@ServiceCode"].Value = order.ServiceCode;
                    cmd.Parameters.Add("@Instructions", SqlDbType.VarChar, 8);
                    cmd.Parameters["@Instructions"].Value = order.SpecialInstructions;
                    cmd.Parameters.Add("@UPLOAD_STATUS", SqlDbType.UniqueIdentifier);
                    cmd.Parameters["@UPLOAD_STATUS"].Direction = System.Data.ParameterDirection.Output;

                    //executes the stored procedure and returns a value indicating the number of effected records which should be 1 when successful and 0 when not
                    int retVal = cmd.ExecuteNonQuery();
                    order.UploadStatus = cmd.Parameters["@UPLOAD_STATUS"].Value.ToString();

                    con.Close();
                    con.Dispose();
                    cmd.Dispose();
                    return order;
                }
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | InsertNewCustomer). " + sqlex.GetBaseException());
                //return CustomerID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | InsertNewCustomer). " + ex.Message);
                //return CustomerID;
            }
            finally
            {

            }
        }

        #endregion

        #region [p_CDSCreateSalesOrderDtl]

        public OrderLine InsertNewOrderDetail(OrderLine order)
        {

            string resultVal = string.Empty;


            try
            {
                using (con = Utility.GetConnection())
                {
                    var cmd = new SqlCommand("p_CDSCreateSalesOrderDtl", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    con.Open();

                    cmd.Parameters.Add("@OEDocID", SqlDbType.UniqueIdentifier);
                    cmd.Parameters["@OEDocID"].Value = order.OEDocID;
                    cmd.Parameters.Add("@OE_DOC_DTL_ID", SqlDbType.UniqueIdentifier);
                    cmd.Parameters["@OE_DOC_DTL_ID"].Value = order.OEDocDtlId;
                    cmd.Parameters.Add("@DeptId", SqlDbType.VarChar);
                    cmd.Parameters["@DeptId"].Value = order.DeptId;
                    cmd.Parameters.Add("@CustPO", SqlDbType.VarChar, 8);
                    cmd.Parameters["@CustPO"].Value = order.CustPO;
                    cmd.Parameters.Add("@ShipByDate", SqlDbType.DateTime);
                    cmd.Parameters["@ShipByDate"].Value = order.ShipByDate;
                    cmd.Parameters.Add("@ItemNumber", SqlDbType.VarChar);
                    cmd.Parameters["@ItemNumber"].Value = order.ItemNumber;
                    cmd.Parameters.Add("@ItemDescription", SqlDbType.Date);
                    cmd.Parameters["@ItemDescription"].Value = order.ItemDescription;
                    cmd.Parameters.Add("@SortOrder", SqlDbType.Int);
                    cmd.Parameters["@SortOrder"].Value = order.SortOrder;
                    cmd.Parameters.Add("@CustItem", SqlDbType.VarChar);
                    cmd.Parameters["@CustItem"].Value = "";
                    cmd.Parameters.Add("@QtyOrdered", SqlDbType.Decimal);
                    cmd.Parameters["@QtyOrdered"].Value = order.Quantity;
                    cmd.Parameters.Add("@QtyUM", SqlDbType.VarChar);
                    cmd.Parameters["@QtyUM"].Value = order.QtyUM;
                    cmd.Parameters.Add("@QtyConv", SqlDbType.Decimal);
                    cmd.Parameters["@QtyConv"].Value = order.QtyConv;
                    cmd.Parameters.Add("@AmtUM", SqlDbType.VarChar);
                    cmd.Parameters["@AmtUM"].Value = order.ProductPriceCurrency;
                    cmd.Parameters.Add("@AmtConv", SqlDbType.Decimal);
                    cmd.Parameters["@AmtConv"].Value = order.ProductPrice;
                    cmd.Parameters.Add("@@COMPANY_ID", SqlDbType.UniqueIdentifier);
                    cmd.Parameters["@@COMPANY_ID"].Value = "";
                    cmd.Parameters.Add("@@CommitToday", SqlDbType.VarChar);
                    cmd.Parameters["@@CommitToday"].Value = "";
                    cmd.Parameters.Add("@UPLOAD_STATUS", SqlDbType.UniqueIdentifier);
                    cmd.Parameters["@UPLOAD_STATUS"].Direction = System.Data.ParameterDirection.Output;

                    //executes the stored procedure and returns a value indicating the number of effected records which should be 1 when successful and 0 when not
                    int retVal = cmd.ExecuteNonQuery();
                    order.UploadStatus = cmd.Parameters["@UPLOAD_STATUS"].Value.ToString();

                    con.Close();
                    con.Dispose();
                    cmd.Dispose();
                    return order;
                }
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | InsertNewCustomer). " + sqlex.GetBaseException());
                //return CustomerID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | InsertNewCustomer). " + ex.Message);
                //return CustomerID;
            }
            finally
            {

            }
        }

        #endregion
        
    }
}