USE CMT
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

 
 
 
 
 
 
 
alter   procedure [dbo].[p_cds_oe_quantity_price] @whse_id varchar(255),
                                          @item_id varchar(255),
                                          @ship2_id varchar(255),
                                          @qty decimal(15, 4),
                                          @custBracket int,
                                          @price money out,
                                          @proc_no integer out,
                                          @error_code integer out
as
 
 
 
-- Set the @proc_no and the initial @error_code
select @proc_no = 2004
select @error_code = 0
select @price = null
 
-- Declare Header Fetch Variables
declare @qty_table_id varchar(255)
declare @method char(1)
declare @cost_basis char(1)
declare @avg_unit_cost decimal(17,8)
declare @total_cost decimal(17,8)
 
 
-- Check for a Qty Price Bracket and the warehouse item level
select @qty_table_id = convert(varchar(255), qty_table_id),
       @qty_table_id = convert(varchar(255), qty_table_id),
       @method = method,
       @cost_basis = cost_basis
from in_qty_price_bracket_hdr (nolock)
where whse_id = @whse_id and
      item_id = @item_id
 
if (@qty_table_id is null)
begin
 
      -- Check for a Qty Price Bracket at the Item level
      select @qty_table_id = convert(varchar(255), qty_table_id),
               @qty_table_id = convert(varchar(255), qty_table_id),
               @method = method,
               @cost_basis = cost_basis
      from in_qty_price_bracket_hdr (nolock)
      where item_id = @item_id
 
end
-- If Qty Price Bracket Exists then ....   
if @qty_table_id is not null begin
 
  -- declare the rest of the variables and retrieve data
  declare @group_id varchar(255)
  declare @list_price money
  declare @next_cost money
  declare @std_cost money
  declare @bracket decimal(15, 4)
  declare @unit_price money
  declare @percentage decimal(15, 4)
  declare @addon_amt money
  declare @bracket_fetch smallint
  declare @best_price money
  declare @unit_cost money
  declare @reference_price money
 
  declare @bracket_discQty decimal(15, 4),
            @bracket_Qty decimal(15, 4),
            @bracket_unit_price money,
              @bracket_percentage decimal(15, 4),
              @bracket_addon_amt money
                   
  -- Get the Group Id, List Price, Next Cost, Std Cost
  select @group_id = group_id,
         @list_price = list_price,
         @next_cost = next_cost,
         @std_cost = std_cost
  from in_item_master (nolock)
  where item_id = @item_id
 
  -- Declare the Bracket Cursor
  declare brackets scroll cursor for
    select bracket,
           qty,
           unit_price,
           percentage/100 as percentage,
           addon_amt
    from in_qty_price_bracket_dtl (nolock)
    where qty_table_id = @qty_table_id
    order by bracket
        
  -- Open the Brackets Cursor 
  open brackets
         
  -- Fetch the variables frm the Cursor
  fetch from brackets  into @bracket,
                           @bracket_discQty,
                           @bracket_unit_price,
                           @bracket_percentage,
                           @bracket_addon_amt
         
  -- Cache the Bracket Fetch Status  
  select @bracket_fetch = @@fetch_status
  
  set @unit_price = @bracket_unit_price
  set @percentage = @bracket_percentage
  set @addon_amt = @bracket_addon_amt
         
  -- Loop through the brackets until you find a bracket that is greater than the qty required                     
  while (@bracket_fetch = 0) begin
    if @qty > @bracket or @custbracket > @bracket begin 
      fetch next from brackets  into @bracket,
                                    @bracket_discQty,
                                    @bracket_unit_price,
                                    @bracket_percentage,
                                    @bracket_addon_amt
                               
          if @qty >= @bracket_discQty or @custBracket >= @bracket begin
             set @unit_price = @bracket_unit_price
             set @percentage = @bracket_percentage
             set @addon_amt = @bracket_addon_amt
          end
      select @bracket_fetch = @@fetch_status
    end else
      select @bracket_fetch = -1                     
  end
         
  -- Close and DeAllocate the Brackets Cursor
  close brackets
  deallocate brackets
          
  -- Check for Null Values
  if @unit_price is null select @unit_price = 0
  if @percentage is null select @percentage = 0
  if @addon_amt is null select @addon_amt = 0
 
  -- Method is L
  if @method = 'L' begin
    select @best_price = @list_price - (@list_price * @percentage) + @addon_amt
  -- Method is C
  end else if @method = 'C' begin
    -- Cost Basis is A
    if @cost_basis = 'A' begin
      -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
      exec p_cds_in_cost_layers @whse_id, 
                                @item_id,
                                @qty,
                                0,
                                null,
                                null,
                                @avg_unit_cost out,
                                @total_cost out
      select @unit_cost = @avg_unit_cost
    -- Cost Basis is R
    end else if @cost_basis = 'R' begin
      select @unit_cost = @next_cost
    -- Cost Basis is S
    end else if @cost_basis = 'S' begin
     select @unit_cost = @std_cost
    end
   
    -- Assign the best price using the unit cost
    select @best_price = @unit_cost + (@unit_cost * @percentage) + @addon_amt
  -- Method is M
  end else if @method = 'M' begin
    -- Cost Basis is A
    if @cost_basis = 'A' begin
      -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
      exec p_cds_in_cost_layers @whse_id, 
                                @item_id,
                                @qty,
                                0,
                                null,
                                null,
                                @avg_unit_cost out,
                                @total_cost out
      select @unit_cost = @avg_unit_cost
    -- Cost Basis is R
    end else if @cost_basis = 'R' begin
      select @unit_cost = @next_cost
    -- Cost Basis is S
    end else if @cost_basis = 'S' begin
      select @unit_cost = @std_cost
    end
   
    -- Assign the best price using the unit cost
    select @best_price = (@unit_cost / (1 - @percentage)) + @addon_amt
  -- Method is U
  end else if @method = 'U' begin
    select @best_price = @list_price - (@list_price * @percentage) + @addon_amt
  -- Method is R
  end else if @method = 'R' begin
    -- Get the Reference Price
    select @reference_price = reference_price
    from in_item_ref_price (nolock)
    where item_id = @item_id
   
    select @best_price = @reference_price - (@reference_price * @percentage) + @addon_amt
  -- Method is D
  end else if @method = 'D' begin
    select @best_price = @unit_price + @addon_amt
  end
  
  -- Return the price
  select @price = @best_price
end
 
return


GO
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

 
 
 
 
 
 
 
alter   procedure [dbo].[p_cds_oe_quantity_price] @whse_id varchar(255),
                                          @item_id varchar(255),
                                          @ship2_id varchar(255),
                                          @qty decimal(15, 4),
                                          @custBracket int,
                                          @price money out,
                                          @proc_no integer out,
                                          @error_code integer out
as
 
 
 
-- Set the @proc_no and the initial @error_code
select @proc_no = 2004
select @error_code = 0
select @price = null
 
-- Declare Header Fetch Variables
declare @qty_table_id varchar(255)
declare @method char(1)
declare @cost_basis char(1)
declare @avg_unit_cost decimal(17,8)
declare @total_cost decimal(17,8)
 
 
-- Check for a Qty Price Bracket and the warehouse item level
select @qty_table_id = convert(varchar(255), qty_table_id),
       @qty_table_id = convert(varchar(255), qty_table_id),
       @method = method,
       @cost_basis = cost_basis
from in_qty_price_bracket_hdr (nolock)
where whse_id = @whse_id and
      item_id = @item_id
 
if (@qty_table_id is null)
begin
 
      -- Check for a Qty Price Bracket at the Item level
      select @qty_table_id = convert(varchar(255), qty_table_id),
               @qty_table_id = convert(varchar(255), qty_table_id),
               @method = method,
               @cost_basis = cost_basis
      from in_qty_price_bracket_hdr (nolock)
      where item_id = @item_id
 
end
-- If Qty Price Bracket Exists then ....   
if @qty_table_id is not null begin
 
  -- declare the rest of the variables and retrieve data
  declare @group_id varchar(255)
  declare @list_price money
  declare @next_cost money
  declare @std_cost money
  declare @bracket decimal(15, 4)
  declare @unit_price money
  declare @percentage decimal(15, 4)
  declare @addon_amt money
  declare @bracket_fetch smallint
  declare @best_price money
  declare @unit_cost money
  declare @reference_price money
 
  declare @bracket_discQty decimal(15, 4),
            @bracket_Qty decimal(15, 4),
            @bracket_unit_price money,
              @bracket_percentage decimal(15, 4),
              @bracket_addon_amt money
                   
  -- Get the Group Id, List Price, Next Cost, Std Cost
  select @group_id = group_id,
         @list_price = list_price,
         @next_cost = next_cost,
         @std_cost = std_cost
  from in_item_master (nolock)
  where item_id = @item_id
 
  -- Declare the Bracket Cursor
  declare brackets scroll cursor for
    select bracket,
           qty,
           unit_price,
           percentage/100 as percentage,
           addon_amt
    from in_qty_price_bracket_dtl (nolock)
    where qty_table_id = @qty_table_id
    order by bracket
        
  -- Open the Brackets Cursor 
  open brackets
         
  -- Fetch the variables frm the Cursor
  fetch from brackets  into @bracket,
                           @bracket_discQty,
                           @bracket_unit_price,
                           @bracket_percentage,
                           @bracket_addon_amt
         
  -- Cache the Bracket Fetch Status  
  select @bracket_fetch = @@fetch_status
  
  set @unit_price = @bracket_unit_price
  set @percentage = @bracket_percentage
  set @addon_amt = @bracket_addon_amt
         
  -- Loop through the brackets until you find a bracket that is greater than the qty required                     
  while (@bracket_fetch = 0) begin
    if @qty > @bracket or @custbracket > @bracket begin 
      fetch next from brackets  into @bracket,
                                    @bracket_discQty,
                                    @bracket_unit_price,
                                    @bracket_percentage,
                                    @bracket_addon_amt
                               
          if @qty >= @bracket_discQty or @custBracket >= @bracket begin
             set @unit_price = @bracket_unit_price
             set @percentage = @bracket_percentage
             set @addon_amt = @bracket_addon_amt
          end
      select @bracket_fetch = @@fetch_status
    end else
      select @bracket_fetch = -1                     
  end
         
  -- Close and DeAllocate the Brackets Cursor
  close brackets
  deallocate brackets
          
  -- Check for Null Values
  if @unit_price is null select @unit_price = 0
  if @percentage is null select @percentage = 0
  if @addon_amt is null select @addon_amt = 0
 
  -- Method is L
  if @method = 'L' begin
    select @best_price = @list_price - (@list_price * @percentage) + @addon_amt
  -- Method is C
  end else if @method = 'C' begin
    -- Cost Basis is A
    if @cost_basis = 'A' begin
      -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
      exec p_cds_in_cost_layers @whse_id, 
                                @item_id,
                                @qty,
                                0,
                                null,
                                null,
                                @avg_unit_cost out,
                                @total_cost out
      select @unit_cost = @avg_unit_cost
    -- Cost Basis is R
    end else if @cost_basis = 'R' begin
      select @unit_cost = @next_cost
    -- Cost Basis is S
    end else if @cost_basis = 'S' begin
     select @unit_cost = @std_cost
    end
   
    -- Assign the best price using the unit cost
    select @best_price = @unit_cost + (@unit_cost * @percentage) + @addon_amt
  -- Method is M
  end else if @method = 'M' begin
    -- Cost Basis is A
    if @cost_basis = 'A' begin
      -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
      exec p_cds_in_cost_layers @whse_id, 
                                @item_id,
                                @qty,
                                0,
                                null,
                                null,
                                @avg_unit_cost out,
                                @total_cost out
      select @unit_cost = @avg_unit_cost
    -- Cost Basis is R
    end else if @cost_basis = 'R' begin
      select @unit_cost = @next_cost
    -- Cost Basis is S
    end else if @cost_basis = 'S' begin
      select @unit_cost = @std_cost
    end
   
    -- Assign the best price using the unit cost
    select @best_price = (@unit_cost / (1 - @percentage)) + @addon_amt
  -- Method is U
  end else if @method = 'U' begin
    select @best_price = @list_price - (@list_price * @percentage) + @addon_amt
  -- Method is R
  end else if @method = 'R' begin
    -- Get the Reference Price
    select @reference_price = reference_price
    from in_item_ref_price (nolock)
    where item_id = @item_id
   
    select @best_price = @reference_price - (@reference_price * @percentage) + @addon_amt
  -- Method is D
  end else if @method = 'D' begin
    select @best_price = @unit_price + @addon_amt
  end
  
  -- Return the price
  select @price = @best_price
end
 
return


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create   procedure [dbo].[p_cds_oe_pricecolumn_price] (@whse_id varchar(255),
                                             @item_id varchar(255),
                                             @ship2_id varchar(255),
                                             @qty decimal(15, 4),
                                             @price_source char(2) out,
                                             @price money out,
                                             @proc_no integer out,
                                             @error_code integer out)
as

-- Set the @proc_no and the initial @error_code
select @proc_no = 2005
select @error_code = 0
select @price = null


-- Declare Fetch Variables
declare @ar_price_col_id varchar(255)
declare @disc_method_id char(1)
declare @cost_basis_id char(1)
declare @price_method_id char(1)
declare @percentage decimal(15, 4)
declare @unit_cost money
declare @best_price money
declare @list_price money
declare @next_cost money
declare @std_cost money
declare @ok_to_discount char(1)
declare @max_disc_percentage decimal(15, 4)
declare @min_margin_percentage decimal(15, 4)
declare @max_disc_price money
declare @min_margin_price money
declare @actual_cost money
declare @total_cost money
declare @avg_unit_cost decimal(17,8)


-- Retrieve Price Column Id
select @ar_price_col_id = ar_price_col_id
from ar_ship2 (nolock) 
where ship2_id = @ship2_id

-- Return if Price Column Id is null
if @ar_price_col_id is null
  return(0)
  
-- Get the List Price, Next Cost, Std Cost, Ok To Discount, Max Disc Percentage, Min Margin Percentage
select @list_price = list_price,
       @next_cost = next_cost,
       @std_cost = std_cost,
       @ok_to_discount = ok_to_discount,
       @max_disc_percentage = max_disc_percentage,
       @min_margin_percentage = min_margin_percentage
from in_item_master (nolock) 
where item_id = @item_id
    
-- Retrieve Price Column Attributes
select @disc_method_id = disc_method_id,
       @cost_basis_id = cost_basis_id,
       @price_method_id = price_method_id,
       @percentage = (percentage/100)
from ar_price_column (nolock) 
where ar_price_col_id = @ar_price_col_id

-- Get the Actual Cost
exec p_cds_in_cost_layers @whse_id,  
                          @item_id,
                          @qty, 
                          0,
                          null,
                          null,
                          @avg_unit_cost out,
                          @total_cost out
select @actual_cost = @avg_unit_cost
-- If Disc Method Id is B
if @disc_method_id = 'B' begin
  select @best_price = @list_price
-- If Disc Method Id is L
end else if @disc_method_id = 'L' begin
  -- if Price Method Id is L
  if @price_method_id = 'L' begin
    select @best_price = @list_price - (@list_price * @percentage)
  -- if Price Method Id is C
  end else if @price_method_id = 'C' begin
    -- If Cost Basis Id is A
    if @cost_basis_id = 'A' begin
      select @unit_cost = @actual_cost
    -- If Cost Basis Id is R
    end else if @cost_basis_id = 'R' begin
      select @unit_cost = @next_cost
    -- If Cost Basis Id is S 
    end else if @cost_basis_id = 'S' begin
      select @unit_cost = @std_cost
    end
    -- Select Best Price
    select @best_price = @unit_cost + (@unit_cost * @percentage)
  -- if Price Method Id is M
  end else if @price_method_id = 'M' begin
    -- If Cost Basis Id is A
    if @cost_basis_id = 'A' begin
      select @unit_cost = @actual_cost
    -- If Cost Basis Id is R
    end else if @cost_basis_id = 'R' begin
      select @unit_cost = @next_cost
    -- If Cost Basis Id is S 
    end else if @cost_basis_id = 'S' begin
      select @unit_cost = @std_cost
    end
    -- Select Best Price
    select @best_price = @unit_cost / (1 - @percentage)
  -- if Price Method Id is U
  end else if @price_method_id = 'U' begin
    select @best_price = @list_price + (@list_price * @percentage)
  -- if Price Method Id is R
  end else if @price_method_id = 'R' begin
    -- Declare the Reference Price
    declare @reference_price money
    -- Get the Reference Price
    select @reference_price = reference_price
    from in_item_ref_price (nolock)
    where item_id = @item_id
    -- Set Best Price
    select @best_price = @reference_price - (@reference_price * @percentage)
  -- if Price Method Id is X
  end else if @price_method_id = 'X' begin
    select @best_price = @list_price * @percentage
  end
-- If Disc Method Id is G
end else if @disc_method_id = 'G' begin
  select @best_price = @list_price
end

-- Check Ok To Discount
if @ok_to_discount = 'N' begin
  select @price_source = 'ND'
  select @price = @list_price
-- Check Max Disc Percentage
end else if @max_disc_percentage > 0 begin
  select @max_disc_price = @list_price - (@list_price * @max_disc_percentage)
  if @max_disc_price < @best_price begin
    select @price_source = 'MD'
    select @price = @max_disc_price
  end else begin
    select @price_source = 'PC'
    select @price = @best_price
  end
-- Check Min Margin Percentage
end else if @min_margin_percentage > 0 begin
  select @min_margin_price = @actual_cost / (1 - @min_margin_percentage)
  if @min_margin_price < @best_price begin
    select @price_source = 'MM'
    select @price = @min_margin_price
  end else begin
    select @price_source = 'PC'
    select @price = @best_price
  end
end else begin
  select @price_source = 'PC'
  select @price = @best_price
end

return(0)

on_error:
return(1)
go

alter   procedure [dbo].[p_cds_oe_list_price] @item_id varchar(255),
                                       @whse_id varchar(255),
                                       @list_price money out,
                                       @proc_no integer out,
                                       @error_code integer out
                                 
as

-- Set the @proc_no and the initial @error_code
select @proc_no = 2001
select @error_code = 0
select @list_price = null

-- Get WhseItem ListPrice if available
select @list_price = list_price
from in_whseitem_master (nolock) 
where item_id = @item_id and
      whse_id = @whse_id
      
if @list_price is null begin
  select @list_price = list_price
  from in_item_master (nolock) 
  where item_id = @item_id  
end

return(0)

on_error:
return(1)











GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




alter    procedure [dbo].[p_cds_in_cost_layers] @whse_id uniqueidentifier,
                                       @item_id uniqueidentifier,
                                       @qty decimal(17, 8),
                                       @adjust_layers bit,
                                       @oe_doc_dtl_id uniqueidentifier,  
                                       @in_doc_dtl_id uniqueidentifier,  
                                       @avg_unit_cost decimal(17,8) out,
                                       @total_cost decimal(17, 8) out

as

-- Declare Local Variables
declare @costing_method char(1)
declare @inventory_id uniqueidentifier
declare @current_qty decimal(17, 8)
declare @unit_cost decimal(17, 8)
declare @cum_qty decimal(17, 8)
declare @over_qty decimal(17, 8)
declare @new_qty decimal(17, 8)
declare @cum_cost decimal(17, 8)

declare @qty_on_hand decimal(14,6)
declare @next_cost decimal(17, 8)
declare @std_cost decimal(17, 8)

-- Initialize Return Variables
select @avg_unit_cost = 0
select @total_cost = 0
select @cum_qty = 0
select @cum_cost = 0
select @over_qty = 0

select @qty_on_hand = in_whseitem_master.qty_on_hand,
       @next_cost = in_item_master.next_cost,
       @std_cost = in_item_master.std_cost
from in_whseitem_master (nolock)
     inner join in_item_master  (nolock) on in_whseitem_master.item_id = in_item_master.item_id
where in_whseitem_master.whse_id = @whse_id
      and in_whseitem_master.item_id = @item_id

if @qty_on_hand is null begin  
  select @qty_on_hand = 0,
         @next_cost = in_item_master.next_cost,
         @std_cost = in_item_master.std_cost
  from in_item_master (nolock)
  where in_item_master.item_id = @item_id
end

-- Retrieve the COSTING_METHOD frm CO_MISC_CONTROL
select @costing_method = costing_method
from co_misc_control (nolock)

if (@costing_method = 'W') or (@costing_method = 'S') begin
    
  -- Case: Weighted Average or Standard Method
  select @avg_unit_cost = avg_cost,
         @total_cost = (@qty * avg_cost)
  from in_whseitem_master (nolock)
  where whse_id = @whse_id and
        item_id = @item_id
  if (@@ERROR <> 0) GOTO on_error

  -- Check to see if item is found
  if @avg_unit_cost is null begin
    return(1)
  end
    
  declare my_cursor scroll cursor for
  select inventory_id,
         current_qty,
         unit_cost
  from in_inventory  (nolock)
  where whse_id = @whse_id 
        and item_id = @item_id
        and ((reserve_oe_doc_dtl_id is null  
             and reserve_in_doc_dtl_id is null)
             or (reserve_oe_doc_dtl_id = @oe_doc_dtl_id
                 or reserve_in_doc_dtl_id = @in_doc_dtl_id))
  order by date_created asc
  if (@@ERROR <> 0) GOTO on_error
end else if @costing_method = 'L' begin
  -- Case: Lifo Method
  declare my_cursor scroll cursor for
  select inventory_id,
         current_qty,
         unit_cost
  from in_inventory  (nolock)
  where whse_id = @whse_id 
        and item_id = @item_id
        and ((reserve_oe_doc_dtl_id is null  
             and reserve_in_doc_dtl_id is null)
             or (reserve_oe_doc_dtl_id = @oe_doc_dtl_id
                 or reserve_in_doc_dtl_id = @in_doc_dtl_id))
  order by date_created desc
  if (@@ERROR <> 0) GOTO on_error
end else if @costing_method = 'F' begin
  -- Case: Fifo Method
  declare my_cursor scroll cursor for
  select inventory_id,
         current_qty,
         unit_cost
  from in_inventory  (nolock)
  where whse_id = @whse_id 
        and item_id = @item_id
        and ((reserve_oe_doc_dtl_id is null  
             and reserve_in_doc_dtl_id is null)
             or (reserve_oe_doc_dtl_id = @oe_doc_dtl_id
                 or reserve_in_doc_dtl_id = @in_doc_dtl_id))
  order by date_created asc
  if (@@ERROR <> 0) GOTO on_error
end else begin
  return(1)
end

-- Open my_cursor
open my_cursor

-- Fetch First Record
fetch next from my_cursor  into @inventory_id,
          @current_qty,
          @unit_cost
if (@@ERROR <> 0) GOTO on_error

-- Loop through Records
while (@@fetch_status = 0) and (@cum_qty < @qty) begin
  set @cum_qty = @cum_qty + @current_qty

  set @over_qty = @cum_qty - @qty

  if @over_qty > 0 begin
    set @cum_qty = @cum_qty - @over_qty
    set @new_qty = @current_qty - @over_qty
  end else begin
    set @new_qty = @current_qty
  end

  set @cum_cost = @cum_cost + (@new_qty * @unit_cost)

  if @adjust_layers = 1 begin
    update in_inventory
    set current_qty = current_qty - @new_qty
    where inventory_id = @inventory_id
    if (@@ERROR <> 0) GOTO on_error
  end
  
  fetch next from my_cursor  into @inventory_id,
            @current_qty,
            @unit_cost
  if (@@ERROR <> 0) GOTO on_error
end

-- Set return variables
if @costing_method != 'W' begin 
  set @total_cost = @cum_cost
  if @cum_qty > 0
    set @avg_unit_cost = @cum_cost / @cum_qty
  else
    set @avg_unit_cost = 0
end

if (@avg_unit_cost = 0) and (@adjust_layers = 0) and (@qty_on_hand = 0) begin  
  if @next_cost > 0
    set @avg_unit_cost = @next_cost
  else
    set @avg_unit_cost = @std_cost

  set @total_cost = @avg_unit_cost * @qty
end
  
-- Close and DeAllocate the cursor
close my_cursor
deallocate my_cursor

return(0)

on_error:
return(1)












GO

