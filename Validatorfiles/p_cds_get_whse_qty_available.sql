USE [CMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Johnny Smith
-- Create date: 5/22/2015
-- Description:	Get the warehouse item quantity available
-- =============================================
create PROCEDURE p_cds_get_whse_qty_available
	@ItemNo varchar(32) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select dbo.whse_code(w.whse_id) as whseCode, item_no as itemNo, dbo.qty_avail(i.item_id, w.whse_id) as qtyAvailable
    from in_item_master i (nolock)
    inner join in_whseitem_master w (nolock) on i.item_id = w.item_id
    where i.item_no = @ItemNo
    
END
GO
