/****** Object:  StoredProcedure [dbo].[p_CDSCreateSalesOrderDtl]    Script Date: 06/18/2015 22:05:35 ******/

USE [CMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER Procedure [dbo].[p_CDSCreateSalesOrderDtl](
                                             @OE_DOC_ID uniqueidentifier,
                                             @OE_DOC_DTL_ID uniqueidentifier,
                                             @DeptId uniqueidentifier = null,
                                             @CustPO varchar(20) = null,
                                             @ShipByDate datetime = null,
                                             @ItemNumber varchar(32),
                                             @ItemDescription varchar(50),
                                             @SortOrder int,
                                             @CustItem varchar(32) = null,
                                             @QtyOrdered decimal(14,6),
                                             @QtyUM varchar(2) = null,
                                             @QtyConv decimal(14,6) = null,
                                             @AmtUM varchar(2) = null,
                                             @AmtConv decimal(14,6) = null,
                                             @COMPANY_ID uniqueidentifier = null,
                                             @CommitToday char(1) = 'Y',
                                             @UploadStatus int out)
AS

declare @CustId uniqueidentifier
declare @CreatedByUserId uniqueidentifier
declare @CustItemId uniqueidentifier
declare @GlSetId uniqueidentifier
declare @InsideSalesrepId uniqueidentifier
declare @ItemId uniqueidentifier
declare @ShipToId uniqueidentifier
declare @OutsideSalesrepId uniqueidentifier
declare @UserID uniqueidentifier
declare @VendorId uniqueidentifier
declare @WhseId uniqueidentifier
declare @OrderDate datetime
declare @AvgCost money
declare @CustPrice money
declare @StockAidPrice money
declare @ContractPrice money
declare @QtyPrice money
declare @ProductGroupPrice money
declare @PriceColPrice money
declare @PromoPrice money
declare @ListPrice money
declare @ReferencePrice money
declare @AcctgPeriod int
declare @AcctgYear int
declare @DocNo int
declare @ErrorCode int
declare @ProcNo int
declare @LatestDaysToShip smallint
declare @Rounding smallint
declare @QtyAvailable decimal(14,6)
declare @QtyBackOrdered decimal(17,8)
declare @CommitQty decimal(17,8)
declare @RestockRate decimal(5,2)
declare @BackOrderCtrlId char(1)
declare @CustomItem char(1)
declare @Disposition char(1)
declare @IsFractional char(1)
declare @IsActive char(1)
declare @IsCommentPrintAcknow char(1)
declare @IsCommentPrintPackslip char(1)
declare @IsCommentPrintPickTicket char(1)
declare @IsCommentPrintInvoice char(1)
declare @IsFamilyDiscOk char(1)
declare @IsStocked char(1)
declare @IsItemTaxable char(1)
declare @IsShipToTaxable char(1)
declare @LineType char(1)
declare @PriceSource varchar(2)
declare @oe_contract_id varchar(255)




BEGIN TRY

	set @CustomItem = 'N'
	SET @Disposition = 'S'
	set @IsActive = 'N'
	set @QtyAvailable = 0
	
	select 
	  @DocNo = doc_no,
	  @CustPO = ISNULL(@CustPO, h.cust_po),
	  @ShipByDate = isnull(@ShipByDate, h.ship_by_date),
	  @DeptId = ISNULL(@DeptId, h.cust_dept_id),
	  @CustId = h.cust_id,
	  @ShipToId = h.ship2_id,
	  @WhseId = h.shipping_whse_id,
	  @UserID = h.entry_user_id,
	  @CreatedByUserId = h.entry_user_id
	from OE_DOC_HDR h (nolock)
	where OE_DOC_ID = h.oe_doc_id
	

	select  
	  @ItemId = item_id, 
	  @IsFractional = is_decimal, 
	  @LineType = status_id,
	  @QtyUm = ISNULL(@QtyUM, qty_um),
	  @QtyConv = ISNULL(@QtyConv, qty_conv),
	  @AmtUM = ISNULL(@AmtUm, price_um),
	  @AmtConv = ISNULL(@AmtConv, price_conv_factor),
	  @IsItemTaxable = is_taxable,
	  @IsStocked = is_stocked,
	  @GlSetId = gl_set_id,
	  @Rounding = rounding,
	  @ItemDescription = base_description,
	  @VendorId = vendor_id
	from in_item_master (nolock)
	where item_no = @ItemNumber
	
	if(@ItemId is null)
		select  
		  @ItemId = item_id, 
		  @IsFractional = is_decimal, 
		  @LineType = status_id,
		  @QtyUm = ISNULL(@QtyUM, qty_um),
		  @QtyConv = ISNULL(@QtyConv, qty_conv),
		  @AmtUM = ISNULL(@AmtUm, price_um),
		  @AmtConv = ISNULL(@AmtConv, price_conv_factor),
		  @IsItemTaxable = is_taxable,
		  @IsStocked = is_stocked,
		  @CustomItem = 'Y',
		  @GlSetId = gl_set_id,
		  @Rounding = rounding
		from in_item_master (nolock)
		where item_no = 'CustomItem'

	select 
	  @WhseID = isnull(@whseId, whse_id), 
	  @DeptId = isnull(@DeptId, cust_dept_id),
	  @BackOrderCtrlId = backorder_ctrl_id,
	  @InsideSalesrepId = inside_salesrep_id,
	  @OutsideSalesrepId = outside_salesrep_id,
	  @IsCommentPrintAcknow = IS_COMMENT_PRINT_ACKNOW,
	  @IsCommentPrintPackslip = IS_COMMENT_PRINT_PACKSLIP,
	  @IsCommentPrintPickTicket = IS_COMMENT_PRINT_PICKTKT,
	  @IsCommentPrintInvoice = IS_COMMENT_PRINT_INVOICE,
	  @IsFamilyDiscOk = is_family_disc_ok,
	  @IsShipToTaxable = is_taxable,
	  @LatestDaysToShip = latest_days_to_ship,
	  @RestockRate = restock_rate
	from ar_ship2 (nolock) 
	where ship2_id = @ShipToID
	

	SELECT 
	  @IsActive = is_active, 
	  @QtyAvailable = qty_on_hand - commit_today_qty - commit_later_qty - qty_rework - qty_damage,
	  @IsStocked = 'Y',
	  @AvgCost = avg_cost,
	  @GlSetId = gl_set_id
	FROM IN_WHSEITEM_MASTER 
	WHERE item_id = @ItemId 
	  and whse_id = @WhseID
	  and is_active = 'Y'
	  and @CustomItem = 'N'
	
	if(@VendorId is null)
		set @VendorId = (select top 1 vendor_id from in_vndritem_master (nolock) where item_id = @ItemId)
		
	if(@IsStocked = 'Y' and @IsActive = 'Y')
	begin
		if(@QtyAvailable >= @QtyOrdered)
		begin
			set @CommitQty = @QtyOrdered
			set @QtyBackOrdered = 0
		end else begin
			set @CommitQty = @QtyAvailable
			set @QtyBackOrdered = @QtyOrdered - @QtyAvailable 
		end
		
		if (@CommitToday = 'Y')
			update in_whseitem_master
			set commit_today_qty = commit_today_qty + @QtyOrdered,
			    backlog_qty_stock = backlog_qty_stock + @QtyBackOrdered
			where whse_id = @WhseId and item_id = @ItemId
		else
			update in_whseitem_master
			set commit_later_qty = commit_later_qty + @QtyOrdered,
			    backlog_qty_stock = backlog_qty_stock + @QtyBackOrdered
			where whse_id = @WhseId and item_id = @ItemId
	end
		
  
	select 
	  @ReferencePrice = reference_price
	from IN_ITEM_REF_PRICE r (nolock)
	where ITEM_ID = @ItemId
	
	if(@ReferencePrice is null)
		set @ReferencePrice = 0
        
	if (@IsStocked = 'N' and @LineType != 'V')
		SET @lineType = 'N'
	else
		set @LineType = 'S'
	
	
	IF @IsStocked = 'Y' AND @IsActive = 'Y' AND @QtyAvailable >= @QtyOrdered
		SET @disposition = 'C'
	IF @IsStocked = 'Y' AND @IsActive = 'Y' AND @QtyAvailable < @QtyOrdered
		SET @disposition = 'B'

	select @AcctgPeriod = dbo.acctg_period((dbo.enspire_date(getdate())))

	if exists (select oe_doc_dtl_id from oe_doc_dtl where oe_doc_dtl_id = @OE_DOC_DTL_ID)
	  begin
		 if @UploadStatus = 1
			delete from oe_doc_dtl where oe_doc_dtl_id = @OE_DOC_DTL_ID
		 else begin
			set @UploadStatus = 2
			 return 1
		 end
	  end
	else
	  begin
		set @UploadStatus = 1
	  end

	select 
	  @CustItemId = xref_item_id 
	FROM in_item_cust_xref (nolock) 
	WHERE item_id = @itemId 
	  and cust_id = @custID
	  and ship2_id = @ShipToId
	  
	if(@CustItemId is null)
		select 
		  @CustItemId = xref_item_id 
		FROM in_item_cust_xref (nolock) 
		WHERE item_id = @itemId 
		  and cust_id = @custID
		  and ship2_id is null
	  	
	--get the pricing parameters
	--exec p_cds_oe_pricing_engine_incl_contractid 
	--			@WhseID,      
 --    			@ItemID,      
	--			@ShipToID,      
	--			@QtyOrdered,                                           
	--			@AcctgPeriod,                                           
	--			null,                                         
	--			@IsFractional,                                         
	--			@CustPrice out, 
	--			@StockAidPrice out, 
	--			@ContractPrice out,  
	--			@QtyPrice out, 
	--			@ProductGroupPrice out,  
	--			@PriceColPrice out, 
	--			@PromoPrice out, 
	--			@ListPrice out,
	--			@PriceSource out,
	--			@oe_contract_id out,
	--			@ProcNo out,
	--			@ErrorCode out

	----Inserts a new record in oe_doc_dtl
	----return error 8010
	insert oe_doc_dtl
	(OE_DOC_DTL_ID, OE_DOC_ID, SHIP2_ID, SORT_ORDER, LINE_TYPE, DISPOSITION, ITEM_ID, ORIG_ITEM_ID, ITEM_NO, DESCRIPTION,
	 EARLY_SHIP_DATE, SHIP_BY_DATE, PRICE_SOURCE, IS_TAXABLE, FLAG_BOTTOM_LINE_DISC, MISS_LATE, MISS_SUB, MISS_BO, MISS, HIT, QTY_UM, QTY_CONV, 
	 AMT_CONV, AMT_UM, GL_SET_ID, QTY_ORDERED, QTY_SHIPPED, QTY_INVOICED, QTY_BACK_ORDERED, COMMIT_QTY, COMMIT_LEVEL, QTY_CANCELED, 
	 QTY_UNIT_CONTROLLED, QTY_REBATE_CUST, QTY_REBATE_VENDOR, QTY_CREDITED, CUST_QTY_REBATE_CREDITED, VENDOR_QTY_REBATE_CREDITED, 
	 LIST_PRICE, CUST_PRICE, BLIND_PRICE, U_COST_ACTUAL, U_COST_NEXT, U_COST_STD, MANUAL_CUST_UNIT_REBATE_AMT, CUST_UNIT_REBATE_AMT, 
	 MANUAL_VEND_REBATE, VENDOR_UNIT_REBATE_AMT, LINE_DISC_RATE, BOTTOM_LINE_DISC_RATE, EXTENDED_AMT, SPIFF_AMT, OE_CONTRACT_ID, LICENSE_USED, 
	 REFERENCE_PRICE, LINE_LEVEL_FAM_DISC_RATE, BOT_LINE_FAM_DISC_RATE, IS_BACKORDERED, SUB_METHOD, PRICE_STRING, QTY_BRACKET, CUST_DEPT_ID, 
	 BACKORDER_CTRL, IS_FAMILY_DISCOUNT_OK, IS_COMMISSION_PAID, RESTOCK_PERCENTAGE, IS_CUST_REBATE_OK, IS_VEND_REBATE_OK, BTO_LABOR_COST, 
	 BTO_PACKAGING_COST, BTO_OVERHEAD_COST, PICK_ERROR_COST, COMMIT_TODAY, COMMIT_LATER_QTY, QTY_SCHEDULED, TIME_ADDED, ROUNDING, 
	 QTY_PREV_SHIPPED, PRINT_ACK, PRINT_PACK, PRINT_PICK, PRINT_INVOICE, GROUP_ID, SYSTEM_PRICE, LAST_CHANGE_DATE)
	select
	  @OE_DOC_DTL_ID AS OE_DOC_DTL_ID, 
	  @OE_DOC_ID AS OE_DOC_ID, 
	  @ShipToID as SHIP2_ID, 
	  @SortOrder as SORT_ORDER, 
	  @LineType as LINE_TYPE,
	  @Disposition AS DISPOSITION,
	  @ItemID as ITEM_ID,
	  @ItemID as ORIG_ITEM_ID,
	  @ItemNumber as ITEM_NO,
	  @ItemDescription as [DESCRIPTION],
	  dbo.enspire_date(getdate()) as EARLY_SHIP_DATE,
	  isnull(@ShipByDate, @LatestDaysToShip + dbo.enspire_date(getdate())) as SHIP_BY_DATE, 
	  @PriceSource as PRICE_SOURCE,
	  case when @IsShipToTaxable = 'Y' and @IsItemTaxable = 'Y' then 'Y' else 'N' end as IS_TAXABLE,
	  'N' as FLAG_BOTTOM_LINE_DISC,
	  'N' as MISS_LATE, 
	  'N' as MISS_SUB, 
	  'N' as MISS_BO, 
	  'N' as MISS, 
	  'N' as HIT, 
	  @QtyUM as QTY_UM,
	  @QtyConv as QTY_CONV,
	  @QtyConv as AMT_CONV,
	  @QtyUM as AMT_UM,
	  @GlSetId as GL_SET_ID,
	  @QtyOrdered as QTY_ORDERED,
	  0 as QTY_SHIPPED, 
	  0 as QTY_INVOICED, 
	  @QtyBackOrdered as QTY_BACK_ORDERED, 
	  @CommitQty as COMMIT_QTY, 
	  0 as COMMIT_LEVEL, 
	  0 as QTY_CANCELED, 
	  0 as QTY_UNIT_CONTROLLED, 
	  0 as QTY_REBATE_CUST, 
	  0 as QTY_REBATE_VENDOR, 
	  0 as QTY_CREDITED, 
	  0 as CUST_QTY_REBATE_CREDITED, 
	  0 as VENDOR_QTY_REBATE_CREDITED, 
	  @ListPrice as LIST_PRICE,
	  @CustPrice as CUST_PRICE,
	  @ListPrice as BLIND_PRICE, 
	  @AvgCost as U_COST_ACTUAL, 
	  i.NEXT_COST as U_COST_NEXT, 
	  i.STD_COST as U_COST_STD, 
	  0 as MANUAL_CUST_UNIT_REBATE_AMT, 
	  0 as CUST_UNIT_REBATE_AMT, 
	  0 as MANUAL_VEND_REBATE, 
	  0 as VENDOR_UNIT_REBATE_AMT, 
	  round((1 - @CustPrice / @ListPrice) * 100, 2) as LINE_DISC_RATE, 
	  0 as BOTTOM_LINE_DISC_RATE, 
	  @CustPrice * @QtyOrdered as EXTENDED_AMT, 
	  0 as SPIFF_AMT, 
	  case when @PriceSource = 'CO' then @oe_contract_id else null end as OE_CONTRACT_ID, 
	  'N' as LICENSE_USED, 
	  @ReferencePrice as REFERENCE_PRICE, 
	  0 as LINE_LEVEL_FAM_DISC_RATE, 
	  0 as BOT_LINE_FAM_DISC_RATE, 
	  'N' as IS_BACKORDERED, 
	  'N' as SUB_METHOD, 
	  cast((@CustPrice / @QtyConv) as varchar(25)) as PRICE_STRING, 
	  0 as QTY_BRACKET, 
	  @DeptId as CUST_DEPT_ID, 
	  case when i.OK_TO_BO = 'Y' then @BackOrderCtrlId else 'N' end as BACKORDER_CTRL, 
	  @IsFamilyDiscOk as IS_FAMILY_DISCOUNT_OK, 
	  i.IS_COMMISSION_PAID as IS_COMMISSION_PAID, 
	  @RestockRate as RESTOCK_PERCENTAGE, 
	  'N' as IS_CUST_REBATE_OK, 
	  'Y' as IS_VEND_REBATE_OK, 
	  0 as BTO_LABOR_COST, 
	  0 as BTO_PACKAGING_COST, 
	  0 as BTO_OVERHEAD_COST, 
	  0 as PICK_ERROR_COST, 
	  @CommitToday as COMMIT_TODAY, 
	  0 as COMMIT_LATER_QTY, 
	  0 as QTY_SCHEDULED, 
	  dbo.enspire_datetime(getdate()) as TIME_ADDED, 
	  i.ROUNDING, 
	  0 as QTY_PREV_SHIPPED, 
	  @IsCommentPrintAcknow as PRINT_ACK, 
	  @IsCommentPrintPackslip as PRINT_PACK, 
	  @IsCommentPrintPickTicket as PRINT_PICK,
	  @IsCommentPrintInvoice as  PRINT_INVOICE, 
	  i.GROUP_ID, 
	  0 as SYSTEM_PRICE, 
	  '1/1/1950' as LAST_CHANGE_DATE
	from in_item_master i
	where i.ITEM_ID = @ItemId 
		
	
	INSERT INTO OE_BOOKINGS (
							BOOKING_ID,
							OE_DOC_DTL_ID,
							OE_DOC_ID,
							DOC_NO,
							DOC_CODE,
							WHSE_ID,
							CUST_PO,
							INSIDE_SALESREP_ID,
							OUTSIDE_SALESREP_ID,
							BOOK_DATE,
							ACCTG_YEAR,
							ACCTG_PERIOD,
							USER_ID,
							ITEM_ID,
							CUST_PRICE,
							UNIT_COST,
							BOOK_AMT,
							AMT_UM,
							AMT_CONV,
							PRICE_SOURCE,
							CURRENT_QTY,
							BOOK_QTY,
							QTY_UM, 
							QTY_CONV, 
							STATUS,
							LINE_TYPE,
							VENDOR_ID,
							CUST_ID,
							SHIP2_ID
							)
	VALUES (
							NEWID(),
							@OE_DOC_DTL_ID,
							@OE_DOC_ID,
							@docNo,
							'B',						--DOC_CODE
							@WhseID,
							@custPO,
							@InsideSalesrepId,
							@OutsideSalesrepId,
							GETDATE(),					--BOOK_DATE
							YEAR(GETDATE()),			--ACCTG_YEAR
							MONTH(GETDATE()),			--ACCTG_PERIOD
							@CreatedByUserId,
							@itemId,
							round(@CustPrice,@rounding), 
							@AvgCost,
							@QtyOrdered * round(@CustPrice,@rounding),
							@QtyUM, 
							@QtyConv, 
							@priceSource, 
							@QtyOrdered,				--CURRENT_QTY
							@QtyOrdered,				--BOOK_QTY
							@QtyUM, 
							@QtyConv, 
							'A',						--STATUS
							@lineType,
							@vendorID,
							@custID,
							@ShipToId
							)


END TRY
BEGIN CATCH
	set @uploadStatus = 2
	
END CATCH













