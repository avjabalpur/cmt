USE [Cmt]
GO
/****** Object:  StoredProcedure [dbo].[p_cds_in_qty_contract]    Script Date: 7/22/2015 1:57:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


alter procedure [dbo].[p_cds_in_qty_contract] (@contract_id varchar(255),
                                       @cust_id varchar(255),
                                       @ship2_id varchar(255),
                                       @item_id varchar(255),
                                       @price money out,
                                       @void_price char(1) out, 
                                       @proc_no integer out,
                                       @error_code integer out)
     
as

-- Set the @proc_no and the initial @error_code
select @proc_no = 2003
select @error_code = 0
select @price = null
  
-- Declare Local Variables
declare @today datetime

-- Declare Fetch Variables
declare @contract_qty decimal(15, 4)
declare @contract_amt money
declare @actual_qty_sold decimal(15, 4)                                  
declare @header_begin datetime
declare @header_end datetime
declare @detail_begin datetime
declare @detail_end datetime
declare @valid_contract bit

-- Get Todays Date
select @today = getdate()

-- Declare variables to hold min and max dates
declare @min_date datetime
declare @max_date datetime

-- Select Fetch Variables
select @contract_qty = IN_QTY_CONTRACT_DTL.CONTRACT_QTY, 
       @contract_amt = IN_QTY_CONTRACT_DTL.CONTRACT_AMT, 
       @actual_qty_sold = IN_QTY_CONTRACT_DTL.ACTUAL_QTY_SOLD, 
       @header_begin = IN_QTY_CONTRACT_HDR.BEGIN_DATE,
       @header_end = IN_QTY_CONTRACT_HDR.END_DATE,
       @detail_begin = IN_QTY_CONTRACT_DTL.BEGIN_DATE,
       @detail_end = IN_QTY_CONTRACT_DTL.END_DATE,
       @void_price = IN_QTY_CONTRACT_DTL.VOID_PRICE
from IN_QTY_CONTRACT_HDR (nolock) 
  inner join IN_QTY_CONTRACT_DTL (nolock) on IN_QTY_CONTRACT_HDR.QTY_CONTRACT_ID = IN_QTY_CONTRACT_DTL.QTY_CONTRACT_ID
where IN_QTY_CONTRACT_HDR.contract_id = @contract_id and
      IN_QTY_CONTRACT_HDR.cust_id = @cust_id and
      IN_QTY_CONTRACT_HDR.ship2_id = @ship2_id and
      IN_QTY_CONTRACT_DTL.item_id = @item_id
      
-- Handle null values for date range variables
if @header_begin is null select @header_begin = '01/01/1900'
if @header_end is null select @header_end = '12/31/3000' 
if @detail_begin is null select @detail_begin = '01/01/1900'
if @detail_end is null select @detail_end = '12/31/3000'

-- Determine Min Date for Contract
select @min_date = '01/01/1900'
if @header_begin > @min_date select @min_date = @header_begin
if @detail_begin > @min_date select @min_date = @detail_begin
  
-- Determine Max Date for Contract
select @max_date = '12/31/3000'
if @header_end < @max_date select @max_date = @header_end
if @detail_end < @max_date select @max_date = @detail_end

-- Determine if Valid Contract
if (@min_date <= @today) and (@max_date >= @today) and  (@actual_qty_sold < @contract_qty)
  select @valid_contract = 1
else
  select @valid_contract = 0
  
-- Assign Return Price
if @valid_contract = 1
  select @price = @contract_amt 

return






