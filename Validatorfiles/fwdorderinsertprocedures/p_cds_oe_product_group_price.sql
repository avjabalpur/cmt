USE cmt
GO
/****** Object:  StoredProcedure [dbo].[p_cds_oe_product_group_price]    Script Date: 7/22/2015 1:55:54 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
alter  procedure [dbo].[p_cds_oe_product_group_price] @whse_id varchar(255),
                                          @item_id varchar(255),
                                          @ship2_id varchar(255),
                                          @qty decimal(15, 4),
                                          @custBracket int,
                                          @price money out,
                                          @proc_no integer out,
                                          @error_code integer out
as



-- Set the @proc_no and the initial @error_code
select @proc_no = 2004
select @error_code = 0
select @price = null

-- Declare Header Fetch Variables
declare @group_id varchar(255)
declare @qty_table_id varchar(255)
declare @method char(1)
declare @cost_basis char(1)
declare @avg_unit_cost decimal(17,8)
declare @total_cost decimal(17,8)
declare @bracket int

-- Set the @proc_no and the initial @error_code
select @proc_no = 2014
select @error_code = 0
select @price = null

-- Get the product group id from the item master
select @group_id = group_id from IN_ITEM_MASTER (nolock) where ITEM_ID = @item_id

-- Check for a Qty Price Bracket
select @qty_table_id = convert(varchar(255), qty_table_id),
	   @method = method,
	   @cost_basis = cost_basis
from in_qty_price_bracket_hdr (nolock) 
where group_id = @group_id

-- If Qty Price Bracket Exists then ....    
if @qty_table_id is not null begin

  -- declare the rest of the variables and retrieve data
  declare @list_price money
  declare @next_cost money
  declare @std_cost money
  declare @discQty decimal(15, 4)
  declare @unit_price money
  declare @percentage decimal(15, 4)
  declare @addon_amt money
  declare @bracket_fetch smallint
  declare @best_price money
  declare @unit_cost money
  declare @reference_price money
  
  declare @bracket_bracket int,
	      @bracket_discQty decimal(15, 4),
	      @bracket_Qty decimal(15, 4),
		  @bracket_unit_price money,
		  @bracket_percentage decimal(15, 4),
		  @bracket_addon_amt money
		  
  -- Get the Group Id, List Price, Next Cost, Std Cost
  select 
         @list_price = list_price,
         @next_cost = next_cost,
         @std_cost = std_cost
  from in_item_master (nolock) 
  where item_id = @item_id
  
  -- Declare the Bracket Cursor
  declare brackets scroll cursor for
    select bracket,
           qty,
           unit_price,
           percentage/100 as percentage,
           addon_amt
    from in_qty_price_bracket_dtl (nolock) 
    where qty_table_id = @qty_table_id
    order by bracket
         
  -- Open the Brackets Cursor  
  open brackets
          
  -- Fetch the variables frm the Cursor
  fetch from brackets /*  (nolock)  */ into @bracket,
	                           @bracket_discQty,
							   @bracket_unit_price,
							   @bracket_percentage,
							   @bracket_addon_amt
          
  -- Cache the Bracket Fetch Status   
  select @bracket_fetch = @@fetch_status
          
  -- Loop through the brackets until you find a bracket that is greater than the qty required                      
  while (@bracket_fetch = 0) begin
    if @qty > @bracket_discQty or @custBracket > @bracket begin  
      fetch next from brackets /* (nolock) */  into @bracket,
		                                @bracket_discQty,
										@bracket_unit_price,
										@bracket_percentage,
										@bracket_addon_amt
          if @qty >= @bracket_discQty or @custBracket >= @bracket begin
             set @discQty = @bracket_discQty
             set @unit_price = @bracket_unit_price
             set @percentage = @bracket_percentage
             set @addon_amt = @bracket_addon_amt
          end
          
      select @bracket_fetch = @@fetch_status
    end else
      select @bracket_fetch = -1                      
  end
          
  -- Close and DeAllocate the Brackets Cursor
  close brackets
  deallocate brackets
           
  -- Check for Null Values
  if @unit_price is null select @unit_price = 0
  if @percentage is null select @percentage = 0
  if @addon_amt is null select @addon_amt = 0
  
  -- Method is L
  if @method = 'L' begin
    select @best_price = @list_price - (@list_price * @percentage) + @addon_amt
  -- Method is C
  end else if @method = 'C' begin
    -- Cost Basis is A
    if @cost_basis = 'A' begin
      -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
      exec p_cds_in_cost_layers @whse_id,  
                                @item_id,
                                @qty, 
                                0,
                                null,
                                null,
                                @avg_unit_cost out,
                                @total_cost out
      select @unit_cost = @avg_unit_cost
    -- Cost Basis is R
    end else if @cost_basis = 'R' begin
      select @unit_cost = @next_cost
    -- Cost Basis is S
    end else if @cost_basis = 'S' begin
     select @unit_cost = @std_cost
    end
    
    -- Assign the best price using the unit cost
    select @best_price = @unit_cost + (@unit_cost * @percentage) + @addon_amt
  -- Method is M
  end else if @method = 'M' begin
    -- Cost Basis is A
    if @cost_basis = 'A' begin
      -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
      exec p_cds_in_cost_layers @whse_id,  
                                @item_id,
                                @qty, 
                                0,
                                null,
                                null,
                                @avg_unit_cost out,
                                @total_cost out
      select @unit_cost = @avg_unit_cost
    -- Cost Basis is R
    end else if @cost_basis = 'R' begin
      select @unit_cost = @next_cost
    -- Cost Basis is S
    end else if @cost_basis = 'S' begin
      select @unit_cost = @std_cost
    end
    
    -- Assign the best price using the unit cost
    select @best_price = (@unit_cost / (1 - @percentage)) + @addon_amt
  -- Method is U
  end else if @method = 'U' begin
    select @best_price = @list_price - (@list_price * @percentage) + @addon_amt
  -- Method is R
  end else if @method = 'R' begin
    -- Get the Reference Price
    select @reference_price = reference_price
    from in_item_ref_price (nolock) 
    where item_id = @item_id
    
    select @best_price = @reference_price - (@reference_price * @percentage) + @addon_amt
  -- Method is D
  end else if @method = 'D' begin
    select @best_price = @unit_price + @addon_amt
  end 
  
  -- Return the price
  select @price = @best_price 
end 

return
