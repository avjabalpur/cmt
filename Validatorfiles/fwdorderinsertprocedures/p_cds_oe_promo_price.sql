USE [CMT]
GO
/****** Object:  StoredProcedure [dbo].[p_cds_oe_promo_price]    Script Date: 7/22/2015 1:54:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter   procedure [dbo].[p_cds_oe_promo_price] (@whse_id varchar(255),
                                       @item_id varchar(255),
                                       @ship2_id varchar(255),
                                       @qty decimal(15, 4),
                                       @price money out,
                                       @period smallint,
                                       @proc_no integer out,
                                       @error_code integer out)
as

-- Set the @proc_no and the initial @error_code
select @proc_no = 2006
select @error_code = 0
select @price = null

-- Declare a variable to set whether a promo has been found
declare @has_promo bit

-- Initialize the variable to indicate no promos have yet been found
select @has_promo = 0

-- Declare a variable to hold the promo fetch status
declare @promo_fetch smallint

-- Declare Fetch Variables
declare @best_price money
declare @priority smallint
declare @timing char(1)
declare @months1 char(1)
declare @months2 char(1) 
declare @months3 char(1) 
declare @months4 char(1) 
declare @months5 char(1) 
declare @months6 char(1) 
declare @months7 char(1) 
declare @months8 char(1) 
declare @months9 char(1) 
declare @months10 char(1)
declare @months11 char(1) 
declare @months12 char(1)  
declare @date_begins datetime
declare @date_ends datetime
declare @price_method char(1)
declare @formula_rate decimal(15, 4)
declare @cost_basis_id char(1)
declare @cust_qty_br char(1)
declare @promo_dtl_id varchar(255)
declare @formula_method_id char(1)
declare @unit_cost money
declare @avg_unit_cost decimal(17,8)
declare @total_cost decimal(17,8)

-- Declare the Promo Cursor for Item
declare promos scroll cursor for
  SELECT AR_SHIP2_PROMO.PRIORITY,
         IN_PROMO_HDR.TIMING, 
         IN_PROMO_HDR.MONTHS1, 
         IN_PROMO_HDR.MONTHS2, 
         IN_PROMO_HDR.MONTHS3, 
         IN_PROMO_HDR.MONTHS4, 
         IN_PROMO_HDR.MONTHS5, 
         IN_PROMO_HDR.MONTHS6, 
         IN_PROMO_HDR.MONTHS7, 
         IN_PROMO_HDR.MONTHS8, 
         IN_PROMO_HDR.MONTHS9, 
         IN_PROMO_HDR.MONTHS10, 
         IN_PROMO_HDR.MONTHS11, 
         IN_PROMO_HDR.MONTHS12, 
         IN_PROMO_HDR.DATE_BEGINS, 
         IN_PROMO_HDR.DATE_ENDS,
         IN_PROMO_DTL.PRICE_METHOD,
         IN_PROMO_DTL.FORMULA_RATE / 100 as FORMULA_RATE,
         IN_PROMO_DTL.COST_BASIS_ID,
         IN_PROMO_DTL.CUST_QTY_BR,
         convert(varchar(255), IN_PROMO_DTL.PROMO_DTL_ID),
         IN_PROMO_DTL.FORMULA_METHOD_ID
FROM IN_PROMO_HDR (nolock) 
                   INNER JOIN IN_PROMO_DTL  (nolock) ON IN_PROMO_HDR.PROMO_HDR_ID = IN_PROMO_DTL.PROMO_HDR_ID
                  INNER JOIN AR_SHIP2_PROMO  (nolock) ON IN_PROMO_HDR.PROMO_HDR_ID = AR_SHIP2_PROMO.PROMO_HDR_ID
WHERE AR_SHIP2_PROMO.SHIP2_ID = @ship2_id and
      IN_PROMO_DTL.ITEM_ID = @item_id
ORDER BY AR_SHIP2_PROMO.PRIORITY
  
-- Open the Promos Cursor
open promos

-- Fetch First Promo Record
fetch next from promos /*  (nolock)  */ into @priority,
                            @timing,
                            @months1,
                            @months2,
                            @months3,
                            @months4,
                            @months5,
                            @months6,
                            @months7,
                            @months8,
                            @months9,
                            @months10,
                            @months11,
                            @months12,
                            @date_begins,
                            @date_ends,
                            @price_method,
                            @formula_rate,
                            @cost_basis_id,
                            @cust_qty_br,
                            @promo_dtl_id,
                            @formula_method_id
if (@@ERROR <> 0) GOTO on_error

-- Cache promo status
select @promo_fetch = @@fetch_status

-- Loop through Promos for Item
while (@promo_fetch = 0) begin
  -- If this is the first promo found then declare the rest of the variables and retrieve data
  if @has_promo = 0 begin
    -- Declare Local Variables
    declare @today datetime
    declare @valid_promo bit
    declare @is_best_price_ok char(1)
    declare @avg_cost money
    declare @list_price money
    declare @next_cost money
    declare @std_cost money
    declare @qty_bracket smallint
    declare @amt money
    declare @rate decimal(15, 4)
    declare @bracket_fetch smallint
    declare @bracket integer
    declare @reference_price money
        
    -- Get the IsBestPriceOk and Qty Bracket
    select @is_best_price_ok = is_best_price_ok,
           @qty_bracket = qty_bracket
    from ar_ship2 (nolock) 
    where ship2_id = @ship2_id
    
    -- Get the List Price, Next Cost, Std Cost
    select @list_price = list_price,
           @next_cost = next_cost,
           @std_cost = std_cost
    from in_item_master (nolock) 
    where item_id = @item_id
    
    -- Get Avg Cost 
    select @avg_cost = avg_cost
    from in_whseitem_master (nolock) 
    where item_id = @item_id and
          whse_id = @whse_id
          
    -- Get the Reference Price
    select @reference_price = reference_price
    from in_item_ref_price (nolock) 
    where item_id = @item_id
    
    -- Get Todays Date
    select @today = getdate()

    -- Set the @has_promo bit to True 
    select @has_promo = 1 
  end
  
  -- Set the Valid Promo Bit to False
  select @valid_promo = 0
  
  -- Handle null values for date range variables
  if @date_begins is null select @date_begins = '01/01/1900'
  if @date_ends is null select @date_ends = '12/31/3000'
  
  -- Timing is D
  if @timing = 'D' begin
    if (@today >= @date_begins) and (@today <= @date_ends)
      select @valid_promo = 1
  -- Timing is P
  end else if @timing = 'P' begin
    if (@months1 = 'Y') and (@period = 1) 
      select @valid_promo = 1
    else if (@months1 = 'Y') and (@period = 1)
      select @valid_promo = 1
    else if (@months2 = 'Y') and (@period = 2)
      select @valid_promo = 1 
    else if (@months3 = 'Y') and (@period = 3)
      select @valid_promo = 1
    else if (@months4 = 'Y') and (@period = 4)
      select @valid_promo = 1
    else if (@months5 = 'Y') and (@period = 5)
      select @valid_promo = 1 
    else if (@months6 = 'Y') and (@period = 6)
      select @valid_promo = 1 
    else if (@months7 = 'Y') and (@period = 7)
      select @valid_promo = 1
    else if (@months8 = 'Y') and (@period = 8)
      select @valid_promo = 1
    else if (@months9 = 'Y') and (@period = 9)
      select @valid_promo = 1 
    else if (@months10 = 'Y') and (@period = 10)
      select @valid_promo = 1 
    else if (@months11 = 'Y') and (@period = 11)
      select @valid_promo = 1
    else if (@months12 = 'Y') and (@period = 12)
      select @valid_promo = 1   
  -- Timing is O
  end else if @timing = 'O' begin
    if @today >= @date_begins
      select @valid_promo = 1
  end
  
  -- If a Valid Promo has been found
  if @valid_promo = 1 begin
    
    -- Set @list_price equal to @avg_cost if it is not null
    if @avg_cost is not null
      select @list_price = @avg_cost  
    
    -- Price Method is Q  
    if @price_method = 'Q' begin
      -- Cust Qty Bracket is Y
      if @cust_qty_br = 'Y' begin
        select @amt = amt,
               @rate = rate / 100
        from in_promo_bracket (nolock) 
        where promo_dtl_id = @promo_dtl_id and
              bracket = @qty_bracket
      end
      -- Cust Qty Bracket is N or not match found
      if (@cust_qty_br = 'N') or (@amt is null) begin
        -- Declare the Bracket Cursor
        declare brackets scroll cursor for
          select qty,
                 amt,
                 rate / 100 as rate
          from in_promo_bracket (nolock) 
          where promo_dtl_id = @promo_dtl_id
          order by bracket
  
        -- Open the Brackets Cursor  
        open brackets
          
        -- Fetch the variables frm the Cursor
        fetch from brackets /*  (nolock)  */ into @bracket,
                                 @amt,
                                 @rate
                  
        -- Cache the Bracket Fetch Status   
        select @bracket_fetch = @@fetch_status
          
        -- Loop through the brackets until you find a bracket that is greater than the qty required                      
        while (@bracket_fetch = 0) begin
          if @qty > @bracket begin  
            fetch next from brackets /*  (nolock)  */ into @bracket,
                                          @amt,
                                          @rate
            select @bracket_fetch = @@fetch_status
          end else
            select @bracket_fetch = -1                      
        end
        
        -- Close and DeAllocate the Brackets Cursor
        close brackets
        deallocate brackets       
        
        -- Check for Null Values
        if @amt is null select @amt = 0
        if @rate is null select @rate = 0
      end
      
      --  Formula Method Id is D
      if @formula_method_id = 'D' begin
        select @best_price = @amt
      --  Formula Method Id is L
      end else if @formula_method_id = 'L' begin
        select @best_price = @list_price - (@list_price * @rate)
      --  Formula Method Id is C
      end else if @formula_method_id = 'C' begin
        -- Cost Basis Id is A
        if @cost_basis_id = 'A' begin
          -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
          exec p_cds_in_cost_layers @whse_id,  
                                    @item_id,
                                    @qty, 
                                    0,
                                    null,
                                    null,
                                    @avg_unit_cost out,
                                    @total_cost out
          select @unit_cost = @avg_unit_cost
        -- Cost Basis Id is R
        end else if @cost_basis_id = 'R' begin
          select @unit_cost = @next_cost
        -- Cost Basis Id is S
        end else if @cost_basis_id = 'S' begin
          select @unit_cost = @std_cost
        end
        
        -- Select Best Price
        select @best_price = @unit_cost + (@unit_cost * @rate)
        
      --  Formula Method Id is M
      end else if @formula_method_id = 'M' begin
        -- Cost Basis Id is A
        if @cost_basis_id = 'A' begin
          -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
          exec p_cds_in_cost_layers @whse_id,  
                                    @item_id,
                                    @qty, 
                                    0,
                                    null,
                                    null,
                                    @avg_unit_cost out,
                                    @total_cost out
          select @unit_cost = @avg_unit_cost
        -- Cost Basis Id is R
        end else if @cost_basis_id = 'R' begin
          select @unit_cost = @next_cost
        -- Cost Basis Id is S
        end else if @cost_basis_id = 'S' begin
          select @unit_cost = @std_cost
        end
        
        -- Select Best Price
        select @best_price = @unit_cost / (1 - @rate)

      --  Formula Method Id is U
      end else if @formula_method_id = 'U' begin
        select @best_price = @list_price + (@list_price * @rate)
      --  Formula Method Id is R
      end else if @formula_method_id = 'R' begin
        select @best_price = @reference_price - (@reference_price * @rate)
      --  Formula Method Id is X
      end else if @formula_method_id = 'X' begin
        select @best_price =  @list_price * @rate
      end
        
    -- Price Method is F     
    end else if @price_method = 'F' begin
      --  Formula Method Id is L
      if @formula_method_id = 'L' begin
        select @best_price = @list_price - (@list_price * @formula_rate)
      --  Formula Method Id is C
      end else if @formula_method_id = 'C' begin
        -- Cost Basis Id is A
        if @cost_basis_id = 'A' begin
          -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
          exec p_cds_in_cost_layers @whse_id,  
                                    @item_id,
                                    @qty, 
                                    0,
                                    null,
                                    null,
                                    @avg_unit_cost out,
                                    @total_cost out
          select @unit_cost = @avg_unit_cost
        -- Cost Basis Id is R
        end else if @cost_basis_id = 'R' begin
          select @unit_cost = @next_cost
        -- Cost Basis Id is S
        end else if @cost_basis_id = 'S' begin
          select @unit_cost = @std_cost
        end
        
        -- Select Best Price
        select @best_price = @unit_cost + (@unit_cost * @formula_rate)
      --  Formula Method Id is M
      end else if @formula_method_id = 'M' begin
        -- Cost Basis Id is A
        if @cost_basis_id = 'A' begin
          -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
          exec p_cds_in_cost_layers @whse_id,  
                                    @item_id,
                                    @qty, 
                                    0,
                                    null,
                                    null,
                                    @avg_unit_cost out,
                                    @total_cost out
          select @unit_cost = @avg_unit_cost
        -- Cost Basis Id is R
        end else if @cost_basis_id = 'R' begin
          select @unit_cost = @next_cost
        -- Cost Basis Id is S
        end else if @cost_basis_id = 'S' begin
          select @unit_cost = @std_cost
        end
        
        -- Select Best Price
        select @best_price = @unit_cost / (1 - @formula_rate)
      --  Formula Method Id is U
      end else if @formula_method_id = 'U' begin
        select @best_price = @list_price + (@list_price * @formula_rate)
      --  Formula Method Id is R
      end else if @formula_method_id = 'R' begin
        select @best_price = @reference_price - (@reference_price * @formula_rate)
      --  Formula Method Id is X
      end else if @formula_method_id = 'X' begin
        select @best_price =  @list_price * @formula_rate
      end
    end
  end
  
  -- Select @price if a Valid Promo
  if (@valid_promo = 1) begin
    if @price is null
      select @price = @best_price
    else if @best_price < @price
      select @price = @best_price
  end
  
  -- If Customer Gets Best Price or no valid promo yet found ...
  if (@is_best_price_ok = 'Y') or (@valid_promo = 0) begin
    -- Fetch First Promo Record
    fetch next from promos /*  (nolock)  */ into @priority,
                                @timing,
                                @months1,
                                @months2,
                                @months3,
                                @months4,
                                @months5,
                                @months6,
                                @months7,
                                @months8,
                                @months9,
                                @months10,
                                @months11,
                                @months12,
                                @date_begins,
                                @date_ends,
                                @price_method,
                                @formula_rate,
                                @cost_basis_id,
                                @cust_qty_br,
                                @promo_dtl_id,
                                @formula_method_id
    if (@@ERROR <> 0) GOTO on_error

    -- Cache promo status
    select @promo_fetch = @@fetch_status 
  end else
    select @promo_fetch = -1
end

-- Close and DeAllocate the Promos Cursor
close promos
deallocate promos       

return(0)

on_error:
return(1)
