USE [CMT]
GO
/****** Object:  StoredProcedure [dbo].[p_cds_oe_pricing_engine_wsAPI]    Script Date: 05/22/2015 18:48:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


alter    procedure [dbo].[p_cds_oe_pricing_engine_wsAPI] (
                                         @itemNumber varchar(32),
                                         @customerNumber varchar(8),
                                         @shipToNumber varchar(8) = null,
                                         @whseCode varchar(8) = null,
                                         @qty decimal(15, 4),
                                         @customer_price money out,
                                         @proc_no integer out,
                                         @error_code integer out)
as

declare @cust_id uniqueidentifier
declare @ship2_id uniqueidentifier
declare @item_id uniqueidentifier
declare @whse_id uniqueidentifier
declare @promo_item_id uniqueidentifier
declare @period int
declare @is_fractional char(1)
declare @stockaid_price money
declare @contract_price money
declare @quantity_price money
declare @product_group_price money
declare @price_column_price money
declare @promo_price money
declare @list_price money
declare @price_source char(2) 

-- Set the @proc_no and the initial @error_code
select @proc_no = 2010
select @error_code = 0

-- Initialize Price Variables
select @customer_price = 0
select @contract_price = 0
select @quantity_price = 0
select @product_group_price = 0
select @price_column_price = 0
select @promo_price = 0
select @list_price = 0

-- Initialize Price Source Variable
select @price_source = 'NF'

-- Declare Local Variables
declare @is_best_price_ok char(1)
declare @is_break_charge_ok char(1)
declare @price money
declare @best_price money
declare @first_source char(2)
declare @temp_price_source char(2)
declare @break_percentage decimal(15, 4)
declare @status_id char(1)
declare @type_code varchar(4)
declare @bracket int
declare @group_id varchar(255)

--get the cust_id of the selected customer
select @cust_id = cust_id from ar_cust (nolock) where cust_no = @customerNumber

--check to see if the Shipto was supplied. If not, get the default shipto for the customer
if (@shipToNumber is null)
  select @ship2_id = default_ship2_id from ar_cust_standard (nolock) where cust_id = @cust_id
else
  select @ship2_id = ship2_id from ar_ship2 (nolock) where cust_id = @cust_id and ship_no = @shipToNumber
  
--check to see if the whse_code was supplied. If not, get the whse_id based on the ship2_id
if(@whseCode is null)
  select @whse_id = whse_id from AR_SHIP2 (nolock) where ship2_id = @ship2_id
  
-- Retrieve the Break Percentage frm the IN_ITEM_MASTER
select @item_id = item_id,
       @status_id = status_id,
       @break_percentage = break_percentage,
       @group_id = group_id,
       @is_fractional = is_decimal
from in_item_master (nolock) 
where item_no = @itemNumber

-- Check Free Good Status ** Exit if Free Good
if @status_id = 'F' begin
  select @price_source = 'FG'
  return(0)
end

-- Check StockAid Status ** Exit if StockAid
--if @status_id = 'S' begin

--end

-- Initialize Best Price
select @best_price = 999999999999

-- Get IsBestPriceOk
select @is_best_price_ok = is_best_price_ok,
       @is_break_charge_ok = is_break_charge_ok,
       @type_code = type_code
from ar_ship2 (nolock) 
where ship2_id = @ship2_id


--ar_type_group_bracket data
select @bracket = bracket
from ar_type_group_bracket
where type_code = @type_code
and group_id = @group_id

--initialize the customer bracket to zero when never set
if @bracket is null
  set @bracket = 0
		  
-- Contract Price
exec p_cds_oe_contract_price @whse_id,
                             @item_id,
                             @ship2_id,
                             @qty,
                             @price out,
                             @proc_no out,
                             @error_code out

if @price is not null begin
  select @contract_price = @price
  
  if @first_source is null
    select @first_source = 'CO'
    
  if @contract_price < @best_price begin
    select @best_price = @contract_price
    select @price_source = 'CO'
  end 
end

-- Quantity Price
exec p_cds_oe_quantity_price @whse_id,
                             @item_id,
                             @ship2_id,
                             @qty,
                             @bracket,
                             @price out,
                             @proc_no out,
                             @error_code out
                         
if @price is not null begin
  select @quantity_price = @price
  
  if @first_source is null
    select @first_source = 'QT'
  
  if @quantity_price < @best_price begin
    select @best_price = @quantity_price
    select @price_source = 'QT'
  end 
end

-- Product Group Price
exec p_cds_oe_product_group_price @whse_id, 
								  @item_id, 
								  @ship2_id, 
								  @qty, 
								  @bracket,
								  @price out,
                                  @proc_no out,
                                  @error_code out
                         
if @price is not null and @price != 0 begin
  select @product_group_price = @price
  
  if @first_source is null
	select @first_source = 'PG'
  
  if @product_group_price < @best_price or @best_price = 0 begin
	select @best_price = @product_group_price
	select @price_source = 'PG'
  end 
end

-- PriceColumn Price
exec p_cds_oe_pricecolumn_price @whse_id,
                                @item_id,
                                @ship2_id,
                                @qty,
                                @temp_price_source out,
                                @price out,
                                @proc_no out,
                                @error_code out

if @price is not null begin
  select @price_column_price = @price
  
  if @first_source is null
    select @first_source = @temp_price_source
  

  if @price_column_price < @best_price begin
    select @best_price = @price_column_price
    select @price_source = @temp_price_source
  end 
end

-- Promo Price
if @promo_item_id is not null begin
  exec p_cds_oe_promo_price @whse_id,
                            @item_id,
                            @ship2_id,
                            @qty,
                            @price out,
                            @period,
                            @proc_no out,
                            @error_code out
                   
  if @price is not null begin
    select @promo_price = @price
  
    if @first_source is null
      select @first_source = 'PR'
  
    if @promo_price < @best_price begin
      select @best_price = @promo_price
      select @price_source = 'PR'
    end
  end 
end

-- List Price
exec p_cds_oe_list_price @item_id,
                         @whse_id,
                         @price out,
                         @proc_no out,
                         @error_code out
                     
if @price is not null begin
  select @list_price = @price
  
  if @first_source is null
    select @first_source = 'LP'
  
  if @list_price < @best_price begin
    select @best_price = @list_price
    select @price_source = 'LP'
  end 
end

-- Determine Customer Price
if @is_best_price_ok = 'Y' begin
  if @price_source is not null begin
    select @customer_price = @best_price
  end else begin
    select @price_source = 'NF'
    select @customer_price = 0
  end
end else begin
  select @price_source = @first_source
  if @first_source = 'PR'
    select @customer_price = @promo_price
  else if @first_source = 'CO'
    select @customer_price = @contract_price
  else if @first_source = 'QT'
    select @customer_price = @quantity_price
  else if @first_source = 'PG'
			select @customer_price = @product_group_price
  else if @first_source = 'PC'
    select @customer_price = @price_column_price
  else if @first_source = 'ND'
    select @customer_price = @price_column_price
  else if @first_source = 'MD'
    select @customer_price = @price_column_price
  else if @first_source = 'MM'
    select @customer_price = @price_column_price
  else if @first_source = 'LP'
    select @customer_price = @list_price
  else begin
    select @price_source = 'NF'
    select @customer_price = 0
  end
end

-- Determine Break Charge if Any
if (@is_fractional = 'Y') and (@is_break_charge_ok = 'Y') begin
  -- Reset the Customer Price to include the Break Percentage
  select @customer_price = @customer_price + (@customer_price * @break_percentage) 
end

return(0)







