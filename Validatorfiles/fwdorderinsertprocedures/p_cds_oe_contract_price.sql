USE [CMT]
GO
/****** Object:  StoredProcedure [dbo].[p_cds_oe_contract_price]    Script Date: 7/22/2015 1:55:11 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

alter    procedure [dbo].[p_cds_oe_contract_price] @whse_id varchar(255),
                                           @item_id varchar(255),
                                           @ship2_id varchar(255),
                                           @qty decimal(15, 4),
                                           @price money out,
                                           @proc_no integer out,
                                           @error_code integer out

as

-- Set the @proc_no and the initial @error_code
select @proc_no = 2002
select @error_code = 0
select @price = null

-- Declare Fetch Variables
declare @contract_id varchar(255)
declare @contract_dtl_id varchar(255)
declare @priority smallint
declare @contract_begin datetime
declare @contract_end datetime
declare @header_begin datetime
declare @header_end datetime
declare @detail_begin datetime
declare @detail_end datetime
declare @contract_price money
declare @method char(1)
declare @calc_base_id char(1)
declare @calc_rate decimal(15, 4)
declare @cost_basis_id char(1)
declare @cust_bracket_used char(1)
declare @family_disc_ok char(1)
declare @best_price money
declare @ship2_cust_id uniqueidentifier
declare @avg_unit_cost decimal(17,8)
declare @total_cost decimal(17,8)


-- Declare a variable to set whether a contract has been found
declare @has_contract bit

-- Initialize the variable to indicate no contracts have yet been found
select @has_contract = 0

-- Declare a variable to hold the contract fetch status
declare @contract_fetch smallint

-- Get the cust_id from ar_ship2 based on the passed in @ship2_id
select @ship2_cust_id = cust_id from ar_ship2 where ship2_id = @ship2_id

-- Declare the Contracts Cursor for Item
declare contracts scroll cursor for
  select convert(varchar(255), AR_SHIP2_CONTRACT.CONTRACT_ID) as CONTRACT_ID,
         convert(varchar(255), OE_CONTRACT_DTL.CONTRACT_DTL_ID) as CONTRACT_DTL_ID,
         AR_SHIP2_CONTRACT.PRIORITY,  
         AR_SHIP2_CONTRACT.BEGIN_DATE as CONTRACT_BEGIN, 
         AR_SHIP2_CONTRACT.END_DATE as CONTRACT_END, 
         OE_CONTRACT_HDR.BEGIN_DATE as HEADER_BEGIN, 
         OE_CONTRACT_HDR.END_DATE as HEADER_END, 
         OE_CONTRACT_DTL.BEGIN_DATE as DETAIL_BEGIN, 
         OE_CONTRACT_DTL.END_DATE as DETAIL_END, 
         OE_CONTRACT_DTL.CONTRACT_PRICE, 
         OE_CONTRACT_DTL.METHOD, 
         OE_CONTRACT_DTL.CALC_BASE_ID, 
         OE_CONTRACT_DTL.CALC_RATE/100 AS CALC_RATE, 
         OE_CONTRACT_DTL.COST_BASIS_ID, 
         OE_CONTRACT_DTL.CUST_BRACKET_USED, 
         OE_CONTRACT_DTL.FAMILY_DISC_OK
  from AR_SHIP2_CONTRACT  (nolock) 
                         inner join OE_CONTRACT_HDR  (nolock) on AR_SHIP2_CONTRACT.CONTRACT_ID = OE_CONTRACT_HDR.CONTRACT_ID
                         inner join OE_CONTRACT_DTL (nolock)  on OE_CONTRACT_HDR.CONTRACT_ID = OE_CONTRACT_DTL.CONTRACT_ID
  where (AR_SHIP2_CONTRACT.CUST_ID = @ship2_cust_id AND 
         SHIP2_ID IS NULL AND 
         (OE_CONTRACT_DTL.ITEM_ID = @item_id or 
          oe_contract_dtl.GROUP_ID = (select GROUP_ID from IN_ITEM_MASTER i (nolock) where i.ITEM_ID = @item_id)))
     or  (AR_SHIP2_CONTRACT.SHIP2_ID = @ship2_id and 
          (OE_CONTRACT_DTL.ITEM_ID = @item_id or 
           oe_contract_dtl.GROUP_ID = (select GROUP_ID from IN_ITEM_MASTER i (nolock) where i.ITEM_ID = @item_id)))
  order by AR_SHIP2_CONTRACT.PRIORITY,
    OE_CONTRACT_DTL.CONTRACT_PRICE
 
-- Open the Contracts Cursor
open contracts
 
-- Fetch First Contract Record
fetch next from contracts /*   (nolock)  */ into @contract_id,
                               @contract_dtl_id,
                               @priority,
                               @contract_begin,
                               @contract_end,
                               @header_begin,
                               @header_end,
                               @detail_begin,
                               @detail_end,
                               @contract_price,
                               @method,
                               @calc_base_id,
                               @calc_rate,
                               @cost_basis_id,
                               @cust_bracket_used,
                               @family_disc_ok
if (@@ERROR <> 0) GOTO on_error

-- Cache contract fetch status
select @contract_fetch = @@fetch_status

-- Loop through Contracts for Item
while (@contract_fetch = 0) begin
  -- If this is the first contract found then declare the rest of the variables and retrieve data
  if @has_contract = 0 begin
    -- Declare Local Variables
    declare @today datetime
    declare @cust_id varchar(255)
    declare @group_id varchar(255)
    declare @unit_cost money
    declare @is_best_price_ok char(1)
    declare @valid_contract bit
    declare @void_price char(1)
    declare @list_price money
    declare @next_cost money
    declare @std_cost money
    declare @reference_price money
    declare @discount_rate decimal(15, 4)
    declare @add_on_amt money
    declare @amt money 
    declare @qty_bracket smallint
    declare @bracket decimal(15, 4)
    declare @bracket_fetch smallint
    
    -- Declare variables to hold min and max dates
    declare @min_date datetime
    declare @max_date datetime
    
    -- Get Todays Date
    select @today = getdate()

    -- Get the Cust Id, IsBestPriceOk and Qty Bracket
    select @cust_id = cust_id,
           @is_best_price_ok = is_best_price_ok,
           @qty_bracket = qty_bracket
    from ar_ship2 (nolock) 
    where ship2_id = @ship2_id

    -- Get the Group Id, List Price, Next Cost, Std Cost
    select @group_id = group_id,
           @list_price = list_price,
           @next_cost = next_cost,
           @std_cost = std_cost
    from in_item_master (nolock) 
    where item_id = @item_id

    -- Get the Reference Price
    select @reference_price = reference_price
    from in_item_ref_price (nolock) 
    where item_id = @item_id
    
    -- Set the @has_contract bit to True 
    select @has_contract = 1 
  end
  
  -- Set the Valid Contract Bit to False
  select @valid_contract = 0
  
  -- Handle null values for date range variables
  if @contract_begin is null select @contract_begin = '01/01/1900'
  if @contract_end is null select @contract_end = '12/31/3000' 
  if @header_begin is null select @header_begin = '01/01/1900'
  if @header_end is null select @header_end = '12/31/3000' 
  if @detail_begin is null select @detail_begin = '01/01/1900'
  if @detail_end is null select @detail_end = '12/31/3000' 
   
  -- Determine Min Date for Contract
  select @min_date = '01/01/1900'
  if @contract_begin > @min_date select @min_date = @contract_begin
  if @header_begin > @min_date select @min_date = @header_begin
  if @detail_begin > @min_date select @min_date = @detail_begin
  
  -- Determine Max Date for Contract
  select @max_date = '12/31/3000'
  if @contract_end < @max_date select @max_date = @contract_end
  if @header_end < @max_date select @max_date = @header_end
  if @detail_end < @max_date select @max_date = @detail_end
  
  -- Check to see if the contract is within date range
  if @today >= @min_date and @today <= @max_date 
  begin
    -- Check for Quantity Contract. Assign Contract Price to Best Price
    exec p_cds_in_qty_contract @contract_id, 
                               @cust_id, 
                               @ship2_id, 
                               @item_id, 
                               @best_price out, 
                               @void_price out, 
                               @proc_no out, 
                               @error_code out
                         
    -- If Best Price has been found in a Qty Contract then set Valid Contract to True
    if (@best_price is not null) 
    begin
      select @valid_contract = 1
    -- else only continue if void_price is false (or null) and best price has not been found in a Qty Contract
    end else if (@void_price = 'N') or (@void_price is null) 
    begin
      -- Set the valid_contract bit to True
      select @valid_contract = 1
      -- Method is F
      if @method = 'F' 
      begin
        select @best_price = @contract_price
      -- Method is M
      end else if @method = 'M' 
      begin
        select @best_price = @contract_price
      -- Method is V
      end else if @method = 'V' 
      begin
        -- Calc Base Id is L
        if @calc_base_id = 'L' 
        begin
          select @best_price = @list_price - (@list_price * (@calc_rate))
        -- Calc Base Id is C
        end else if @calc_base_id = 'C' 
        begin
          -- Cost Basis Id is A
          if @cost_basis_id = 'A' 
          begin
            -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
            exec p_cds_in_cost_layers @whse_id,  
                                      @item_id,
                                      @qty, 
                                      0,
                                      null,
                                      null,
                                      @avg_unit_cost out,
                                      @total_cost out
             select @unit_cost = @avg_unit_cost
          -- Cost Basis Id is R
          end else if @cost_basis_id = 'R' 
          begin
            select @unit_cost = @next_cost
          -- Cost Basis Id is S
          end else if @cost_basis_id = 'S' 
          begin
            select @unit_cost = @std_cost
          end
          -- Set the Best Price equal to unit cost + percentage
          select @best_price = @unit_cost + (@unit_cost * (@calc_rate))
        -- Calc Base Id is M
        end else if @calc_base_id = 'M' 
        begin
        -- Cost Basis Id is A
          if @cost_basis_id = 'A' 
          begin
            -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
               exec p_cds_in_cost_layers @whse_id,     
                                         @item_id, 
                                         @qty, 
                                         0,
                                         null,
                                         null,
                                         @avg_unit_cost out,
                                         @total_cost out
                                                                                                       
             select @unit_cost = @avg_unit_cost
          -- Cost Basis Id is R
          end else if @cost_basis_id = 'R' 
          begin
            select @unit_cost = @next_cost
          -- Cost Basis Id is S
          end else if @cost_basis_id = 'S' 
          begin
            select @unit_cost = @std_cost
          end
          -- Set the Best Price
          select @best_price = @unit_cost / (1 - (@calc_rate))
        -- Calc Base Id is U
        end else if @calc_base_id = 'U' 
        begin
          -- Set the Best Price
          select @best_price = @list_price + (@list_price * (@calc_rate))
        -- Calc Base Id is R
        end else if @calc_base_id = 'R' 
        begin
          select @best_price = @reference_price - (@reference_price * (@calc_rate))  
        end
      -- Method is Q
      end else if @method = 'Q' 
      begin
        -- Assigned Qty Bracket
        if (@cust_bracket_used = 'Y') and (@ship2_id is not null) 
        begin
          select @amt = amt,
                 @discount_rate = disc_rate/100,
                 @add_on_amt = add_on_amt
          from oe_contract_dtl_qty (nolock) 
          where contract_dtl_id = @contract_dtl_id and
                bracket = @qty_bracket
        -- If not Assigned Qty Bracket then Get Matching Bracket for Qty
        end else 
        begin
          -- Declare the Bracket Cursor
          declare brackets scroll cursor for
            select qty,
                   amt,
                   disc_rate/100 as disc_rate,
                   add_on_amt
            from oe_contract_dtl_qty (nolock) 
            where contract_dtl_id = @contract_dtl_id
            order by bracket
          
          -- Open the Brackets Cursor  
          open brackets
          
          -- Fetch the variables frm the Cursor
          fetch from brackets /*   (nolock)  */ into @bracket,
                                   @amt,
                                   @discount_rate,
                                   @add_on_amt
          
          -- Cache the Bracket Fetch Status   
          select @bracket_fetch = @@fetch_status
          
          -- Loop through the brackets until you find a bracket that is greater than the qty required                      
          while (@bracket_fetch = 0) 
          begin
            if @qty > @bracket 
            begin  
              fetch next from brackets /*   (nolock)  */ into @bracket,
                                            @amt,
                                            @discount_rate,
                                            @add_on_amt
              select @bracket_fetch = @@fetch_status
            end else
              select @bracket_fetch = -1                      
          end
        end
        
        -- Close and DeAllocate the Brackets Cursor
        close brackets
        deallocate brackets       
        
        -- Check for Null Values
        if @amt is null select @amt = 0
        if @discount_rate is null select @discount_rate = 0
        if @add_on_amt is null select @add_on_amt = 0
        
        -- Calc Base Id is L
        if @calc_base_id = 'D' 
        begin
          select @best_price = @amt
        -- Calc Base Id is L
        end else if @calc_base_id = 'L' 
        begin
          select @best_price = @list_price - (@list_price * @discount_rate) + @add_on_amt
        -- Calc Base Id is C
        end else if @calc_base_id = 'C' 
        begin
          -- Cost Basis Id is A
          if @cost_basis_id = 'A' 
          begin
            -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
            exec p_cds_in_cost_layers @whse_id,    
                                      @item_id, 
                                      @qty, 
                                      0,
                                      null,
                                      null,
                                      @avg_unit_cost out,
                                      @total_cost out
             select @unit_cost = @avg_unit_cost
          -- Cost Basis Id is R
          end else if @cost_basis_id = 'R' 
          begin
            select @unit_cost = @next_cost
          -- Cost Basis Id is S
          end else if @cost_basis_id = 'S' 
          begin
            select @unit_cost = @std_cost
          end
          -- Set the Best Price
          select @best_price = @unit_cost + (@unit_cost * @discount_rate) + @add_on_amt
        -- Calc Base Id is M
        end else if @calc_base_id = 'M' 
        begin
          -- Cost Basis Id is A
          if @cost_basis_id = 'A' 
          begin
            -- Get the unit cost frm the cost layers (best price will hold the unit_cost times the qty)
            exec p_cds_in_cost_layers @whse_id,    
                                      @item_id, 
                                      @qty, 
                                      0,
                                      null,
                                      null,
                                      @avg_unit_cost out,
                                      @total_cost out
             select @unit_cost = @avg_unit_cost
          -- Cost Basis Id is R
          end else if @cost_basis_id = 'R' 
          begin
            select @unit_cost = @next_cost
          -- Cost Basis Id is S
          end else if @cost_basis_id = 'S' 
          begin
            select @unit_cost = @std_cost
          end
          -- Set the Best Price
          select @best_price = (@unit_cost / (1 - @discount_rate)) + @add_on_amt
        -- Calc Base Id is U
        end else if @calc_base_id = 'U' 
        begin
          select @best_price = @list_price + (@list_price * @discount_rate) + @add_on_amt
        -- Calc Base Id is R
        end else if @calc_base_id = 'R' 
        begin
          select @best_price = @reference_price + (@reference_price * @discount_rate) + @add_on_amt
        end 
      end
    end 
  end
  
  -- Select @price if a Valid Contract
  if (@valid_contract = 1) 
  begin
    if @price is null
      select @price = @best_price
    else if @best_price < @price
      select @price = @best_price
  end
  
  -- If Customer Gets Best Price or no valid contract yet found ...
  if (@is_best_price_ok in ('Y', 'X')) or (@valid_contract = 0) 
  begin
    -- Fetch Next Contract Record
    fetch next from contracts /*   (nolock)  */ into @contract_id,
                                   @contract_dtl_id,
                                   @priority,
                                   @contract_begin,
                                   @contract_end,
                                   @header_begin,
                                   @header_end,
                                   @detail_begin,
                                   @detail_end,
                                   @contract_price,
                                   @method,
                                   @calc_base_id,
                                   @calc_rate,
                                   @cost_basis_id,
                                   @cust_bracket_used,
                                   @family_disc_ok
    if (@@ERROR <> 0) GOTO on_error
			                     
    -- Cache contract fetch status
    select @contract_fetch = @@fetch_status
  end else
    select @contract_fetch = -1
end

-- Close and DeAllocate the Contracts Cursor
close contracts
deallocate contracts         
 
return(0)

on_error:
return(1)