USE [CMT]
GO
/****** Object:  StoredProcedure [dbo].[p_CDSCreateSalesOrderHdr]    Script Date: 06/18/2015 14:37:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter Procedure [dbo].[p_CDSCreateSalesOrderHdr](
											 @OEDocID uniqueidentifier,
											 @CustID uniqueidentifier,
											 @ShipToID uniqueidentifier,
											 @CustDeptID uniqueidentifier = null,
											 @WhseId uniqueidentifier = null,
											 @SubOrderType char(1) = null,
											 @WillCallDate datetime = null,
											 @SourceCode varchar(4) = null,
											 @CustPO varchar(20),
											 @OrderDate datetime = null,
											 @UserID uniqueidentifier = null,
											 @ReleaseNo varchar(14) = null,
											 @COMPANY_ID uniqueidentifier,
											 @OrderedBy varchar(255) = null,
											 @EarliestShipDate datetime = null,
											 @LatestShipDate datetime = null,
											 @Carrier varchar(10) = null,
											 @ServiceCode varchar(4) = null,
											 @Instructions varchar(40) = null,
											 @UPLOAD_STATUS int output
											)
AS

declare @CompanyID int
declare @DocNo int
declare @ReturnCode int
declare @EarlyShipDays int

BEGIN TRY

	set @UPLOAD_STATUS = 0
	
	select @WhseId = whse_id from IN_WAREHOUSE (nolock) where WHSE_ID = @WhseId
	
	select @WhseId = isnull(@WhseId, whse_id), @EarlyShipDays = days_to_ship
	from ar_ship2
	where ship2_id = @ShipToID

	if(@WhseId is null)
	begin
	  SET @UPLOAD_STATUS = 1
	  RETURN @UPLOAD_STATUS
	end
		
	--exec p_ADCCoDocControl2NextDocNo 'ORDER', @DocNo out
	
	if(@DocNo < 0)
	begin
	  SET @UPLOAD_STATUS = 2
	  RETURN @UPLOAD_STATUS
	end

	--check to see if the order already exists and return @UPLOAD_STATUS = 3 if it does
	if exists (select * from oe_doc_hdr where oe_doc_id = @OEDocID)
	BEGIN
	  SET @UPLOAD_STATUS = 3
	  RETURN @UPLOAD_STATUS
	END
	
	--Inserts a new record in oe_doc_hdr
	insert oe_doc_hdr
	(OE_DOC_ID, DOC_CODE, DOC_NO, SHIP2_ID, SHIPPING_WHSE_ID, ORIG_WHSE_ID, SUB_ORDER_TYPE, CUST_ID, COMMIT_TODAY, ROUTE, ROUTE_STOP, ORD_DATE, EARLY_SHIP_DATE, 
	 SHIP_BY_DATE, CANCELS_DATE, STATUS_ID, BUILD_EDI, REVISION_NO, IS_BACKORDER, USE_SUMMARY_BILL, TERMS_ID, FLAG_FRTIN, FLAG_FRTOUT, ENTRY_USER_ID, MIN_ORDER_UPCHARGE, 
	 RESTOCK_PERCENTAGE, INSTRUCTIONS, PROFIT_CTR_ID, FAX_NO, CARRIER_ID, BOX_COUNT, LAST_CHANGE_USER_ID, FOB_DEST, CURRENCY, EXCHANGE_RATE, WILL_CALL, POS_AMT, 
	 POS_DISCOUNT_AMT, POS_ACTION, CUST_PO, FRT_CTRL_ID, FRT_ALLOWANCE, CUST_DEPT_ID, CUST_REL_NO, ZONE_CODE, TERRITORY_CODE, IS_TAXABLE, IS_LOT_PRICE_PRINTED, 
	 BACKORDER_CTRL, FRT_IN_AMT, FRT_OUT_AMT, COD_PLUS_AMT, SHIP_COD_FEE, SHIP_INSUR_FEE, SHIP_HANDLING_FEE, IS_SURCHARGE_TAXABLE, INSIDE_SALESREP_ID, 
	 OUTSIDE_SALESREP_ID, SERVICE_CODE_ID, AR_PRICE_COL_ID, ORDERED_BY, SOURCE_CODE, SURCHARGE, COMMIT_TODAY_DATE, CHG_FRT_ON_BACKORDERS, IS_SUB_OK, CUST_INSTANT_DISC, 
	 IS_SHIP_COMPLETE, FREEZE_UPCHARGE, FREEZE_SURCHARGE, FREEZE_FREIGHT, FREEZE_HANDLING, FREEZE_ALLOWANCE, CREDIT_LIMIT_BALANCE, LAST_CHANGE_DATE)
	select
		@OEDocID as OE_DOC_ID, 
		'B' as DOC_CODE, 
		@DocNo as DOC_NO, 
		AR_SHIP2.SHIP2_ID as  SHIP2_ID, 
		AR_SHIP2.WHSE_ID as SHIPPING_WHSE_ID, 
		AR_SHIP2.WHSE_ID as ORIG_WHSE_ID, 
		@SubOrderType as SUB_ORDER_TYPE, 
		AR_SHIP2.CUST_ID as CUST_ID, 
		case when isnull(datediff(dd, getdate(), @EarliestShipDate), ar_ship2.DAYS_TO_SHIP) <= 0 then 'Y' else 'N' end as COMMIT_TODAY, 
		ar_ship2.ROUTE_CODE as ROUTE, 
		AR_SHIP2.ROUTE_STOP as ROUTE_STOP, 
		isnull(@OrderDate, dbo.enspire_date(getdate())) as ORD_DATE, 
		isnull(@earliestShipDate, dbo.enspire_date(getdate()) + AR_SHIP2.DAYS_TO_SHIP) as EARLY_SHIP_DATE, 
		isnull(@LatestShipDate, dbo.enspire_date(getdate()) + AR_SHIP2.LATEST_DAYS_TO_SHIP) as SHIP_BY_DATE,  
		isnull(@OrderDate, dbo.enspire_date(getdate())) + ar_ship2.DAYS_TO_CANCEL as CANCELS_DATE,  
		'E' as STATUS_ID,  
		'N' as BUILD_EDI, 
		@ReleaseNo as REVISION_NO, 
		'N' as IS_BACKORDER, 
		AR_SHIP2.IS_SUMMARY_BILLED as USE_SUMMARY_BILL,  
		AR_SHIP2.AR_TERMS_ID as TERMS_ID, 
		case when AR_SHIP2.FRT_CTRL in ('N', 'P') then 'N' else 'Y' end as FLAG_FRTIN, 
		case when AR_SHIP2.FRT_CTRL in ('N', 'P') then 'N' else 'Y' end as FLAG_FRTOUT, 
		@UserID as ENTRY_USER_ID, 
		0 as MIN_ORDER_UPCHARGE, 
		ar_ship2.RESTOCK_RATE as RESTOCK_PERCENTAGE, 
		ar_ship2.COMMENTS as INSTRUCTIONS, 
		IN_WAREHOUSE.PROFIT_CTR_ID as PROFIT_CTR_ID, 
		ar_ship2.FAX_NO as FAX_NO, 
		isnull(dbo.carrier_id(@carrier),ar_ship2.BILL_VIA_CARRIER_ID) as CARRIER_ID, 
		0 as BOX_COUNT, 
		@userId as LAST_CHANGE_USER_ID,  
		ar_ship2.FOB_DEST as FOB_DEST, 
		'US' as CURRENCY, 
		1 as EXCHANGE_RATE, 
		@WillCallDate as WILL_CALL, 
		0 as POS_AMT, 
		0 as POS_DISCOUNT_AMT, 
		'N' as POS_ACTION, 
		@CustPO as CUST_PO, 
		AR_SHIP2.FRT_CTRL as FRT_CTRL_ID, 
		0 as FRT_ALLOWANCE, 
		@CustDeptID as CUST_DEPT_ID, 
		@ReleaseNo as CUST_REL_NO, 
		ar_ship2.SHIP_ZONE as ZONE_CODE, 
		ar_ship2.TERRITORY_CODE as TERRITORY_CODE, 
		ar_ship2.IS_TAXABLE as IS_TAXABLE, 
		ar_ship2.IS_LOT_PRICED as IS_LOT_PRICE_PRINTED, 
		ar_ship2.BACKORDER_CTRL_ID as BACKORDER_CTRL, 
		0 as FRT_IN_AMT, 
		0 as FRT_OUT_AMT, 
		0 as COD_PLUS_AMT, 
		0 as SHIP_COD_FEE, 
		0 as SHIP_INSUR_FEE, 
		0 as SHIP_HANDLING_FEE, 
		ar_ship2.IS_TAXABLE as IS_SURCHARGE_TAXABLE, 
		ar_ship2.INSIDE_SALESREP_ID as INSIDE_SALESREP_ID, 
		ar_ship2.outside_salesrep_id as OUTSIDE_SALESREP_ID, 
		isnull(dbo.carrier_service_code_id(@Carrier, @ServiceCode), dbo.carrier_service_code_id(dbo.carrier_code(AR_SHIP2.BILL_VIA_CARRIER_ID), AR_SHIP2.CARRIER_SERVICE_CODE)) as SERVICE_CODE_ID, 
		ar_ship2.AR_PRICE_COL_ID as AR_PRICE_COL_ID, 
		@OrderedBy as ORDERED_BY, 
		@SourceCode as SOURCE_CODE, 
		0 as SURCHARGE, 
		case when isnull(datediff(dd, getdate(), @EarliestShipDate), ar_ship2.DAYS_TO_SHIP) <= 0 then dbo.enspire_datetime(GETDATE()) else null end as COMMIT_TODAY_DATE, 
		'N' as CHG_FRT_ON_BACKORDERS,  
		ar_ship2.IS_SUB_OK as IS_SUB_OK, 
		0 as CUST_INSTANT_DISC, 
		AR_SHIP2.IS_SHIP_COMPLETE as IS_SHIP_COMPLETE, 
		'N' as FREEZE_UPCHARGE, 
		'N' as FREEZE_SURCHARGE, 
		'N' as FREEZE_FREIGHT, 
		'N' as FREEZE_HANDLING, 
		'N' as FREEZE_ALLOWANCE, 
		0 as CREDIT_LIMIT_BALANCE, 
		dbo.enspire_date(getdate()) as LAST_CHANGE_DATE
    from AR_SHIP2 (nolock)
    inner join IN_WAREHOUSE (nolock) on AR_SHIP2.WHSE_ID = IN_WAREHOUSE.WHSE_ID
    where AR_SHIP2.SHIP2_ID = @ShipToID

END TRY
BEGIN CATCH
	SET @UPLOAD_STATUS = ERROR_NUMBER()
	RETURN @UPLOAD_STATUS
END CATCH






















