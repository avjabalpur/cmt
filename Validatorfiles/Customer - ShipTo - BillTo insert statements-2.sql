
USE [CMT]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

alter      PROCEDURE p_ar_ship2_insert
AS

declare @cust_id varchar(255)
declare @cust_no varchar(8)
declare @cust_name varchar(50)
declare @ship2_id varchar(255)
declare @ship_no varchar(8)
declare @ship2_name varchar(50)

begin tran

declare my_cursor scroll cursor        		--create a cursor for the ar_cust_standard table where no ship2 exists
  for select ar_cust_standard.cust_id,
	ar_cust.cust_no,
	ar_cust.cust_name
       from ar_cust_standard  (nolock) 
       inner join ar_cust   (nolock) on (ar_cust_standard.cust_id = ar_cust.cust_id)
       where ar_cust_standard.default_ship2_id is null
    if (@@ERROR <> 0) GOTO on_error

open my_cursor

fetch next from my_cursor /*  (nolock)  */  into @cust_id,
			@cust_no,
			@cust_name

while (@@fetch_status=0) begin			--start looping thru the records

 select @ship2_id = newid()
 
 if len(@cust_no) = 8
   select @ship_no = substring(@cust_no, 1, 7) + '1'
 else 
   select @ship_no = @cust_no + '1'

  select @ship2_name = @cust_name + ' -1'

 insert into ar_ship2
  	(ship2_id,
	cust_id,
	ship_no,
	ship2_name,
	status_id,
	date_created,
	inside_salesrep_id,
	outside_salesrep_id,
	whse_id,
	ar_terms_id,
	days_to_cancel,
	days_to_ship,
	min_order_amt,
	language_code,
	single_order_limit,
	surcharge_rate,
	next_po_rel_no,
	is_pick_ticket_priced,
	is_pack_slip_priced,
	is_acknow_priced,
	qty_bracket,
	container_bracket,
	backorder_ctrl_id,
	usage_tracking_id,
	frt_ctrl_id,
	subtotal_ctrl_id,
	frt_allowance_id,
	is_comment_print_picktkt,
	is_comment_print_packslip,
	is_comment_print_acknow,
	is_comment_print_invoice,
	is_comment_print_quote,
	cod_method_id,
	is_job,
	is_residence,
	is_licensed,
	is_lot_priced,
	is_taxable,
	is_summary_billed,
	is_best_price_ok,
	is_sub_ok,
	is_po_required,
	is_restock_fee_ok,
	is_ship_complete,
	is_save_msg_ok,
	is_break_charge_ok,
	is_surcharge_ok,
	is_co_check_ok,
	is_family_disc_ok,
	is_special_order_ok,
	fob_dest,
	is_job_usage_purger,
	save_msg_min,
	source_code,
	route_code,
	territory_code,
	type_code,
	sic_code,
	ar_price_col_id)
 values
	(@ship2_id,
	@cust_id,
	@ship_no,		
	@ship2_name,		
	'O',
	getdate(),
	'0DBFFF54-CC4B-11D2-8FCA-00105ACE5EFC',		
	'7B098BB7-78F1-11D2-B19D-00104B973ACF',	
	'A732D654-8ABB-11D2-A6A4-00104B60C0C1',			
	'06C00DF4-690F-11D2-A662-00104B60C0C1',
	0,
	0,
	10,
	'ENG',
	100000,
	0,
	1,
	'N',
	'N',
	'N',
	1,
	1,
	'Y',
	'N',
	'A',
	'N',
	'N',
	'N',
	'N',
	'N',
	'N',
	'N',
	'B',
	'N',
	'N',
	'N',
	'N',
	'Y',
	'N',
	'N',
	'Y',
	'N',
	'Y',
	'N',
	'N',
	'Y',
	'N',
	'Y',
	'Y',
	'Y',
	'N',
	'N',
	2,
	'SRC1',
	'RTE',
	'TER',
	'TYP',
	'SIC1',
	'A111D111-1ABB-11D2-A6A4-00104B60C0C1')
 if(@@ERROR <>0) GOTO on_error

 update ar_cust_standard
 set default_ship2_id = @ship2_id
 where cust_id = @cust_id 
 if(@@ERROR <>0) GOTO on_error

 fetch next from my_cursor /*  (nolock)  */  into @cust_id,
			@cust_no,
			@cust_name

end								--end batch loop

commit tran

close my_cursor
deallocate my_cursor

return(0)

on_error:
rollback tran
close my_cursor
deallocate my_cursor
return(1)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


--Created By:	Johnny Smith
--Company:	CenGen Technologies, Inc.
--Date:		12/12/2006
--Test Case:
/*

   exec p_AR_CUST_Select_CUST_ID '3M',----cust_no
                           
*/
--Change Log:
/*
	Date		Name			Description of Change

*/

alter  PROC p_AR_CUST_Select_CUST_ID(@CUST_NO varchar(8), @CUST_ID uniqueidentifier out)
AS
BEGIN
	SELECT @CUST_ID = cust_id FROM ar_cust WHERE cust_no = @CUST_NO
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO









--Created By:	Johnny Smith
--Company:	CenGen Technologies, Inc.
--Date:		1/17/2006
--Test Case:
/*

   exec p_AR_BillTo_Insert 'D7150654-47C4-4D36-8F93-05D0173E3A33',---cust_id
                           'CENGEN HOLDINGS',------------------------bill_to_name
                           '1000 W. Main Street',--------------------bill_to_address1
                           'Suite 1225',-----------------------------bill_to_address2
                           '0F9DA579-E67A-49A2-A90F-F0C4FE2BD66F',---bill_to_zip_code_dtl_id
                           '1252',-----------------------------------bill_to_zip4
                           '6825189251',-----------------------------bill_to_tel_no
                           '6825185585',-----------------------------bill_to_fax_no
                           'www.cengentech.com,----------------------bill_to_web_address
                           'jsmith@cengentech.com',------------------bill_to_email_address

*/
--Change Log:
/*
	Date		Name			Description of Change

*/

alter procedure  p_AR_BillTo_Insert(@CUST_ID uniqueidentifier,
                                     @BILL_TO_NAME varchar(50),
                                     @BILL_TO_ADDRESS1 varchar(100),
                                     @BILL_TO_ADDRESS2 varchar(100),
                                     --@ZIP_CODE varchar(14),
                                     --@STATE_CODE varchar(2),
                                     --@CITY varchar(50),
                                     @BILL_TO_ZIP_CODE_DTL_ID uniqueidentifier,
                                     @BILL_TO_ZIP4 varchar(4),
                                     @BILL_TO_TEL_NO varchar(16),
                                     @BILL_TO_FAX_NO varchar(16),
                                     @BILL_TO_WEB_ADDRESS varchar (255),
                                     @BILL_TO_EMAIL_ADDRESS varchar(255))


as
begin

declare @ADDRESS varchar(255)

set @ADDRESS = dbo.append_strings(@BILL_TO_ADDRESS1, @BILL_TO_ADDRESS2, 'Y')

	insert ar_bill_to
	(CUST_ID, 
	 BILL_TO_NAME,
	 ADDR,
	 ZIP_CODE_DTL_ID,
	 ZIP4,
	 TEL_NO,
	 FAX_NO,
	 WEB_ADDRESS,
	 EMAIL_ADDRESS)
	select
	  @CUST_ID as cust_id,
	  @BILL_TO_NAME as bill_to_name,
          @ADDRESS as addr,
	  @BILL_TO_ZIP_CODE_DTL_ID as zip_code_dtl_id,
	  @BILL_TO_ZIP4,
	  @BILL_TO_TEL_NO,
	  @BILL_TO_FAX_NO,
	  @BILL_TO_WEB_ADDRESS,
	  @BILL_TO_EMAIL_ADDRESS
        where not exists (select b.BILL_TO_NAME
                          from ar_bill_to b (nolock)
                          where b.cust_id = @CUST_ID
                            and b.bill_to_name = @BILL_TO_NAME)

end





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




--Name:		Johnny Smith
--Company:	CenGen Technologies, Inc.
--Date:		02/20/2006
--Description:	Inserts new ar_ship2_depts records

/*

Change Log:


Test Case:

	

	declare @Ship2ID uniqueidentifier
        declare @DeptID uniqueidentifier
	declare @DEPT_ID uniqueidentifier

	select @Ship2ID = ship2_id from ar_ship2 where cust_id = dbo.cust_id('3M')

	exec p_AR_ShipTo_Dept_Insert 'CORP', @Ship2ID, 'Corporate', 'Johnny Smith', '6825189251', '6825185806', 'Y', 'jsmith@cengentech.com', @DEPT_ID

	delete from ar_ship2_depts where cust_dept_id = 'A5F3EDAD-EE6E-483B-9193-E2369EC31701'

*/
alter      PROCEDURE p_AR_ShipTo_Dept_Insert (@DEPT varchar(12),
                                               @SHIP2_ID uniqueidentifier,
                                               @DESCRIPTION varchar(25),
                                               @ATTN varchar(25),
                                               @TEL_NO varchar(16),
                                               @FAX_NO varchar(16),
                                               @IS_MAILED char(1),
                                               @EMAIL_ADDRESS varchar(255),
                                               @DEPT_ID uniqueidentifier out)
AS

declare @CUST_DEPT_ID uniqueidentifier

	select @DEPT_ID = newid()

	insert ar_ship2_depts
	(CUST_DEPT_ID, DEPT, SHIP2_ID, [DESCRIPTION], ATTN, TEL_NO, FAX_NO, IS_MAILED, EMAIL_ADDRESS)
        values(@DEPT_ID, @DEPT, @SHIP2_ID, @DESCRIPTION, @ATTN, @TEL_NO, @FAX_NO, @IS_MAILED, @EMAIL_ADDRESS)




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO







--Created By:	Johnny Smith
--Company:	CenGen Technologies, Inc.
--Date:		12/15/2006
--Test Case:
/*

   exec p_AR_CUST_STANDARD_Insert '235E90EA-E993-4174-A56B-5088E123797B',----template_cust_id
                                  'D7150654-47C4-4D36-8F93-05D0173E3A33',----cust_id
                                  '0CEB9D71-25EB-45E8-8789-BC3399A2C62E'-----ship2_id
                             

delete from ar_cust where cust_name = 'XXXYYYZZZ Test Company'
select * from ar_cust where cust_no = '3M'
select * from ar_cust where cust_name = 'XYZCUST DISTR CO'
select * from ar_ship2 where cust_id = 'D7150654-47C4-4D36-8F93-05D0173E3A33'
*/
--Change Log:
/*
	Date		Name			Description of Change

*/

alter  procedure  p_AR_CUST_STANDARD_Insert(@TEMPLATE_CUST_ID uniqueidentifier,
                                             @CUST_ID uniqueidentifier,
                                             @SHIP2_ID uniqueidentifier)


as
begin
	insert ar_cust_standard
	(CUST_ID, DATE_LAST_PMT, LAST_PMT_AMT, LAST_CHECK_NO, DATE_BAD_CHECK, DATE_REVIEW_CREDIT, DATE_LAST_PURCHASED, HIGH_BALANCE, DATE_HIGH_BALANCE, AVG_PAY_DAYS, LAST_PAY_DAYS, OLDEST_DAYS, PAST_DUE_DAYS, CREDIT_HOLD_DAYS, FINANCE_CHG_RATE, MIN_FINANCE_CHG, STMT_CYCLE, BILL_TO_CUST_ID, COOP_RATE, COOP_AD_CTRL, IS_COOP_ROLLOVER, IS_STMT_ONLY, IS_STMT_SENT, IS_STMT_DUNNING, DUNNING_MESSAGE_ID, IS_STMT_SHOW_PO, INVOICE_COPIES, DEFAULT_SHIP2_ID, PAYFROM_ID, OPEN_ITEMS_BALANCE, DELIVERY_TYPE, STATEMENT_DELIVERY_TYPE, FAX_DAYS, IS_LATE_DISCOUNT_OK, OPEN_ORDERS_BALANCE, LAST_CHANGE_DATE)
	select
	 @cust_id, 
         date_last_pmt, 
         last_pmt_amt, 
         last_check_no, 
         date_bad_check, 
         date_review_credit, 
         date_last_purchased, 
         high_balance, 
         date_high_balance, 
         avg_pay_days, 
         last_pay_days, 
         oldest_days, 
         past_due_days, 
         credit_hold_days, 
         finance_chg_rate, 
         min_finance_chg, 
         stmt_cycle, 
         bill_to_cust_id, 
         coop_rate, 
         coop_ad_ctrl, 
         is_coop_rollover, 
         is_stmt_only, 
         is_stmt_sent, 
         is_stmt_dunning, 
         dunning_message_id, 
         is_stmt_show_po, 
         invoice_copies, 
         @SHIP2_ID as default_ship2_id, 
         payfrom_id, 
         open_items_balance, 
         delivery_type, 
         statement_delivery_type, 
         fax_days, 
         is_late_discount_ok, 
         open_orders_balance, 
         last_change_date
	 from ar_cust_standard 
	 where cust_id = @TEMPLATE_CUST_ID

end





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



--Created By:	Johnny Smith
--Company:	CenGen Technologies, Inc.
--Date:		12/12/2006
--Test Case:
/*
DECLARE @CUST_NO VARCHAR(8), @CUST_ID uniqueidentifier

   exec @error = p_AR_Cust_Insert '3M',-----------------template_cust_no
                                  'XYZCUST DISTR CO',---cust_name
                                  'XYZCUST DISTR CO',---xref_name
                                  @CUST_NO out,---------cust_no output parameter
                                  @CUST_ID out----------cust_id output parameter
                         


*/
--Change Log:
/*
	Date		Name			Description of Change

*/

alter      procedure p_AR_Cust_Insert @TEMPLATE_CUST_NO varchar(8),
                                       @CUST_NAME varchar(50),
                                       @XREF_NAME varchar(50),
                                       @CUST_NO varchar(8) out,
                                       @CUST_ID uniqueidentifier out
as

	--declare @CUST_NO varchar(8)

        select @CUST_NO = dbo.fn_next_cust_no(@CUST_NAME)

	insert ar_cust
	  (CUST_ID, CUST_NO, CUST_NAME, XREF_NAME, CURRENCY, CREDIT_LIMIT, CUST_TYPE_ID, MEMO_STATUS, CLASS_ID)
	select 
	 newid() as cust_id,
	 @CUST_NO as cust_no,
	 @CUST_NAME as cust_name,
         @XREF_NAME as xref_name,
         ar_cust.currency,
         ar_cust.credit_limit,
         ar_cust.cust_type_id,
         ar_cust.memo_status,
         ar_cust.class_id
        from ar_cust (nolock)
	where cust_no = @TEMPLATE_CUST_NO and 
            not exists (select y.cust_no 
	                from ar_cust y
	                where @CUST_NO = y.cust_no)
	and @CUST_NO is not null

         select @CUST_ID = cust_id
         from ar_cust (nolock)
         where cust_no = @CUST_NO


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

