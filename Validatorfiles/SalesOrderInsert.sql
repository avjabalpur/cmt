/****** Object:  StoredProcedure [dbo].[u_sp_SalesOrderValidate]    Script Date: 05/13/2015 09:50:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================
-- Author:		<Jason Dierks>
-- Create date: <11-26-2014>
-- Description:	<Validate Vending PO for Sales Order Automation>
-- =============================================================
ALTER PROCEDURE [dbo].[u_sp_SalesOrderValidate]	@purchaseOrderId varchar(32)
as	
	DECLARE @item 		varchar(32)
	DECLARE @warehouse 	varchar(32)

	DECLARE @xferItems TABLE
	(
		ItemNumber varchar(32) NOT NULL,
		WarehouseDestination varchar(64) NULL
	)
	INSERT INTO @xferItems
	SELECT DISTINCT I.VendorItem As ItemNumber, S.Notes AS WarehouseDestination	
	FROM [Release] R	
		JOIN [Station] S ON (S.MyNo = R.StationID AND S.cust_no = R.cust_no) 
		JOIN [Item] I ON (I.ItemNo = R.ItemID AND I.cust_no = R.cust_no)
	WHERE 
		R.StationID + '-' + R.PoNo + '-' + R.cust_no = @purchaseOrderId
		
	-----------------------------------------------------------------------------------
	-- Check all items for existence in the item master table
	-----------------------------------------------------------------------------------	
	SELECT TOP 1 @item = ItemNumber 
	FROM @xferItems xi 
		LEFT JOIN [CMT].dbo.IN_ITEM_MASTER i ON xi.ItemNumber = i.ITEM_NO
	WHERE ITEM_NO IS NULL

	IF @item IS NOT NULL
		RAISERROR ('Item %s on PO %s is not defined in Enspire.', 16, 1, @item, @purchaseOrderId);

	-----------------------------------------------------------------------------------	           
	-- Check for a duplicate PO in Order Entry (this one already added)
	-----------------------------------------------------------------------------------	
	IF EXISTS (SELECT 1 FROM [CMT].dbo.OE_DOC_HDR WHERE ORDERED_BY = @purchaseOrderId)
		RAISERROR ('The PO %s already exists in Enspire Order Entry.', 16, 1, @purchaseOrderId);



/****** Object:  StoredProcedure [dbo].[u_sp_SalesOrderInsert]    Script Date: 05/13/2015 10:24:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:		<Jason Dierks>
-- Create date: <11-26-2014>
-- Description:	<Insert Procedure for Remote Order>
-- ================================================
ALTER PROCEDURE [dbo].[u_sp_SalesOrderInsert] @ShipTo varchar(32), @purchaseOrderId varchar(32), @Username varchar(32), @docID uniqueidentifier OUT
as
	DECLARE @soDocID uniqueidentifier 		-- output parameter 	
	
	-- cursor variables 
	DECLARE @custNo nvarchar(8)
	DECLARE @ShipNo nvarchar(8)
	DECLARE @Whse nvarchar(8)
	DECLARE @requisitionDescription varchar(32)
	DECLARE @createdBy varchar(255)	
	DECLARE @SortOrder int
	DECLARE @itemNumber varchar(32)					-- detail line
	DECLARE @quantity decimal(17,8)					-- detail line
	DECLARE @department varchar(32)					-- detail line
	
	DECLARE POItems_cursor CURSOR FOR 
	SELECT 
		R.cust_no,
		@ShipTo AS Ship2,
		'MAIN' AS Whse,
		R.StationID + '-' + R.PoNo + '-' + R.cust_no AS RequisitionDescription, 
		REPLACE(@Username,'CMT\','') AS CreatedBy, 
		ROW_NUMBER() OVER (ORDER BY R.ItemID) AS SortOrder,
		I.VendorItem AS ItemNumber, 
		CAST(SUM(R.PackQty * R.MyOrder) as decimal(17,8)) As Quantity,
		NULL AS Dept
	FROM Release R 
		JOIN Station S ON (S.MyNo = R.StationID AND S.cust_no = R.cust_no)
		JOIN Item I ON (I.ItemNo = R.ItemID AND R.cust_no = I.cust_no)
	WHERE 
		R.StationID + '-' + R.PoNo + '-' + R.cust_no = @purchaseOrderId
	GROUP BY R.StationID, R.PoNo, r.cust_no, s.Notes, R.ItemID, I.VendorItem
	
	OPEN POItems_cursor

	FETCH NEXT FROM POItems_cursor INTO 
		@custNo, @ShipNo, @Whse, @requisitionDescription, @createdBy, @SortOrder, @itemNumber, @quantity, @department

	WHILE @@FETCH_STATUS = 0
	BEGIN
		----------------------------------------------	
		-- insert header (on first row)
		----------------------------------------------		
		IF @soDocID IS NULL
		BEGIN				
		
			EXEC [CMT].dbo.u_sp_SalesOrderHeaderInsert	@soDocID OUT,
																			@custNo,
																			@ShipNo,
																			@Whse,
																			@requisitionDescription,
																			@createdBy	
																					
		END
			
		----------------------------------------------			
		-- insert detail record
		----------------------------------------------			
		DECLARE @soDetailDocID uniqueidentifier		-- primary key of detail line, output parm
		
		-- get uniqueidentifier ids (used in stored proc call below)		
		DECLARE @itemId 				uniqueidentifier
		SELECT @itemId = ITEM_ID FROM [CMT].dbo.IN_ITEM_MASTER WHERE ITEM_NO = @itemNumber 

		EXEC [CMT].dbo.u_sp_SalesOrderDetailInsert	@soDetailDocID OUT,
																		@soDocID,
																		@custNo,
																		@ShipNo,
																		@Whse,
																		@createdBy,
																		@SortOrder,
																		@itemNumber,
																		@quantity,
																		@department
	
	
		FETCH NEXT FROM POItems_cursor INTO 
			@custNo, @ShipNo, @Whse, @requisitionDescription, @createdBy, @SortOrder, @itemNumber, @quantity, @department

	END 
	CLOSE POItems_cursor
	DEALLOCATE POItems_cursor
	
	-- Return newly added Enspire transfer id
	SET @docID = @soDocID



/****** Object:  StoredProcedure [dbo].[u_sp_SalesOrderHeaderInsert]    Script Date: 5/13/2015 10:27:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jason Dierks>
-- Create date: <11-26-2014>
-- Description:	<Remote Order Header Insert>
-- =============================================
ALTER PROCEDURE [dbo].[u_sp_SalesOrderHeaderInsert]
@soDocID uniqueidentifier OUT,
	@custNo nvarchar(8),
	@ShipNo nvarchar(8),
	@Whse nvarchar(8),
	@requisitionDescription varchar(max),
	@createdBy varchar(255)
as
	-- output parm; primary key
	DECLARE @custID UNIQUEIDENTIFIER
	SELECT @soDocID = NEWID()	
	
	SELECT @custID = CUST_ID FROM AR_CUST WHERE CUST_NO = @CustNo

	DECLARE @Ship2ID UNIQUEIDENTIFIER
	SELECT @Ship2ID = SHIP2_ID FROM AR_SHIP2 WHERE CUST_ID = @CustID AND SHIP_NO = @ShipNo
	
	DECLARE @TermsID UNIQUEIDENTIFIER
	SELECT @TermsID = AR_TERMS_ID FROM AR_SHIP2 WHERE SHIP2_ID = @Ship2ID

	DECLARE @WhseID UNIQUEIDENTIFIER
	SELECT @WhseID = WHSE_ID FROM IN_WAREHOUSE WHERE UPPER(WHSE_CODE) = @Whse

	DECLARE @today varchar(10)
	SELECT @today = CONVERT(VARCHAR(10), GETDATE(), 101)
	
	DECLARE @latestShip varchar(10)
	SELECT @latestShip = CONVERT(VARCHAR(10), GETDATE() +  365, 101)
		
	DECLARE @carrierId uniqueidentifier
	SELECT @carrierID = c.CARRIER_ID FROM dbo.OE_CARRIER C JOIN dbo.AR_SHIP2 S ON S.SHIP_VIA_CARRIER_ID = C.CARRIER_ID WHERE S.SHIP2_ID = @Ship2ID
	
	DECLARE @createdById uniqueidentifier
	SELECT @createdById = USER_ID FROM dbo.CO_USER WHERE USER_ID = (SELECT USER_ID FROM enspire.dbo.SY_USER WHERE NT_ACCOUNT = @createdBy)
	
	DECLARE @loginId uniqueidentifier
	SELECT TOP 1 @loginId = LOGON_ID FROM enspire.dbo.ENT_LOGONS WHERE USER_ID = @createdById ORDER BY LOGGED_ON_TIME DESC

	DECLARE @insideRepID uniqueidentifier
	SELECT @insideRepID = SALESREP_ID FROM AR_SALESREP WHERE SALESREP_CODE = (SELECT TOP 1 SalesRep FROM SalesReps WHERE AD_LOGIN = 'CMT\' + @createdBy)
	
	DECLARE @profitCtr uniqueidentifier
	SELECT @profitCtr = PROFIT_CTR_ID FROM IN_WAREHOUSE WHERE WHSE_ID = @WhseID
	
	DECLARE @blanketPO varchar(20)
	DECLARE @summaryBill varchar(1)
	DECLARE @frtCtrl varchar(1)
	DECLARE @fobDest varchar(1)
	DECLARE @territoryCode varchar(4)
	DECLARE @isTax varchar(1)
	SELECT @blanketPO = BLANKET_PO, @summaryBill = IS_SUMMARY_BILLED, @frtCtrl = FRT_CTRL, @fobDest = FOB_DEST, @territoryCode = TERRITORY_CODE, 
		@isTax = IS_TAXABLE FROM AR_SHIP2 WHERE SHIP2_ID = @Ship2ID
	 
	DECLARE @outsideRepID uniqueidentifier
	SELECT @outsideRepID = OUTSIDE_SALESREP_ID FROM AR_SHIP2 WHERE SHIP2_ID = @Ship2ID
	 
	-- execute stored proc (from enspire) to get next doc number
	DECLARE @nextDoc int	
	EXEC dbo.p_co_next_doc_no 'ORDER', @nextDoc output	


	INSERT INTO OE_DOC_HDR (
							[OE_DOC_ID],
							[DOC_CODE],
							[DOC_NO],
							[SHIP2_ID],
							[SHIPPING_WHSE_ID],
							[SUB_ORDER_TYPE],
							[CUST_ID],
							[COMMIT_TODAY],
							[ORD_DATE],
							[SHIP_BY_DATE],
							[STATUS_ID],
							[BUILD_EDI],
							[REVISION_NO],
							[IS_BACKORDER],
							[USE_SUMMARY_BILL],
							[TERMS_ID],
							[FLAG_FRTIN],
							[FLAG_FRTOUT],
							[ENTRY_USER_ID],
							[MIN_ORDER_UPCHARGE],
							[RESTOCK_PERCENTAGE],
							[PROFIT_CTR_ID],
							[CARRIER_ID],
							[BOX_COUNT],
							[FOB_DEST],
							[CURRENCY],
							[EXCHANGE_RATE],
							[POS_AMT],
							[POS_DISCOUNT_AMT],
							[POS_ACTION],
							[CUST_PO],
							[FRT_CTRL_ID],
							[FRT_ALLOWANCE],
							[CUST_DEPT_ID],
							[TERRITORY_CODE],
							[IS_TAXABLE],
							[IS_LOT_PRICE_PRINTED],
							[FRT_IN_AMT],
							[FRT_OUT_AMT],
							[COD_PLUS_AMT],
							[SHIP_COD_FEE],
							[SHIP_INSUR_FEE],
							[SHIP_HANDLING_FEE],
							[INSIDE_SALESREP_ID],
							[OUTSIDE_SALESREP_ID],
							[ORDERED_BY],
							[CHG_FRT_ON_BACKORDERS],
							[IS_SUB_OK],
							[CUST_INSTANT_DISC],
							[IS_SHIP_COMPLETE],
							[FREEZE_UPCHARGE],
							[FREEZE_SURCHARGE],
							[FREEZE_FREIGHT],
							[FREEZE_HANDLING],
							[FREEZE_ALLOWANCE],
							[CREDIT_LIMIT_BALANCE],
							[LAST_CHANGE_DATE]
							)
	 VALUES (
							@soDocID, 
							'B',						--DOC_CODE 
							@nextDoc,
							@Ship2ID, 
							@WhseID, 
							'S',						--SUB_ORDER_TYPE
							@CustID,
							'Y',						--COMMIT_TODAY 
							@today, 
							@latestShip,
							'E',						--STATUS_ID 
							'N',						--BUILD_EDI
							0,							--REVISION_NO
							'N',						--IS_BACKORDER
							@summaryBill,
							@TermsID,
							'Y',						--FLG_FRTIN
							@frtCtrl,
							@createdById,
							0,							--MIN_ORDER_UPCHARGE
							0,							--RESTOCK_PERCENTAGE
							@profitCtr,
							@CarrierID, 
							0,							--BOX_COUNT
							@fobDest,
							'US',						--CURRENCY
							1.00000,					--EXCHANGE_RATE
							0,							--POS_AMT
							0,							--POS_DISCOUNT_AMT
							'N',						--POS_ACTION
							@blanketPO,
							'N',						--FRT_CTRL_ID
							0,							--FRT_ALLOWANCE
							null,						--CUSTOMER PO; NOT POPULATED BECAUSE THIS INFORMATION IS AT THE LINE ITEM
							@territoryCode,
							@isTax,
							'N',						--IS_LOT_PRICE_PRINTED
							0,							--FRT_IN_AMT
							0,							--FRT_OUT_AMT
							0,							--COD_PLUS_AMT
							0,							--SHIP_CODE_FEE
							0,							--SHIP_INSUR_FEE
							0,							--SHIP_HANDLING_FEE
							@insideRepID,
							@outsideRepID,
							@requisitionDescription,
							'Y',						--CHG_FRT_ON_BACKORDERS
							'Y',						--IS_SUB_OK
							0,							--CUST_INSTANT_DISC
							'N',						--IS_SHIP_COMPLETE
							'N',						--FREEZE_UPCHARGE
							'N',						--FREEZE_SURCHARGE
							'N',						--FREEZE_FREIGHT
							'N',						--FREEZE_HANDLING
							'N',						--FREEZE_ALLOWANCE
							0,							--CREDIT_LIMIT_BALANCE
							@today						
							)



/****** Object:  StoredProcedure [dbo].[u_sp_SalesOrderDetailInsert]    Script Date: 5/13/2015 10:28:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jason Dierks>
-- Create date: <11-26-2014>
-- Description:	<Remote Order Detail Insert>
-- =============================================
ALTER PROCEDURE [dbo].[u_sp_SalesOrderDetailInsert]
		@soDetailDocID uniqueidentifier OUT,
		@soDocID uniqueidentifier,
		@custNo nvarchar(8),
		@ShipNo nvarchar(8),
		@Whse nvarchar(8),
		@createdBy nvarchar(32),
		@SortOrder int,
		@itemNumber varchar(32),
		@orderQuantity decimal(17,8),
		@department nvarchar(32)
as
	-- output parm; primary key
	SELECT @soDetailDocID = NEWID()	

	DECLARE @itemId 		uniqueidentifier
	DECLARE @itemQtyUM 		varchar(2)
	DECLARE @itemQtyConv 	decimal(17,8)	
	DECLARE @itemListPrice 	decimal(17,8)
	DECLARE @groupID		uniqueidentifier
	SELECT 
		@itemId = ITEM_ID, 
		@itemQtyUM = QTY_UM, 
		@itemQtyConv = QTY_CONV,
		@itemListPrice = LIST_PRICE,
		@groupID = GROUP_ID
	FROM dbo.IN_ITEM_MASTER WHERE ITEM_NO = @itemNumber   --  (table also has CALCULATED_LIST_PRICE, CALCULATED_NEXT_COST, etc

	DECLARE @custID UNIQUEIDENTIFIER
	SELECT @custID = CUST_ID FROM AR_CUST WHERE CUST_NO = @CustNo

	DECLARE @Ship2ID UNIQUEIDENTIFIER
	SELECT @Ship2ID = SHIP2_ID FROM AR_SHIP2 WHERE CUST_ID = @CustID AND SHIP_NO = @ShipNo

	DECLARE @WhseID UNIQUEIDENTIFIER
	SELECT @WhseID = WHSE_ID FROM IN_WAREHOUSE WHERE UPPER(WHSE_CODE) = @Whse
	
	DECLARE @deptID UNIQUEIDENTIFIER
	SELECT @deptID = CUST_DEPT_ID FROM AR_SHIP2_DEPTS WHERE SHIP2_ID = @Ship2ID AND DEPT = @department
	IF @custNo IN ('686') AND @deptID IS NULL AND LEN(@department) < 13
	BEGIN
		SET @deptID = NEWID()
		INSERT INTO AR_SHIP2_DEPTS (CUST_DEPT_ID, DEPT, SHIP2_ID, DESCRIPTION) VALUES (@deptID, @department, @Ship2ID, @department)
	END

	DECLARE @is_active varchar(1)
	SELECT @is_active = IS_ACTIVE FROM IN_WHSEITEM_MASTER WHERE ITEM_ID = @itemId AND WHSE_ID = @WhseID

	DECLARE @is_stocked varchar(1)
	DECLARE @qty_available decimal(14,6)
	EXEC dbo.p_in_qty_avail
		@whse_id = @whseID,
        @item_id = @itemId,
        @is_stocked = @is_stocked OUTPUT,
        @qty_available = @qty_available OUTPUT	
        
	DECLARE @lineType varchar(1)
	SET @lineType = 'S'
	IF @is_stocked = 'N'
		SET @lineType = 'N'

	DECLARE @disposition varchar(1)
	SET @disposition = 'S'
	IF @is_stocked = 'Y' AND @is_active = 'Y' AND @qty_available >= @orderQuantity
		SET @disposition = 'C'
	IF @is_stocked = 'Y' AND @is_active = 'Y' AND @qty_available < @orderQuantity
		SET @disposition = 'B'

	DECLARE @vendorID uniqueidentifier
	SET @vendorID = (SELECT TOP 1 VENDOR_ID FROM IN_VNDRITEM_MASTER WHERE ITEM_ID = @itemId)
	
	DECLARE @xrefID uniqueidentifier
	SET @xrefID = (SELECT TOP 1 XREF_ITEM_ID FROM IN_ITEM_CUST_XREF WHERE ITEM_ID = @itemId AND CUST_ID = @custID)

	DECLARE @desc varchar(50)
	DECLARE @taxable varchar(1)
	DECLARE @costSTD decimal(17,8)
	DECLARE @costActual decimal(17,8)
	DECLARE @isCommission varchar(1)
	DECLARE @rounding integer
	SELECT @desc = BASE_DESCRIPTION, @taxable = IS_TAXABLE, @costSTD = STD_COST, @costActual = NEXT_COST, 
		@isCommission = IS_COMMISSION_PAID, @rounding = ROUNDING FROM IN_ITEM_MASTER WHERE ITEM_ID = @itemId

	DECLARE @ship2Taxable varchar(1)
	SELECT @ship2Taxable = IS_TAXABLE FROM AR_SHIP2 WHERE SHIP2_ID = @Ship2ID
	IF @ship2Taxable = 'N'
		SET @taxable = 'N'	
	
	DECLARE @customer_price decimal(17,4)
	EXEC dbo.u_sp_GetPricingForStoreFront 
		@ItemID = @ItemID, 
		@Ship2ID = @Ship2ID, 
		@Qty = @orderQuantity, 
		@Price = @customer_price OUTPUT
	
	DECLARE @priceSource varchar(2)
	SET @priceSource = 'CO'
	IF @customer_price = @itemListPrice 
		SET @priceSource = 'LP'	
		
	DECLARE @glSetId uniqueidentifier
	SELECT @glSetId = GL_SET_ID FROM IN_ITEM_MASTER WHERE ITEM_ID = @itemId
	IF @lineType = 'S'
		SELECT @glSetId = GL_SET_ID FROM IN_WHSEITEM_MASTER WHERE ITEM_ID = @itemId AND WHSE_ID = @WhseID
		IF @qty_available > 0
			SELECT @costActual = AVG_COST FROM IN_WHSEITEM_MASTER WHERE ITEM_ID = @itemId AND WHSE_ID = @WhseID
		
	DECLARE @backorderQty decimal(16,4)
	SET @backorderQty = 0
	IF @lineType = 'N'
		SET @backorderQty = @orderQuantity
	IF @orderQuantity > @qty_available AND @lineType = 'S'
		SET @backorderQty = @orderQuantity - @qty_available

	DECLARE @commitQty decimal(16,4)
	SET @commitQty = 0
	IF @orderQuantity <= @qty_available AND @lineType = 'S'
		SET @commitQty = @orderQuantity
	IF @orderQuantity > @qty_available AND @lineType = 'S'
		SET @commitQty = @qty_available
		
	DECLARE @today varchar(10)
	SELECT @today = CONVERT(VARCHAR(10), GETDATE(), 101)
	
	DECLARE @latestShip varchar(10)
	SELECT @latestShip = CONVERT(VARCHAR(10), GETDATE() +  365, 101)
		
	INSERT INTO OE_DOC_DTL (
							OE_DOC_DTL_ID,
							OE_DOC_ID,
							SHIP2_ID,
							SORT_ORDER,
							LINE_TYPE,
							DISPOSITION,
							VENDOR_ID,
							ORIG_ITEM_ID,
							ITEM_ID,
							CUST_ITEM_ID,
							ITEM_NO,
							DESCRIPTION,
							EARLY_SHIP_DATE,
							SHIP_BY_DATE,
							PRICE_SOURCE,
							IS_TAXABLE,
							FLAG_BOTTOM_LINE_DISC,
							MISS_LATE,
							MISS_SUB,
							MISS_BO,
							MISS,
							HIT,
							QTY_UM, 
							QTY_CONV, 
							AMT_CONV, 
							AMT_UM,
							GL_SET_ID,
							QTY_ORDERED,
							QTY_SHIPPED,
							QTY_INVOICED,
							QTY_BACK_ORDERED,
							COMMIT_QTY,
							QTY_CANCELED,
							QTY_UNIT_CONTROLLED,
							QTY_REBATE_CUST,
							QTY_REBATE_VENDOR,
							QTY_CREDITED,
							CUST_QTY_REBATE_CREDITED,
							VENDOR_QTY_REBATE_CREDITED,
							LIST_PRICE,
							CUST_PRICE,
							BLIND_PRICE,
							U_COST_ACTUAL,
							U_COST_NEXT,
							U_COST_STD,
							MANUAL_CUST_UNIT_REBATE_AMT,
							CUST_UNIT_REBATE_AMT,
							MANUAL_VEND_REBATE,
							VENDOR_UNIT_REBATE_AMT,
							LINE_DISC_RATE,
							EXTENDED_AMT,
							SPIFF_AMT,
							LICENSE_USED,
							LINE_LEVEL_FAM_DISC_RATE,
							BOT_LINE_FAM_DISC_RATE,
							IS_BACKORDERED,
							SUB_METHOD,
							PRICE_STRING,
							CUST_DEPT_ID, 
							BACKORDER_CTRL,
							IS_FAMILY_DISCOUNT_OK, 
							IS_COMMISSION_PAID,
							RESTOCK_PERCENTAGE,
							IS_CUST_REBATE_OK, 
							IS_VEND_REBATE_OK,
							BTO_LABOR_COST,
							BTO_PACKAGING_COST,
							BTO_OVERHEAD_COST,
							COMMIT_TODAY,
							COMMIT_LATER_QTY,
							QTY_SCHEDULED,
							TIME_ADDED,
							ROUNDING,
							QTY_PREV_SHIPPED,
							GROUP_ID,
							SYSTEM_PRICE
							)
	VALUES (
							@soDetailDocID,
							@soDocID,
							@Ship2ID,
							@SortOrder,
							@lineType,
							@disposition,
							@vendorID,
							@itemId,
							@itemId,
							@xrefID,
							@itemNumber,
							@desc,
							@today,
							@latestShip,
							@priceSource, 
							@taxable,
							'N',						--FLAG_BOTTOM_LINE_DISC
							'N',						--MISS_LATE
							'N',						--MISS_SUB
							'N',						--MISS_BO
							'N',						--MISS
							'N',						--HIT
							@itemQtyUM, 
							@itemQtyConv, 
							@itemQtyConv, 
							@itemQtyUM, 
							@glSetId,
							@orderQuantity, 
							0,							--QTY_SHIPPED
							0,							--QTY_INVOICED
							@backorderQty, 
							@commitQty,
							0,							--QTY_CANCELED
							0,							--QTY_UNIT_CONTROLLED
							0,							--QTY_REBATE_CUST
							0,							--QTY_REBATE_VENDOR
							0,							--QTY_CREDITED
							0,							--CUST_QTY_REBATE_CREDITED
							0,							--VENDOR_QTY_REBATE_CREDITED
							@itemListPrice, 
							round(@customer_price,@rounding), 
							@itemListPrice,
							@costActual,
							@costActual,
							@costSTD,
							0,							--MANUAL_CUST_UNIT_REBATE_AMT
							0,							--CUST_UNIT_REBATE_AMT
							0,							--MANUAL_VEND_REBATE
							0,							--VENDOR_UNIT_REBATE_AMT
							round((1-(@customer_price/@itemListPrice))*100,2),
							@orderQuantity * round(@customer_price,@rounding),
							0,							--SPIFF_AMT
							'N',						--LICENSE_USED
							0,							--LINE_LEVEL_FAM_DISC_RATE
							0,							--BOT_LINE_FAM_DISC_RATE
							'N',						--IS_BACKORDERED
							'N',						--SUB_METHOD
							round(@customer_price,@rounding), 
							@deptID,
							'Y',						--BACKORDER_CTRL
							'N',						--IS_FAMILY_DISCOUNT_OK
							@isCommission,
							0,							--RESTOCK_PERCENTAGE
							'N',						--IS_CUST_REBATE_OK
							'Y',						--IS_VEND_REBATE_OK
							0,							--BTO_LABOR_COST
							0,							--BTO_PACKAGING_COST
							0,							--BTO_OVERHEAD_COST
							'Y',						--COMMIT_TODAY
							0,							--COMMIT_LATER_QTY
							0,							--QTY_SCHEDULED
							getdate(),					--TIME_ADDED
							@rounding,
							0,							--QTY_PREV_SHIPPED
							@groupID,
							@customer_price
							)

UPDATE IN_WHSEITEM_MASTER 
SET COMMIT_TODAY_QTY = COMMIT_TODAY_QTY + @commitQty, BACKLOG_QTY_STOCK = BACKLOG_QTY_STOCK + @backorderQty
WHERE WHSE_ID = @WhseID AND ITEM_ID = @itemId

	DECLARE @bookingId uniqueidentifier
	SELECT @bookingId = NEWID()	
	
	DECLARE @createdById uniqueidentifier
	SELECT @createdById = USER_ID FROM dbo.CO_USER WHERE USER_ID = (SELECT USER_ID FROM enspire.dbo.SY_USER WHERE NT_ACCOUNT = @createdBy)

	DECLARE @docNo varchar(8)
	DECLARE @custPO varchar(32)
	DECLARE @insideRep uniqueidentifier
	DECLARE @outsideRep uniqueidentifier
	SELECT @docNo = DOC_NO, @custPO = CUST_PO, @insideRep = INSIDE_SALESREP_ID, @outsideRep = OUTSIDE_SALESREP_ID FROM OE_DOC_HDR WHERE OE_DOC_ID = @soDocID
	
	INSERT INTO OE_BOOKINGS (
							BOOKING_ID,
							OE_DOC_DTL_ID,
							OE_DOC_ID,
							DOC_NO,
							DOC_CODE,
							WHSE_ID,
							CUST_PO,
							INSIDE_SALESREP_ID,
							OUTSIDE_SALESREP_ID,
							BOOK_DATE,
							ACCTG_YEAR,
							ACCTG_PERIOD,
							USER_ID,
							ITEM_ID,
							CUST_PRICE,
							UNIT_COST,
							BOOK_AMT,
							AMT_UM,
							AMT_CONV,
							PRICE_SOURCE,
							CURRENT_QTY,
							BOOK_QTY,
							QTY_UM, 
							QTY_CONV, 
							STATUS,
							LINE_TYPE,
							VENDOR_ID,
							CUST_ID,
							SHIP2_ID
							)
	VALUES (
							@bookingId,
							@soDetailDocID,
							@soDocID,
							@docNo,
							'B',						--DOC_CODE
							@WhseID,
							@custPO,
							@insideRep,
							@outsideRep,
							GETDATE(),					--BOOK_DATE
							YEAR(GETDATE()),			--ACCTG_YEAR
							MONTH(GETDATE()),			--ACCTG_PERIOD
							@createdById,
							@itemId,
							round(@customer_price,@rounding), 
							@costActual,
							@orderQuantity * round(@customer_price,@rounding),
							@itemQtyUM, 
							@itemQtyConv, 
							@priceSource, 
							@orderQuantity,				--CURRENT_QTY
							@orderQuantity,				--BOOK_QTY
							@itemQtyUM, 
							@itemQtyConv, 
							'A',						--STATUS
							@lineType,
							@vendorID,
							@custID,
							@Ship2ID
							)


