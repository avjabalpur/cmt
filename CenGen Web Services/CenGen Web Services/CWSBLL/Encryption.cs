﻿using System;

namespace CWSBLL
{
    public class Encryption
    {
        /// <summary>
        /// Summary description for Encryption.
        /// </summary>
        /// 

        #region Constructors
        public Encryption()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        #region EncryptString
        public static string EncryptString(string encryptString)
        {

            Byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(encryptString);

            string encryptedString = Convert.ToBase64String(b);

            return encryptedString;
        }
        #endregion
    }
}