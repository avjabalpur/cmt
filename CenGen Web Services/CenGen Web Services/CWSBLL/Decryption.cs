﻿using System;

namespace CWSBLL
{
    public class Decryption
    {
        #region Constructors
        public Decryption()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        #region DecryptString
        public string DecryptString(string Key)
        {

            Byte[] b = Convert.FromBase64String(Key);

            string decryptedConnectionString = System.Text.ASCIIEncoding.ASCII.GetString(b);

            return decryptedConnectionString;

        }
        #endregion
    }
}