﻿using System;
using System.Data;
using System.IO;
using System.Text;

namespace CWSBLL
{
    /// <summary>
    /// Summary description for LogError.
    /// </summary>
    [Serializable]
    public class LogError
    {
        public const string FileName = "CenGenXMLWebServicesLogFile.txt";
        private static string m_sFilePath;
        private static string sLogFilePath;
        private static string sErrorClass;
        private static string sErrorMethod;
        private static string sErrorStep;
        private static bool bLogError;

        #region Constructors
        public LogError()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        #region Properties
        public static string FilePath
        {
            get { return m_sFilePath; }
            set { m_sFilePath = value; }
        }
        #endregion

        #region pLogError in log file only
        //Writes an error message to the log file	
        public static void pLogError(Exception e, string LogFilePath, string ErrorClass, string ErrorMethod, string ErrorStep, bool LogError)
        {
            sLogFilePath = LogFilePath;
            sErrorClass = ErrorClass;
            sErrorMethod = ErrorMethod;
            sErrorStep = ErrorStep;
            bLogError = LogError;

            if (sLogFilePath == null)
                sLogFilePath = FileName;

            WriteErrorLog(e);
        }
        #endregion

        #region pLogError in log file and problem data set

        //Writes an error message to the log file and to the data set that was being processed at the time of the error
        public static void pLogError(Exception e, string LogFilePath, string ErrorClass, string ErrorMethod, string ErrorStep, bool LogError, DataSet dsErrorDataSet)
        {
            sLogFilePath = LogFilePath;
            sErrorClass = ErrorClass;
            sErrorMethod = ErrorMethod;
            sErrorStep = ErrorStep;
            bLogError = LogError;

            if (sLogFilePath == null)
                sLogFilePath = System.AppDomain.CurrentDomain.RelativeSearchPath + @"\" + FileName;

            WriteErrorLog(e);
            WriteErrorRecord(e, dsErrorDataSet);
        }
        #endregion

        #region WriteErrorLog
        private static void WriteErrorLog(Exception e)
        {
            StreamWriter file = null;
            try
            {
                if ((bLogError = true) && (sLogFilePath != ""))
                {
                    StringBuilder sbError = new StringBuilder();
                    sbError.Append("***errror logged at " + DateTime.Now + "\r\n");
                    sbError.Append("Error In: " + sErrorMethod);
                    sbError.Append("Source: " + e.Source + "\r\n");
                    sbError.Append("Method: " + e.TargetSite.Name + "\r\n");
                    sbError.Append("Description: " + e.Message + "\r\n");
                    sbError.Append("------------------------------------------");
                    file = new StreamWriter(sLogFilePath, true);
                    file.WriteLine(sbError.ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
                //This error needs to be thrown else it will put the program in a never ending loop
            }
            finally
            {
                if (file != null)
                {
                    file.Close();
                }
            }

        }
        #endregion

        #region WriteErrorRecord
        private static void WriteErrorRecord(Exception e, DataSet dsErrorDataSet)
        {
            try
            {
                if ((bLogError = true) && (dsErrorDataSet != null))
                {
                    DataTable dtErrorLog = new DataTable("ErrorLog");

                    dtErrorLog.Columns.Add("Class", System.Type.GetType("System.String"));
                    dtErrorLog.Columns.Add("Method", System.Type.GetType("System.String"));
                    dtErrorLog.Columns.Add("Step", System.Type.GetType("System.String"));
                    dtErrorLog.Columns.Add("Source", System.Type.GetType("System.String"));
                    dtErrorLog.Columns.Add("ErrorDate", System.Type.GetType("System.DateTime"));
                    dtErrorLog.Columns.Add("ErrorMsg", System.Type.GetType("System.String"));

                    DataRow drError = dtErrorLog.NewRow();

                    drError["Class"] = sErrorClass;
                    drError["Method"] = sErrorMethod;
                    drError["Step"] = sErrorStep;
                    drError["Source"] = e.Source + ' ' + e.TargetSite.Name;
                    drError["ErrorDate"] = DateTime.Now;
                    drError["ErrorMsg"] = e.Message;

                    dtErrorLog.Rows.Add(drError);

                    dsErrorDataSet.Tables.Add(dtErrorLog);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
