﻿using System;
using System.Data;

namespace CWSBLL
{
    public class PriceCalculator
    {
        private string mPriceSource;
        private string mContractID;
        private string mContract;
        private string mIsBestPriceOK;
        private string mOkToUseDefaultBracket;
        private string mOkToUseFamilyDiscounts;
        private string mOkToUseCustomerRebates;
        private string mMethod;
        private string mBasis;
        private string mCostBasis;
        private int mDefaultBracket = 1;
        private int mRounding = 2;
        private Decimal mQtyOrdered = 0;
        private Decimal mRate = 0;
        private Decimal mListPrice = 0;
        private Decimal mStdCost = 0;
        private Decimal mNextCost = 0;
        private Decimal mActualCost = 0;
        private Decimal mManualPrice = 0;
        private Decimal mItemReferencePrice = 0;
        private Decimal mAddOnAmount = 0;
        private Decimal mCustPrice = 0;
        private Decimal mContractPrice = 0;
        private Decimal mQuantityPrice = 0;


        private string sErrorClass = "PriceCalculator";
        private string sErrorMethod = string.Empty;
        private string sErrorStep = string.Empty;


        #region Constructors
        public PriceCalculator()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        #region Properties
        public string PriceSource
        {
            get { return mPriceSource; }
            set { mPriceSource = value; }
        }
        public string ContractID
        {
            get { return mContractID; }
            set { mContractID = value; }
        }
        public string Contract
        {
            get { return mContract; }
            set { mContract = value; }
        }
        public string IsBestPriceOK
        {
            get { return mIsBestPriceOK; }
            set { mIsBestPriceOK = value; }
        }
        public string OkToUseDefaultBracket
        {
            get { return mOkToUseDefaultBracket; }
            set { mOkToUseDefaultBracket = value; }
        }
        public string OkToUseFamilyDiscounts
        {
            get { return mOkToUseFamilyDiscounts; }
            set { mOkToUseFamilyDiscounts = value; }
        }
        public string OkToUseCustomerRebates
        {
            get { return mOkToUseCustomerRebates; }
            set { mOkToUseCustomerRebates = value; }
        }
        public string Method
        {
            get { return mMethod; }
            set { mMethod = value; }
        }
        public string Basis
        {
            get { return mBasis; }
            set { mBasis = value; }
        }
        public string CostBasis
        {
            get { return mCostBasis; }
            set { mCostBasis = value; }
        }
        public Int32 DefaultBracket
        {
            get { return mDefaultBracket; }
            set { mDefaultBracket = value; }
        }
        public Int32 Rounding
        {
            get { return mRounding; }
            set { mRounding = value; }
        }
        public Decimal QtyOrdered
        {
            get { return mQtyOrdered; }
            set { mQtyOrdered = value; }
        }
        public Decimal Rate
        {
            get { return mRate; }
            set { mRate = value; }
        }
        public Decimal ListPrice
        {
            get { return mListPrice; }
            set { mListPrice = value; }
        }
        public Decimal StdCost
        {
            get { return mStdCost; }
            set { mStdCost = value; }
        }
        public Decimal NextCost
        {
            get { return mNextCost; }
            set { mNextCost = value; }
        }
        public Decimal ActualCost
        {
            get { return mActualCost; }
            set { mActualCost = value; }
        }
        public Decimal ManualPrice
        {
            get { return mManualPrice; }
            set { mManualPrice = value; }
        }
        public Decimal ItemReferencePrice
        {
            get { return mItemReferencePrice; }
            set { mItemReferencePrice = value; }
        }
        public Decimal AddOnAmount
        {
            get { return mAddOnAmount; }
            set { mAddOnAmount = value; }
        }
        public Decimal CustPrice
        {
            get { return mCustPrice; }
            set { mCustPrice = value; }
        }
        public Decimal ContractPrice
        {
            get { return mContractPrice; }
            set { mContractPrice = value; }
        }
        public Decimal QuantityPrice
        {
            get { return mQuantityPrice; }
            set { mQuantityPrice = value; }
        }
        #endregion

        #region CalculatePrice
        public Decimal CalculatePrice()
        {
            Decimal CalcedPrice = 0;

            switch (Basis)
            {
                case "C": //Cost Up
                    switch (CostBasis)
                    {
                        case "A":
                            CalcedPrice = GetCostUp(this.ActualCost, this.NextCost, this.StdCost) + this.AddOnAmount;
                            break;
                        case "N":
                            CalcedPrice = GetCostUp(this.NextCost, this.StdCost, 0) + this.AddOnAmount;
                            break;
                        case "S":
                            CalcedPrice = GetCostUp(this.StdCost, this.NextCost, 0) + this.AddOnAmount;
                            break;
                        default:
                            break;
                    }
                    break;
                case "D": //Manual Dollar Amount
                    CalcedPrice = (this.ManualPrice + this.AddOnAmount);
                    break;
                case "L": //List Down
                    CalcedPrice = GetListPrice() + this.AddOnAmount;
                    break;
                case "M": //Margin Cost
                    switch (this.CostBasis)
                    {
                        case "A":
                            CalcedPrice = GetMarginCost(this.ActualCost, this.NextCost, this.StdCost) + this.AddOnAmount;
                            break;
                        case "N":
                            CalcedPrice = GetMarginCost(this.NextCost, this.StdCost, 0) + this.AddOnAmount;
                            break;
                        case "S":
                            CalcedPrice = GetMarginCost(this.StdCost, this.NextCost, 0) + this.AddOnAmount;
                            break;
                        default:
                            break;
                    }
                    break;
                case "R":  //Reference Price Down
                    CalcedPrice = GetReferencePriceDown() + this.AddOnAmount;
                    break;
                case "U": //Upcharge
                    CalcedPrice = GetUpcharge() + this.AddOnAmount;
                    break;
                case "X": //List Multiplier
                    CalcedPrice = GetListMultiplier() + this.AddOnAmount;
                    break;
                default:
                    break;
            }
            return Decimal.Round(CalcedPrice, this.Rounding);
        }
        #endregion

        #region GetCostUp
        private Decimal GetCostUp(Decimal ChosenCost, Decimal FirstFallBackCost, Decimal FinalFallBackCost)
        {
            //Business rule dictates that when the ChosenCost equal zero, the check to see if the FirstFallBackCost is greater than zero
            //if not, then check the FinalFallBackCost and if it's zero the price returned will be zero.
            if (ChosenCost > 0)
                return ChosenCost + (ChosenCost * this.Rate);
            else if (FirstFallBackCost > 0)
                return FirstFallBackCost + (FirstFallBackCost * this.Rate);
            else
                return FinalFallBackCost + (FinalFallBackCost * this.Rate);
        }
        #endregion

        #region GetListPrice
        private Decimal GetListPrice()
        {
            return (this.ListPrice - (this.ListPrice * this.Rate));
        }
        #endregion

        #region GetMarginCost
        private Decimal GetMarginCost(Decimal ChosenCost, Decimal FirstFallBackCost, Decimal FinalFallBackCost)
        {
            //Business rule dictates that when the ChosenCost equal zero, the check to see if the FirstFallBackCost is greater than zero
            //if not, then check the FinalFallBackCost and if it's zero the price returned will be zero.
            if (ChosenCost > 0)
                return ChosenCost / (1 - this.Rate);
            else if (FirstFallBackCost > 0)
                return FirstFallBackCost / (1 - this.Rate);
            else if (FinalFallBackCost > 0)
                return FinalFallBackCost / (1 - this.Rate);
            else
                return 0;
        }
        #endregion

        #region GetReferencePriceDown
        private Decimal GetReferencePriceDown()
        {
            return (this.ItemReferencePrice - (this.ItemReferencePrice * this.Rate));
        }
        #endregion

        #region GetUpcharge
        private Decimal GetUpcharge()
        {
            return (this.ListPrice + (this.ListPrice * this.Rate));
        }
        #endregion

        #region GetListMultiplier
        private Decimal GetListMultiplier()
        {
            return (this.ListPrice * this.Rate);
        }
        #endregion

        #region CalculateContractPrice
        public void CalculateContractPrice(DataTable dtOEContractRecords, DataTable dtOEContractRecordsQtyBrackets, DataTable dtINQtyContractRecords)
        {
            DateTime Today = DateTime.Now;
            bool ContractPricingExists = false;

            //IsBestPriceOk: L - customer gets list price only
            //               N - customer gets the first price in the price hierarchy
            //               X - customer gets the best price including all contracts not just in priority ranking
            //               Y - customer gets the best price but use the contract priority to determine the contract price

            //Loop through OEContractRecords
            for (int OEContractRecord = 0; OEContractRecord < dtOEContractRecords.Rows.Count; OEContractRecord++)
            {
                if (ContractPricingExists && this.IsBestPriceOK != "X")
                    break;
                //make sure the contract is still in effect based on the shipto, contract header, and contract detail begin and end dates.
                if ((dtOEContractRecords.Rows[OEContractRecord]["shipto_begin_date"].ToString() == null || DateTime.Parse(dtOEContractRecords.Rows[OEContractRecord]["shipto_begin_date"].ToString()) <= Today) &&
                    (dtOEContractRecords.Rows[OEContractRecord]["shipto_end_date"].ToString() == null || DateTime.Parse(dtOEContractRecords.Rows[OEContractRecord]["shipto_end_date"].ToString()) >= Today) &&
                    (dtOEContractRecords.Rows[OEContractRecord]["contract_hdr_begin_date"].ToString() == null || DateTime.Parse(dtOEContractRecords.Rows[OEContractRecord]["contract_hdr_begin_date"].ToString()) <= Today) &&
                    (dtOEContractRecords.Rows[OEContractRecord]["contract_dtl_end_date"].ToString() == null || DateTime.Parse(dtOEContractRecords.Rows[OEContractRecord]["contract_dtl_end_date"].ToString()) >= Today) &&
                    (dtOEContractRecords.Rows[OEContractRecord]["contract_dtl_end_date"].ToString() == null || DateTime.Parse(dtOEContractRecords.Rows[OEContractRecord]["contract_dtl_end_date"].ToString()) >= Today))
                {
                    //check to see if any quantity contracts exists at all
                    if (dtINQtyContractRecords != null && dtINQtyContractRecords.Rows.Count > 0)
                    {
                        DataRow[] drQtyContractRecord = dtINQtyContractRecords.Select("contract_id = " + dtOEContractRecords.Rows[OEContractRecord]["contract_id"].ToString(), "bracket asc");
                        //Check to see if any quantity contracts exist for the contract
                        if (drQtyContractRecord.Length > 0)
                        {
                            for (int i = 0; i < drQtyContractRecord.Length; i++)
                            {
                                if (IsQuantityContractStillValid(drQtyContractRecord[i], Today))
                                {
                                    ContractPricingExists = true;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }

                    //Set the contract price record's parameters
                    this.ContractID = dtOEContractRecords.Rows[OEContractRecord]["contract_id"].ToString();
                    this.Contract = dtOEContractRecords.Rows[OEContractRecord]["contract"].ToString();
                    this.Method = dtOEContractRecords.Rows[OEContractRecord]["method"].ToString();
                    this.Basis = dtOEContractRecords.Rows[OEContractRecord]["calc_base_id"].ToString();
                    this.CostBasis = dtOEContractRecords.Rows[OEContractRecord]["cost_basis_id"].ToString();
                    this.Rate = Decimal.Parse(dtOEContractRecords.Rows[OEContractRecord]["calc_rate"].ToString());
                    this.OkToUseDefaultBracket = dtOEContractRecords.Rows[OEContractRecord]["cust_bracket_used"].ToString();
                    this.OkToUseFamilyDiscounts = dtOEContractRecords.Rows[OEContractRecord]["family_disc_ok"].ToString();
                    this.OkToUseCustomerRebates = dtOEContractRecords.Rows[OEContractRecord]["cust_rebate_ok"].ToString();


                    switch (this.Method)
                    {
                        case "F":
                            this.ManualPrice = Decimal.Parse(dtOEContractRecords.Rows[OEContractRecord]["contract_price"].ToString());
                            this.ContractPrice = this.ManualPrice;
                            this.CustPrice = this.ManualPrice;
                            this.PriceSource = "Contract";
                            break;
                        case "M":
                            this.ManualPrice = Decimal.Parse(dtOEContractRecords.Rows[OEContractRecord]["contract_price"].ToString());
                            this.ContractPrice = this.ManualPrice;
                            this.CustPrice = this.ManualPrice;
                            this.PriceSource = "Contract";
                            break;
                        case "Q":
                            //get the quantity records associated to this contract
                            DataRow[] drQtyBrackets = dtOEContractRecordsQtyBrackets.Select("contract_dtl_id = " + dtOEContractRecords.Rows[OEContractRecord]["contract_dtl_id"].ToString(), "bracket");

                            if (drQtyBrackets != null)
                            {
                                for (int x = 0; x < drQtyBrackets.Length; x++)
                                {
                                    if (Decimal.Parse(drQtyBrackets[x]["qty"].ToString()) < this.QtyOrdered || (Int32.Parse(drQtyBrackets[x]["bracket"].ToString()) <= this.DefaultBracket && String.Compare(this.OkToUseDefaultBracket, "Y") == 0))
                                    {
                                        this.ManualPrice = Decimal.Parse(drQtyBrackets[x]["amt"].ToString());
                                        this.Rate = Decimal.Parse(drQtyBrackets[x]["disc_rate"].ToString());
                                        this.AddOnAmount = Decimal.Parse(drQtyBrackets[x]["add_on_amt"].ToString());
                                        this.ContractPrice = this.ManualPrice + this.AddOnAmount;
                                        this.CustPrice = this.ManualPrice + this.AddOnAmount;
                                        this.PriceSource = "Contract";
                                    }
                                    else break;
                                }
                            }
                            break;
                        case "V":
                            this.ManualPrice = CalculatePrice();
                            this.ContractPrice = this.ManualPrice;
                            this.CustPrice = this.ManualPrice;
                            this.PriceSource = "Contract";
                            break;
                        default:
                            break;
                    }
                }
                //set the ContractPricingExists bool value
            }
            return;
        }
        #endregion

        #region IsQuantityContractStillValid
        private bool IsQuantityContractStillValid(DataRow drQtyContractRecords, DateTime Today)
        {
            this.sErrorMethod = "IsQuantityContractStillValid";

            try
            {
                this.sErrorStep = "Make sure the contract Header and Detail Begin and End Dates are valid";
                //Make sure the contract Header and Detail Begin and End Dates are valid
                if ((drQtyContractRecords["hdr_begin_date"] == null || DateTime.Parse(drQtyContractRecords["hdr_begin_date"].ToString()) <= Today) &&
                    (drQtyContractRecords["hdr_end_date"] == null || DateTime.Parse(drQtyContractRecords["hdr_end_date"].ToString()) >= Today) &&
                    (drQtyContractRecords["dtl_begin_date"] == null || DateTime.Parse(drQtyContractRecords["dtl_begin_date"].ToString()) <= Today) &&
                    (drQtyContractRecords["dtl_end_date"] == null || DateTime.Parse(drQtyContractRecords["dtl_end_date"].ToString()) >= Today))
                {
                    this.sErrorStep = "If the Void Price parameter equals Yes, return false, else return true";
                    //If the Void Price parameter equals "Y", return false else return true
                    if (String.Compare(drQtyContractRecords["void_price"].ToString(), "Y") != 0 &&
                        Decimal.Parse(drQtyContractRecords["contract_qty"].ToString()) < Decimal.Parse(drQtyContractRecords["actual_qty_sold"].ToString()) &&
                        Decimal.Parse(drQtyContractRecords["contract_amt"].ToString()) < Decimal.Parse(drQtyContractRecords["actual_amt_sold"].ToString()))
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                CWSBLL.LogError.pLogError(ex, LogError.FilePath, sErrorClass, sErrorMethod, sErrorStep, true);
                return false;
            }
        }
        #endregion

        #region CalculateQuantityPrice
        public void CalculateQuantityPrice(DataTable dtQtyPriceBrackets)
        {
            int bracketAwarded = 0;
            DataRow[] drQtyPriceBrackets = null;

            //get a list of warehouse item quantity brackets
            DataRow[] drWhseItemQtyPriceBrackets = dtQtyPriceBrackets.Select("is_whseitem_record = 1", "bracket");

            //get a list of item quantity brackets
            DataRow[] drItemQtyPriceBrackets = dtQtyPriceBrackets.Select("is_item_record = 1", "bracket");

            //get a list of product group brackets
            DataRow[] drProductGroupQtyPriceBrackets = dtQtyPriceBrackets.Select("is_group_record = 1", "bracket");

            //check to see if the warehouse item quantity brackets exist.  If so, this is the quantity price that will be selected
            if (drWhseItemQtyPriceBrackets != null && drWhseItemQtyPriceBrackets.Length > 0)
                drQtyPriceBrackets = drWhseItemQtyPriceBrackets;
            else if (drItemQtyPriceBrackets != null && drItemQtyPriceBrackets.Length > 0)
                drQtyPriceBrackets = drItemQtyPriceBrackets;
            else if (drProductGroupQtyPriceBrackets != null && drProductGroupQtyPriceBrackets.Length > 0)
                drQtyPriceBrackets = drProductGroupQtyPriceBrackets;

            //check to see if quantity brackets exist.  If so, get the quantity price bracket to use
            if (drQtyPriceBrackets != null && drQtyPriceBrackets.Length > 0)
            {
                if (String.Compare(drQtyPriceBrackets[0]["use_price_table"].ToString(), "Y") == 0)
                {
                    bracketAwarded = 0;
                    for (int bracket = 0; bracket < drQtyPriceBrackets.Length; bracket++)
                    {
                        if (this.DefaultBracket >= Int32.Parse(drQtyPriceBrackets[bracket]["bracket"].ToString()))
                        {
                            bracketAwarded = bracket + 1;
                        }
                    }
                }
                else
                {
                    bracketAwarded = 0;
                    for (int bracket = 0; bracket < drQtyPriceBrackets.Length; bracket++)
                    {
                        if (this.QtyOrdered >= Decimal.Parse(drQtyPriceBrackets[bracket]["qty"].ToString()) || this.DefaultBracket >= Int32.Parse(drQtyPriceBrackets[bracket]["bracket"].ToString()))
                        //if(this.QtyOrdered >= 5 || this.DefaultBracket >= Int32.Parse(drQtyPriceBrackets[bracket]["bracket"].ToString()))
                        {
                            bracketAwarded = bracket + 1;
                            this.ManualPrice = Decimal.Parse(drQtyPriceBrackets[bracket]["unit_price"].ToString());
                        }
                    }
                }
            }

            if (bracketAwarded > 0)
            {
                //Set the contract price record's parameters
                if (drQtyPriceBrackets[bracketAwarded - 1]["method"] != null)
                    this.Basis = drQtyPriceBrackets[bracketAwarded - 1]["method"].ToString();
                if (drQtyPriceBrackets[bracketAwarded - 1]["cost_basis"] != null)
                    this.CostBasis = drQtyPriceBrackets[bracketAwarded - 1]["cost_basis"].ToString();
                if (drQtyPriceBrackets[bracketAwarded - 1]["percentage"] != null)
                    this.Rate = Decimal.Parse(drQtyPriceBrackets[bracketAwarded - 1]["percentage"].ToString());
                if (drQtyPriceBrackets[bracketAwarded - 1]["addon_amt"] != null)
                    this.AddOnAmount = Decimal.Parse(drQtyPriceBrackets[bracketAwarded - 1]["addon_amt"].ToString());
                this.QuantityPrice = CalculatePrice();
            }
        }
        #endregion
    }
}