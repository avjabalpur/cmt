﻿using System;

namespace CenGen_Web_Services
{
    public class CustomerPriceResults
    {
        private Guid mCustId;
        private Guid mShipToId;
        private float mCustPrice;
        private float mListPrice;
        private float mReferencePrice;
        private string mPriceSource;
        private decimal mQtyAvailable;
        private string mMessage;
        private int mErrorCode;

        public Guid CustId
        {
            get { return mCustId; }
            set { mCustId = value; }
        }
        public Guid ShipToId
        {
            get { return mShipToId; }
            set { mShipToId = value; }
        }
        public float CustPrice
        {
            get { return mCustPrice; }
            set { mCustPrice = value; }
        }
        public float ListPrice
        {
            get { return mListPrice; }
            set { mListPrice = value; }
        }
        public float ReferencePrice
        {
            get { return mReferencePrice; }
            set { mReferencePrice = value; }
        }
        public string PriceSource
        {
            get { return mPriceSource; }
            set { mPriceSource = value; }
        }
        public decimal QtyAvailable
        {
            get { return mQtyAvailable; }
            set { mQtyAvailable = value; }
        }
        public string Message
        {
            get { return mMessage; }
            set { mMessage = value; }
        }
        public int ErrorCode
        {
            get { return mErrorCode; }
            set { mErrorCode = value; }
        }
    }
}