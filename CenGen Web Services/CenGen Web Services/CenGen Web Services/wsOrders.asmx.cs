﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Collections;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.IO;

namespace CenGen_Web_Services
{
    /// <summary>
    /// Summary description for wsOrders
    /// </summary>
    [WebService(Namespace = "http://www.cengentech.com")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class wsOrders : System.Web.Services.WebService
    {

        #region Class properties

        private string connString;
        CWSBLL.Decryption myDecryptor;

        #endregion

        #region Constructors
        public wsOrders()
        {
            //CODEGEN: This call is required by the ASP.NET Web Services Designer
            InitializeComponent();

            //Create Connection String for access to the database
            GetDbConnectionString();
        }
        #endregion

        #region Component Designer generated code

        //Required by the Web Services Designer 
        private IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion

        #region GetDbConnectionString
        private void GetDbConnectionString()
        {
            try
            {
                //Instantiate a new instance of the Decryption class
                //the encrypted string retrieved from the AppSettings Web.config file will be decrypted by this class
                myDecryptor = new CWSBLL.Decryption();

                //Instantiate a new instance of the appliation's property settings
                Properties.Settings mySettings = new Properties.Settings();

                //Set the connString variable equal to the ConnectionString setting
                connString = mySettings.ConnectionString;

                //The method DecryptString within the myDecryptor helper class is utilized to decrypt the encrypted string
                //stored in the connString string variable.
                connString = myDecryptor.DecryptString(connString);
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting database connection string from web.config file (Class | Method; wsCustomer | GetDbConnectionString). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion

        #region Stored Procedure, views, and functions required for this web service
        //p_WebOrderHdrInsert
        //p_WebOrderDtlInsert
        //p_GetOEDocument
        //f_ar_terms_desc
        //f_service_code_desc
        #endregion

        #region CreateWebOrder WebMethod
        [WebMethod]
        //public string CreateWebOrder(string DB_NAME, DataSet WebOrder)
        public string CreateWebOrder(string DB_NAME, string WebOrder)
        {
            string response = createWebOrder(DB_NAME, WebOrder);
            return response;

        }
        #endregion

        #region createWebOrder Class
        //private string createWebOrder(string DB_NAME, DataSet WebOrder)
        private string createWebOrder(string DB_NAME, string WebOrderXML)
        {
            DataSet WebOrder = new DataSet();

            StringReader sr = new StringReader(WebOrderXML);

            WebOrder.ReadXml(sr, XmlReadMode.ReadSchema);

            //Define Order Header parameters
            string ITDocID = string.Empty;
            string OutsideSalesrepID = string.Empty;
            int ShipByDays = 0;
            string Response = string.Empty;
            int resultVal;

            //set connection string text
            connString += "Database=" + DB_NAME + ";";

            //Create new connection
            SqlConnection conn = new SqlConnection(connString);

            //Open connection to database
            conn.Open();

            //Create new sql command object and assign it to the current connection
            SqlCommand cmd = conn.CreateCommand();

            //Create new sql transaction object
            SqlTransaction sqlTrans;

            // Start a local transaction
            sqlTrans = conn.BeginTransaction();

            // Must assign both transaction object and connection
            // to Command object for a pending local transaction
            cmd.Connection = conn;
            cmd.Transaction = sqlTrans;

            try
            {


                cmd.CommandText = "p_WebOrderHdrInsert";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["CUST_ID"];

                cmd.Parameters.Add("@SHIP2_ID", SqlDbType.VarChar);
                cmd.Parameters["@SHIP2_ID"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["SHIP2_ID"];

                cmd.Parameters.Add("@DEPT_ID", SqlDbType.VarChar);
                cmd.Parameters["@DEPT_ID"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["DEPT_ID"];

                cmd.Parameters.Add("@CUST_PO", SqlDbType.VarChar);
                cmd.Parameters["@CUST_PO"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["CUST_PO"];
                cmd.Parameters.Add("@ORDERED_BY", SqlDbType.VarChar);
                cmd.Parameters["@ORDERED_BY"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["ORDERED_BY"];
                cmd.Parameters.Add("@CARRIER", SqlDbType.VarChar);
                cmd.Parameters["@CARRIER"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["CARRIER"];
                cmd.Parameters.Add("@SERVICE_CODE", SqlDbType.VarChar);
                cmd.Parameters["@SERVICE_CODE"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["SERVICE_CODE"];
                cmd.Parameters.Add("@INSTRUCTIONS", SqlDbType.VarChar);
                cmd.Parameters["@INSTRUCTIONS"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["INSTRUCTIONS"];
                cmd.Parameters.Add("@ORDER_DATE", SqlDbType.DateTime);
                cmd.Parameters["@ORDER_DATE"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["ORDER_DATE"];
                cmd.Parameters.Add("@IS_TAXABLE", SqlDbType.Char);
                cmd.Parameters["@IS_TAXABLE"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["IS_TAXABLE"];
                cmd.Parameters.Add("@SHIP_BY_DATE", SqlDbType.DateTime);
                cmd.Parameters["@SHIP_BY_DATE"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["REQUESTED_SHIP_DATE"];
                cmd.Parameters.Add("@FRT_AMT", SqlDbType.Decimal);
                cmd.Parameters["@FRT_AMT"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["FRT_AMT"];
                cmd.Parameters.Add("@ITDocID", SqlDbType.VarChar, 255);
                cmd.Parameters["@ITDocID"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@OutsideSalesrepID", SqlDbType.VarChar, 255);
                cmd.Parameters["@OutsideSalesrepID"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@LATEST_DAYS_TO_SHIP", SqlDbType.Int, 4);
                cmd.Parameters["@LATEST_DAYS_TO_SHIP"].Direction = ParameterDirection.Output;

                resultVal = cmd.ExecuteNonQuery();

                ITDocID = cmd.Parameters["@ITDocID"].Value.ToString();
                OutsideSalesrepID = cmd.Parameters["@OutsideSalesrepID"].Value.ToString();
                ShipByDays = Int32.Parse(cmd.Parameters["@LATEST_DAYS_TO_SHIP"].Value.ToString());

                if (WebOrder.Tables["WebOrderHdr"].Rows[0]["SHIP_NO"] != null && WebOrder.Tables["WebOrderHdr"].Rows[0]["SHIP_NO"].ToString().Length > 0)
                {
                    //clear the sql command parameters in preperation for the next command
                    cmd.Parameters.Clear();

                    cmd.CommandText = "p_WebOrderOneTimeShipToInsert";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@IT_DOC_ID", SqlDbType.VarChar);
                    cmd.Parameters["@IT_DOC_ID"].Value = ITDocID;
                    cmd.Parameters.Add("@SHIP_NO", SqlDbType.VarChar);
                    cmd.Parameters["@SHIP_NO"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["SHIP_NO"];
                    cmd.Parameters.Add("@SHIP_NAME", SqlDbType.VarChar);
                    cmd.Parameters["@SHIP_NAME"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["SHIP_NAME"];
                    cmd.Parameters.Add("@ADDRESS1", SqlDbType.VarChar);
                    cmd.Parameters["@ADDRESS1"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["ADDRESS1"];
                    cmd.Parameters.Add("@ADDRESS2", SqlDbType.VarChar);
                    cmd.Parameters["@ADDRESS2"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["ADDRESS2"];
                    cmd.Parameters.Add("@ZIP_CODE_DTL_ID", SqlDbType.VarChar);
                    cmd.Parameters["@ZIP_CODE_DTL_ID"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["ZIP_CODE_DTL_ID"];
                    cmd.Parameters.Add("@ZIP4", SqlDbType.VarChar);
                    cmd.Parameters["@ZIP4"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["ZIP4"];
                    cmd.Parameters.Add("@TEL_NO", SqlDbType.VarChar);
                    cmd.Parameters["@TEL_NO"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["TEL_NO"];
                    cmd.Parameters.Add("@FAX_NO", SqlDbType.VarChar);
                    cmd.Parameters["@FAX_NO"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["FAX_NO"];
                    cmd.Parameters.Add("@IS_RESIDENCE", SqlDbType.VarChar);
                    cmd.Parameters["@IS_RESIDENCE"].Value = WebOrder.Tables["WebOrderHdr"].Rows[0]["IS_RESIDENCE"];
                    cmd.Parameters.Add("@ERROR_MSG", SqlDbType.VarChar, 1000);
                    cmd.Parameters["@ERROR_MSG"].Direction = ParameterDirection.Output;

                    resultVal = cmd.ExecuteNonQuery();

                    Response = cmd.Parameters["@ERROR_MSG"].Value.ToString();

                    if (Response.Length > 0)
                        return Response;
                }

                //clear the sql command parameters in preperation for the next command
                cmd.Parameters.Clear();

                cmd.CommandText = "p_WebOrderDtlInsert";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ITDocID", SqlDbType.VarChar);
                cmd.Parameters.Add("@OutsideSalesrepID", SqlDbType.VarChar);
                cmd.Parameters.Add("@LINE_NO", SqlDbType.VarChar);
                cmd.Parameters.Add("@ITEM_NO", SqlDbType.VarChar);
                cmd.Parameters.Add("@QTY_ORDERED", SqlDbType.VarChar);
                cmd.Parameters.Add("@QTY_UOM", SqlDbType.VarChar);
                cmd.Parameters.Add("@QTY_CONV", SqlDbType.VarChar);
                cmd.Parameters.Add("@LIST_PRICE", SqlDbType.VarChar);
                cmd.Parameters.Add("@CUST_PRICE", SqlDbType.VarChar);
                cmd.Parameters.Add("@PRICE_UOM", SqlDbType.VarChar);
                cmd.Parameters.Add("@PRICE_CONV", SqlDbType.VarChar);
                cmd.Parameters.Add("@PRICE_SOURCE", SqlDbType.VarChar);
                cmd.Parameters.Add("@CUST_PO", SqlDbType.VarChar);
                cmd.Parameters.Add("@OE_CONTRACT_ID", SqlDbType.VarChar);
                cmd.Parameters.Add("@IS_TAXABLE", SqlDbType.VarChar);
                cmd.Parameters.Add("@REQUESTED_SHIP_DATE", SqlDbType.VarChar);
                cmd.Parameters.Add("@ShipByDays", SqlDbType.Int);
                cmd.Parameters.Add("@ERROR_MSG", SqlDbType.VarChar, 1000);
                cmd.Parameters["@ERROR_MSG"].Direction = ParameterDirection.Output;

                //upload the web order lines
                for (int dtl = 0; dtl < WebOrder.Tables["WebOrderDtl"].Rows.Count; dtl++)
                {
                    cmd.Parameters["@ITDocID"].Value = ITDocID;
                    if (OutsideSalesrepID == "")
                        OutsideSalesrepID = null;
                    cmd.Parameters["@OutsideSalesrepID"].Value = OutsideSalesrepID;
                    cmd.Parameters["@LINE_NO"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["LINE_NO"];
                    cmd.Parameters["@ITEM_NO"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["ITEM_NO"];
                    cmd.Parameters["@QTY_ORDERED"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["QTY_ORDERED"];
                    cmd.Parameters["@QTY_UOM"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["QTY_UOM"];
                    cmd.Parameters["@QTY_CONV"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["QTY_CONV"];
                    cmd.Parameters["@LIST_PRICE"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["LIST_PRICE"];
                    cmd.Parameters["@CUST_PRICE"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["CUST_PRICE"];
                    cmd.Parameters["@PRICE_UOM"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["PRICE_UOM"];
                    cmd.Parameters["@PRICE_CONV"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["PRICE_CONV"];
                    cmd.Parameters["@PRICE_SOURCE"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["PRICE_SOURCE"];
                    cmd.Parameters["@CUST_PO"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["CUST_PO"];
                    cmd.Parameters["@OE_CONTRACT_ID"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["OE_CONTRACT_ID"];
                    cmd.Parameters["@IS_TAXABLE"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["IS_TAXABLE"];
                    cmd.Parameters["@REQUESTED_SHIP_DATE"].Value = WebOrder.Tables["WebOrderDtl"].Rows[dtl]["REQUESTED_SHIP_DATE"];
                    cmd.Parameters["@ShipByDays"].Value = ShipByDays;


                    resultVal = cmd.ExecuteNonQuery();

                    Response = cmd.Parameters["@ERROR_MSG"].Value.ToString();
                }

                //Since all records were written successfully, commit the transaction to the database
                sqlTrans.Commit();

                if (Response.Length == 0)
                    Response = "The order was uploaded successfully and is awaiting approval prior to release into the system as an order.";
                return Response;
            }
            catch (Exception e)
            {
                try
                {
                    sqlTrans.Rollback();
                }
                catch (SqlException ex)
                {
                    if (sqlTrans.Connection != null)
                    {
                        Response = "An exception of type " + ex.GetType() +
                            " was encountered while attempting to roll back the transaction.  Please contact your system administrator for assistance as data corruption could have occurred.";
                    }
                }

                Response = "An exception of type " + e.GetType() + " / " + e.Message +
                    " was encountered while inserting the order.  The order was not inserted into the database.";
                return Response;

            }
            finally
            {
                cmd.Dispose();
                sqlTrans.Dispose();
                conn.Close();
                conn.Dispose();
            }
        }
        #endregion

        #region GetOEDocument
        [WebMethod]
        public DataSet GetOEDocument(string DB_NAME, string OE_DOC_ID)
        {
            //creates a new dataset to store the result set from the database so you can pass back to the calling procedure
            DataSet dsOEDocument = new DataSet("OEDocument");

            //creates the connection string 
            connString += "Database=" + DB_NAME + ";";

            //creates the sql connection object
            SqlConnection conn;

            //creates the sql command object
            SqlCommand cmd;

            //creates the data adapter object
            SqlDataAdapter da;

            try
            {

                //enstantiates the new connection
                conn = new SqlConnection(connString);


                cmd = new SqlCommand("p_GetOEDocument", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OE_DOC_ID", SqlDbType.VarChar, 255);
                cmd.Parameters["@OE_DOC_ID"].Value = OE_DOC_ID;

                da = new SqlDataAdapter(cmd);

                da.Fill(dsOEDocument);

                dsOEDocument.Tables[0].TableName = "HEADER";
                dsOEDocument.Tables[1].TableName = "SHIPTO_ADDRESS";
                dsOEDocument.Tables[2].TableName = "ONE_TIME_SHIPTO_ADDRESS";
                dsOEDocument.Tables[3].TableName = "DOCUMENT_COMMENTS";
                dsOEDocument.Tables[4].TableName = "DETAILS";

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                return dsOEDocument;

            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving order entry document (class | Method; wsOrder | GetOEDocument). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting order entry document (Class | Method; wsOrder | GetOEDocument). " + ex.StackTrace);
            }
            finally
            {

            }
        }
        #endregion

        #region GetInvoiceRecords
        [WebMethod]
        public DataSet GetInvoiceRecords(string DB_NAME, string INVOICE_DATE, string CUST_ID)
        {
            DataSet dsCustInvoices = new DataSet("CustomerInvoices");
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataAdapter da;

            try
            {


                conn = new SqlConnection(connString);

                cmd = new SqlCommand("p_GetInvoices", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@INVOICE_DATE", SqlDbType.VarChar);
                cmd.Parameters["@INVOICE_DATE"].Value = INVOICE_DATE;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;

                da = new SqlDataAdapter(cmd);

                da.Fill(dsCustInvoices);

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                return dsCustInvoices;

            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | GetCustomerKardex). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | GetCustomerKardex). " + ex.StackTrace);
            }
            finally
            {

            }
        }

        #endregion

        #region TestWebOrderUpload
        [WebMethod]
        public string TestWebOrderUpload(string DB_NAME)
        {
            //create dataset and datatables
            DataSet dsWebOrder = new DataSet("WebOrder");
            DataTable dtHdr = new DataTable("WebOrderHdr");
            DataTable dtDtl = new DataTable("WebOrderDtl");

            //add the data tables to the dataset
            dsWebOrder.Tables.Add(dtHdr);
            dsWebOrder.Tables.Add(dtDtl);

            //create the data rows to be added to the data tables
            DataRow drWebOrderHdr;
            DataRow drWebOrderDtl1;
            DataRow drWebOrderDtl2;
            DataRow drWebOrderDtl3;

            try
            {

                //define the columns in the data table - drHdr
                dtHdr.Columns.Add("CUST_ID", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("SHIP2_ID", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("CUST_PO", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("ORDERED_BY", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("CARRIER", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("SERVICE_CODE", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("INSTRUCTIONS", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("ORDER_DATE", System.Type.GetType("System.DateTime"));
                dtHdr.Columns.Add("IS_TAXABLE", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("REQUESTED_SHIP_DATE", System.Type.GetType("System.DateTime"));
                dtHdr.Columns.Add("FRT_AMT", System.Type.GetType("System.Decimal"));
                dtHdr.Columns.Add("SHIP_NO", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("SHIP_NAME", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("ADDRESS1", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("ADDRESS2", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("ZIP_CODE_DTL_ID", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("ZIP4", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("TEL_NO", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("FAX_NO", System.Type.GetType("System.String"));
                dtHdr.Columns.Add("IS_RESIDENCE", System.Type.GetType("System.String"));


                //define the columns in the data table - drDtl
                dtDtl.Columns.Add("LINE_NO", System.Type.GetType("System.Int32"));
                dtDtl.Columns.Add("ITEM_NO", System.Type.GetType("System.String"));
                dtDtl.Columns.Add("QTY_ORDERED", System.Type.GetType("System.Decimal"));
                dtDtl.Columns.Add("QTY_UOM", System.Type.GetType("System.String"));
                dtDtl.Columns.Add("QTY_CONV", System.Type.GetType("System.Decimal"));
                dtDtl.Columns.Add("LIST_PRICE", System.Type.GetType("System.Decimal"));
                dtDtl.Columns.Add("CUST_PRICE", System.Type.GetType("System.Decimal"));
                dtDtl.Columns.Add("PRICE_UOM", System.Type.GetType("System.String"));
                dtDtl.Columns.Add("PRICE_CONV", System.Type.GetType("System.Decimal"));
                dtDtl.Columns.Add("PRICE_SOURCE", System.Type.GetType("System.String"));
                dtDtl.Columns.Add("CUST_PO", System.Type.GetType("System.String"));
                dtDtl.Columns.Add("OE_CONTRACT_ID", System.Type.GetType("System.String"));
                dtDtl.Columns.Add("IS_TAXABLE", System.Type.GetType("System.String"));
                dtDtl.Columns.Add("REQUESTED_SHIP_DATE", System.Type.GetType("System.String"));

                //Create the new row
                drWebOrderHdr = dtHdr.NewRow();

                //Assign values to each column in the row

                drWebOrderHdr["CUST_ID"] = "03272AD1-B2CB-4EA7-B400-7CB259D003B3";
                drWebOrderHdr["SHIP2_ID"] = "37B993AC-BF9D-4D34-AAF6-D3062F224101";
                drWebOrderHdr["CUST_PO"] = "123456";
                drWebOrderHdr["ORDERED_BY"] = "JOHNNY SMITH";
                drWebOrderHdr["CARRIER"] = "UPS";
                drWebOrderHdr["SERVICE_CODE"] = "G";
                drWebOrderHdr["INSTRUCTIONS"] = "PLEASE BRING TO BACK OF BUILDING AT LOADING DOCK";
                drWebOrderHdr["ORDER_DATE"] = "4/25/2007";
                drWebOrderHdr["IS_TAXABLE"] = "Y";
                drWebOrderHdr["REQUESTED_SHIP_DATE"] = "4/26/2007";
                drWebOrderHdr["FRT_AMT"] = 5.00;
                drWebOrderHdr["SHIP_NO"] = "9999";
                drWebOrderHdr["SHIP_NAME"] = "HOWARD SALES INC.";
                drWebOrderHdr["ADDRESS1"] = "8205 ZIONSVILLE RD";
                drWebOrderHdr["ADDRESS2"] = null;
                drWebOrderHdr["ZIP_CODE_DTL_ID"] = "0F9DA579-E67A-49A2-A90F-F0C4FE2BD66F";
                drWebOrderHdr["TEL_NO"] = "6825185811";
                drWebOrderHdr["FAX_NO"] = "6825188888";
                drWebOrderHdr["IS_RESIDENCE"] = "N";

                //assign the new row to the table
                dtHdr.Rows.Add(drWebOrderHdr);


                drWebOrderDtl1 = dtDtl.NewRow();

                drWebOrderDtl1["LINE_NO"] = 1;
                drWebOrderDtl1["ITEM_NO"] = "ACC12990";
                drWebOrderDtl1["QTY_ORDERED"] = 24;
                drWebOrderDtl1["QTY_UOM"] = "CS";
                drWebOrderDtl1["QTY_CONV"] = 24;
                drWebOrderDtl1["LIST_PRICE"] = 3.75;
                drWebOrderDtl1["CUST_PRICE"] = 3.245;
                drWebOrderDtl1["PRICE_UOM"] = "EA";
                drWebOrderDtl1["PRICE_CONV"] = 1;
                drWebOrderDtl1["PRICE_SOURCE"] = "CONTRACT";
                drWebOrderDtl1["CUST_PO"] = "123456";
                drWebOrderDtl1["OE_CONTRACT_ID"] = "61FD2EE0-2C98-41A2-964A-3977B753254A";
                drWebOrderDtl1["IS_TAXABLE"] = "Y";
                drWebOrderDtl1["REQUESTED_SHIP_DATE"] = "4/26/2007";

                dtDtl.Rows.Add(drWebOrderDtl1);

                drWebOrderDtl2 = dtDtl.NewRow();

                drWebOrderDtl2["LINE_NO"] = 2;
                drWebOrderDtl2["ITEM_NO"] = "ACC12990";
                drWebOrderDtl2["QTY_ORDERED"] = 1000;
                drWebOrderDtl2["QTY_UOM"] = "M";
                drWebOrderDtl2["QTY_CONV"] = 1000;
                drWebOrderDtl2["LIST_PRICE"] = 0.40;
                drWebOrderDtl2["CUST_PRICE"] = 0.211024;
                drWebOrderDtl2["PRICE_UOM"] = "M";
                drWebOrderDtl2["PRICE_CONV"] = 1000;
                drWebOrderDtl2["PRICE_SOURCE"] = "CONTRACT";
                drWebOrderDtl2["CUST_PO"] = "123456";
                drWebOrderDtl2["OE_CONTRACT_ID"] = "61FD2EE0-2C98-41A2-964A-3977B753254A";
                drWebOrderDtl2["IS_TAXABLE"] = "Y";
                drWebOrderDtl2["REQUESTED_SHIP_DATE"] = "4/26/2007";

                dtDtl.Rows.Add(drWebOrderDtl2);

                drWebOrderDtl3 = dtDtl.NewRow();

                drWebOrderDtl3["LINE_NO"] = 3;
                drWebOrderDtl3["ITEM_NO"] = "ACC12990";
                drWebOrderDtl3["QTY_ORDERED"] = 1000;
                drWebOrderDtl3["QTY_UOM"] = "M";
                drWebOrderDtl3["QTY_CONV"] = 1000;
                drWebOrderDtl3["LIST_PRICE"] = 0.45;
                drWebOrderDtl3["CUST_PRICE"] = 0.2398;
                drWebOrderDtl3["PRICE_UOM"] = "M";
                drWebOrderDtl3["PRICE_CONV"] = 1000;
                drWebOrderDtl3["PRICE_SOURCE"] = "CONTRACT";
                drWebOrderDtl3["CUST_PO"] = "123456";
                drWebOrderDtl3["OE_CONTRACT_ID"] = "61FD2EE0-2C98-41A2-964A-3977B753254A";
                drWebOrderDtl3["IS_TAXABLE"] = "Y";
                drWebOrderDtl3["REQUESTED_SHIP_DATE"] = "4/26/2007";

                dtDtl.Rows.Add(drWebOrderDtl3);

                //dsWebOrder.WriteXmlSchema(@"C:\WebOrder.xsd");
                //dsWebOrder.WriteXml(@"C:\WebOrder.xml");
                dsWebOrder.WriteXml(@"C:\WebOrder.xml", XmlWriteMode.WriteSchema);

                //wsOrders OrdersWebService = new wsOrders("http://localhost/CenGen Web Services/Orders/wsOrders.asmx");

                // string Response = createWebOrder(DB_NAME, dsWebOrder);

                // return Response;
                return "";

            }
            catch (SqlException sqlex)
            {
                return sqlex.Message;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {

            }
        }
        #endregion

        #region ASPDNSF_RemoteOrderHdrUpload
        [WebMethod]
        public int ASPDNSF_RemoteOrderHdrUpload(string ITDocId, int OrderNumber, string CustID, string ShipToID, string CarrierID, string ServiceCodeID, DateTime OrderDate, DateTime EarliestShipDate, string CustPO, string OrderedBy, decimal FrtOutAmt, decimal OrderAmt, decimal TaxAmt, string EmailAddress, string PaymentMethod, string IsBlindShipment, string DB_NAME)
        {
            int resultVal;
            int UploadStatus;

            //set connection string text
            connString += "Database=" + DB_NAME + ";";

            //Create new connection
            SqlConnection conn = new SqlConnection(connString);

            //Open connection to database
            conn.Open();

            //Create new sql command object and assign it to the current connection
            SqlCommand cmd = conn.CreateCommand();

            cmd.Connection = conn;

            try
            {


                cmd.CommandText = "p_aspdnsfOrderHdrUpload";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ITDocID", SqlDbType.VarChar);
                cmd.Parameters["@ITDocID"].Value = ITDocId;
                cmd.Parameters.Add("@ECatOrderNo", SqlDbType.Int);
                cmd.Parameters["@ECatOrderNo"].Value = OrderNumber;
                cmd.Parameters.Add("@CustID", SqlDbType.VarChar);
                cmd.Parameters["@CustID"].Value = CustID;
                cmd.Parameters.Add("@ShipToID", SqlDbType.VarChar);
                cmd.Parameters["@ShipToID"].Value = ShipToID;
                cmd.Parameters.Add("@CarrierID", SqlDbType.VarChar);
                cmd.Parameters["@CarrierID"].Value = CarrierID;
                cmd.Parameters.Add("@ServiceCodeID", SqlDbType.VarChar);
                cmd.Parameters["@ServiceCodeID"].Value = ServiceCodeID;
                cmd.Parameters.Add("@DocDate", SqlDbType.DateTime);
                cmd.Parameters["@DocDate"].Value = OrderDate;
                cmd.Parameters.Add("@RelDate", SqlDbType.DateTime);
                cmd.Parameters["@RelDate"].Value = EarliestShipDate;
                cmd.Parameters.Add("@CustPO", SqlDbType.VarChar);
                cmd.Parameters["@CustPO"].Value = CustPO;
                cmd.Parameters.Add("@OrderedBy", SqlDbType.VarChar);
                cmd.Parameters["@OrderedBy"].Value = OrderedBy;

                cmd.Parameters.Add("@FrtOutAmt", SqlDbType.Decimal);
                cmd.Parameters["@FrtOutAmt"].Value = FrtOutAmt;

                cmd.Parameters.Add("@OrderAmt", SqlDbType.Decimal);
                cmd.Parameters["@OrderAmt"].Value = OrderAmt;

                cmd.Parameters.Add("@TaxAmt", SqlDbType.Decimal);
                cmd.Parameters["@TaxAmt"].Value = TaxAmt;

                cmd.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 255);
                cmd.Parameters["@EmailAddress"].Value = EmailAddress;

                cmd.Parameters.Add("@PmtMethod", SqlDbType.Char, 1);
                cmd.Parameters["@PmtMethod"].Value = PaymentMethod;

                cmd.Parameters.Add("@IsBlindShipment", SqlDbType.Char, 1);
                cmd.Parameters["@IsBlindShipment"].Value = IsBlindShipment;

                cmd.Parameters.Add("@UploadStatus", SqlDbType.Int, 4);
                cmd.Parameters["@UploadStatus"].Direction = ParameterDirection.Output;

                resultVal = cmd.ExecuteNonQuery();

                UploadStatus = Int32.Parse(cmd.Parameters["@UploadStatus"].Value.ToString());

                return UploadStatus;
            }
            catch (Exception e)
            {
                UploadStatus = 1;
                return UploadStatus;

            }
            finally
            {
                cmd.Dispose();
                conn.Close();
                conn.Dispose();
            }
        }
        #endregion

        #region ASPDNSF_RemoteOrderHdrCommentsUpload
        [WebMethod]
        public int ASPDNSF_RemoteOrderHdrCommentsUpload(Guid ITDocId, Guid ITCommentsID, string Comment, int OrderNumber, string DB_NAME)
        {
            int resultVal;
            int UploadStatus;
            char PrintCtrl = 'Y';
            char PrintPlacement = 'T';

            //set connection string text
            connString += "Database=" + DB_NAME + ";";

            //Create new connection
            SqlConnection conn = new SqlConnection(connString);

            //Open connection to database
            conn.Open();

            //Create new sql command object and assign it to the current connection
            SqlCommand cmd = conn.CreateCommand();


            cmd.Connection = conn;

            try
            {


                cmd.Parameters.Add("@ITDocID", SqlDbType.UniqueIdentifier);
                cmd.Parameters.Add("@ITCommentsID", SqlDbType.UniqueIdentifier);
                cmd.Parameters.Add("@Comment", SqlDbType.VarChar, 255);
                cmd.Parameters.Add("@PrintCtrl", SqlDbType.Char, 1);
                cmd.Parameters.Add("@PrintPlacement", SqlDbType.Char, 1);
                cmd.Parameters.Add("@ECatOrderNo", SqlDbType.Int);
                cmd.Parameters.Add("@UploadStatus", SqlDbType.Int);
                cmd.Parameters["@UploadStatus"].Direction = ParameterDirection.Output;

                cmd.Parameters["@ITDocID"].Value = ITDocId;
                cmd.Parameters["@ITCommentsID"].Value = ITCommentsID;
                cmd.Parameters["@Comment"].Value = Comment;
                cmd.Parameters["@PrintCtrl"].Value = PrintCtrl;
                cmd.Parameters["@PrintPlacement"].Value = PrintPlacement;
                cmd.Parameters["@ECatOrderNo"].Value = OrderNumber;

                resultVal = cmd.ExecuteNonQuery();

                UploadStatus = Int32.Parse(cmd.Parameters["@UploadStatus"].Value.ToString());

                return UploadStatus;
            }
            catch (Exception e)
            {
                UploadStatus = 1;
                return UploadStatus;

            }
            finally
            {
                cmd.Dispose();
                conn.Close();
                conn.Dispose();
            }
        }
        #endregion

        #region ASPDNSF_RemoteOrderBillToUpload
        [WebMethod]
        public int ASPDNSF_RemoteOrderBillToUpload(Guid ITDocId, string BillToNo, string BillToName, string BillToAddr1, string BillToAddr2, string BillToSuite, string BillToCity, string BillToState, string BillToZip, string BillToCountry, string BillToPhone, int OrderNumber, string DB_NAME)
        {
            int resultVal;
            int UploadStatus;

            //set connection string text
            connString += "Database=" + DB_NAME + ";";

            //Create new connection
            SqlConnection conn = new SqlConnection(connString);

            //Open connection to database
            conn.Open();

            //Create new sql command object and assign it to the current connection
            SqlCommand cmd = conn.CreateCommand();


            cmd.Connection = conn;

            try
            {


                cmd.Parameters.Add("@ITDocID", SqlDbType.UniqueIdentifier);
                cmd.Parameters.Add("@BillToNo", SqlDbType.VarChar, 8);
                cmd.Parameters.Add("@BillToName", SqlDbType.VarChar, 50);
                cmd.Parameters.Add("@BillToAddr1", SqlDbType.VarChar, 100);
                cmd.Parameters.Add("@BillToAddr2", SqlDbType.VarChar, 100);
                cmd.Parameters.Add("@BillToSuite", SqlDbType.VarChar, 50);
                cmd.Parameters.Add("@BillToCity", SqlDbType.VarChar, 100);
                cmd.Parameters.Add("@BillToState", SqlDbType.VarChar, 100);
                cmd.Parameters.Add("@BillToZip", SqlDbType.VarChar, 10);
                cmd.Parameters.Add("@BillToCountry", SqlDbType.VarChar, 100);
                cmd.Parameters.Add("@BillToPhone", SqlDbType.VarChar, 25);
                cmd.Parameters.Add("@ECatOrderNo", SqlDbType.Int);

                cmd.Parameters.Add("@UploadStatus", SqlDbType.Int);
                cmd.Parameters["@UploadStatus"].Direction = ParameterDirection.Output;

                cmd.Parameters["@ITDocID"].Value = ITDocId;
                cmd.Parameters["@BillToNo"].Value = BillToNo;
                cmd.Parameters["@BillToName"].Value = BillToName;
                cmd.Parameters["@BillToAddr1"].Value = BillToAddr1;
                cmd.Parameters["@BillToAddr2"].Value = BillToAddr2;
                cmd.Parameters["@BillToSuite"].Value = BillToSuite;
                cmd.Parameters["@BillToCity"].Value = BillToCity;
                cmd.Parameters["@BillToState"].Value = BillToState;
                cmd.Parameters["@BillToZip"].Value = BillToZip;
                cmd.Parameters["@BillToCountry"].Value = BillToCountry;
                cmd.Parameters["@BillToPhone"].Value = BillToPhone;
                cmd.Parameters["@ECatOrderNo"].Value = OrderNumber;

                resultVal = cmd.ExecuteNonQuery();

                UploadStatus = Int32.Parse(cmd.Parameters["@UploadStatus"].Value.ToString());

                return UploadStatus;
            }
            catch (Exception e)
            {
                UploadStatus = 1;
                return UploadStatus;

            }
            finally
            {
                cmd.Dispose();
                conn.Close();
                conn.Dispose();
            }
        }
        #endregion

        #region ASPDNSF_RemoteOrderShipToUpload
        [WebMethod]
        public int ASPDNSF_RemoteOrderShipToUpload(Guid ITDocId, string ShipToNo, string ShipToName, string ShipToAddr1, string ShipToAddr2, string ShipToSuite, string ShipToCity, string ShipToState, string ShipToZip, string ShipToCountry, string ShipToPhone, char IsResidence, bool IsBlindShipment, int OrderNumber, string DB_NAME)
        {
            int resultVal;
            int UploadStatus;

            //set connection string text
            connString += "Database=" + DB_NAME + ";";

            //Create new connection
            SqlConnection conn = new SqlConnection(connString);

            //Open connection to database
            conn.Open();

            //Create new sql command object and assign it to the current connection
            SqlCommand cmd = conn.CreateCommand();


            cmd.Connection = conn;

            try
            {


                cmd.Parameters.Add("@ITDocID", SqlDbType.UniqueIdentifier);
                cmd.Parameters.Add("@ShipToNo", SqlDbType.VarChar, 8);
                cmd.Parameters.Add("@ShipToName", SqlDbType.VarChar, 50);
                cmd.Parameters.Add("@ShipToAddr1", SqlDbType.VarChar, 100);
                cmd.Parameters.Add("@ShipToAddr2", SqlDbType.VarChar, 100);
                cmd.Parameters.Add("@ShipToSuite", SqlDbType.VarChar, 50);
                cmd.Parameters.Add("@ShipToCity", SqlDbType.VarChar, 100);
                cmd.Parameters.Add("@ShipToState", SqlDbType.VarChar, 100);
                cmd.Parameters.Add("@ShipToZip", SqlDbType.VarChar, 10);
                cmd.Parameters.Add("@ShipToCountry", SqlDbType.VarChar, 100);
                cmd.Parameters.Add("@ShipToPhone", SqlDbType.VarChar, 25);
                cmd.Parameters.Add("@IsResidence", SqlDbType.Char, 1);
                cmd.Parameters.Add("@ECatOrderNo", SqlDbType.Int);
                cmd.Parameters.Add("@IsBlindShipment", SqlDbType.Bit);
                cmd.Parameters.Add("@UploadStatus", SqlDbType.Int);
                cmd.Parameters["@UploadStatus"].Direction = ParameterDirection.Output;

                cmd.Parameters["@ITDocID"].Value = ITDocId;
                cmd.Parameters["@ShipToNo"].Value = ShipToNo;
                cmd.Parameters["@ShipToName"].Value = ShipToName;
                cmd.Parameters["@ShipToAddr1"].Value = ShipToAddr1;
                cmd.Parameters["@ShipToAddr2"].Value = ShipToAddr2;
                cmd.Parameters["@ShipToSuite"].Value = ShipToSuite;
                cmd.Parameters["@ShipToCity"].Value = ShipToCity;
                cmd.Parameters["@ShipToState"].Value = ShipToState;
                cmd.Parameters["@ShipToZip"].Value = ShipToZip;
                cmd.Parameters["@ShipToCountry"].Value = ShipToCountry;
                cmd.Parameters["@ShipToPhone"].Value = ShipToPhone;
                cmd.Parameters["@IsResidence"].Value = IsResidence;
                cmd.Parameters["@ECatOrderNo"].Value = OrderNumber;
                cmd.Parameters["@IsBlindShipment"].Value = IsBlindShipment;

                resultVal = cmd.ExecuteNonQuery();

                UploadStatus = Int32.Parse(cmd.Parameters["@UploadStatus"].Value.ToString());

                return UploadStatus;
            }
            catch (Exception e)
            {
                UploadStatus = 1;
                return UploadStatus;

            }
            finally
            {
                cmd.Dispose();
                conn.Close();
                conn.Dispose();
            }
        }
        #endregion

        #region ASPDNSF_RemoteOrderDtlUpload
        [WebMethod]
        public int ASPDNSF_RemoteOrderDtlUpload(string ITDocId, string ITDocDtlId, int LineNumber, Guid ProductGuid, string ProductNumber, string Description, string PriceSource, string IsTaxable, Decimal QtyOrdered, string QtyUM, Decimal QtyConv, Decimal ListPrice, Decimal CustPrice, Decimal ExtendedAmount, int ShoppingCartRecId, string DB_NAME)
        {
            int resultVal;
            int UploadStatus;
            string PriceString = string.Format("{0:C}", ExtendedAmount);

            //set connection string text
            connString += "Database=" + DB_NAME + ";";

            //Create new connection
            SqlConnection conn = new SqlConnection(connString);

            //Open connection to database
            conn.Open();

            //Create new sql command object and assign it to the current connection
            SqlCommand cmd = conn.CreateCommand();

            //Create new sql transaction object
            SqlTransaction sqlTrans;

            // Start a local transaction
            //sqlTrans = conn.BeginTransaction();

            // Must assign both transaction object and connection
            // to Command object for a pending local transaction
            cmd.Connection = conn;
            //cmd.Transaction = sqlTrans;

            try
            {


                cmd.Parameters.Add("@ITDocID", SqlDbType.UniqueIdentifier);
                cmd.Parameters.Add("@ITDocDtlID", SqlDbType.UniqueIdentifier);
                cmd.Parameters.Add("@SortOrder", SqlDbType.Int);
                cmd.Parameters.Add("@ItemID", SqlDbType.UniqueIdentifier);
                cmd.Parameters.Add("@ItemNo", SqlDbType.VarChar, 32);
                cmd.Parameters.Add("@Description", SqlDbType.VarChar, 50);
                cmd.Parameters.Add("@PriceSource", SqlDbType.VarChar, 2);
                cmd.Parameters.Add("@IsTaxable", SqlDbType.Char, 1);
                cmd.Parameters.Add("@QtyOrdered", SqlDbType.Decimal);
                cmd.Parameters.Add("@QtyUM", SqlDbType.VarChar, 2);
                cmd.Parameters.Add("@QtyConv", SqlDbType.Decimal);
                cmd.Parameters.Add("@ListPrice", SqlDbType.Decimal);
                cmd.Parameters.Add("@CustPrice", SqlDbType.Decimal);
                cmd.Parameters.Add("@ExtendedPrice", SqlDbType.Decimal);
                cmd.Parameters.Add("@PriceString", SqlDbType.VarChar, 30);
                cmd.Parameters.Add("@ShoppingCartRecID", SqlDbType.Int);
                cmd.Parameters.Add("@UploadStatus", SqlDbType.Int);
                cmd.Parameters["@UploadStatus"].Direction = ParameterDirection.Output;

                cmd.Parameters["@ITDocID"].Value = ITDocId;
                cmd.Parameters["@ITDocDtlID"].Value = ITDocDtlId;
                cmd.Parameters["@SortOrder"].Value = LineNumber;
                cmd.Parameters["@ItemID"].Value = ProductGuid;
                cmd.Parameters["@ItemNo"].Value = ProductNumber;
                cmd.Parameters["@Description"].Value = Description;
                cmd.Parameters["@PriceSource"].Value = PriceSource;
                cmd.Parameters["@IsTaxable"].Value = IsTaxable;
                cmd.Parameters["@QtyOrdered"].Value = QtyOrdered;
                cmd.Parameters["@QtyUM"].Value = QtyUM;
                cmd.Parameters["@QtyConv"].Value = QtyConv;
                cmd.Parameters["@ListPrice"].Value = ListPrice;
                cmd.Parameters["@CustPrice"].Value = CustPrice;
                cmd.Parameters["@ExtendedPrice"].Value = ExtendedAmount;
                cmd.Parameters["@PriceString"].Value = PriceString;
                cmd.Parameters["@ShoppingCartRecID"].Value = ShoppingCartRecId;

                resultVal = cmd.ExecuteNonQuery();

                UploadStatus = Int32.Parse(cmd.Parameters["@UploadStatus"].Value.ToString());

                return UploadStatus;
            }
            catch (Exception e)
            {
                UploadStatus = 1;
                return UploadStatus;

            }
            finally
            {
                cmd.Dispose();
                //sqlTrans.Dispose();
                conn.Close();
                conn.Dispose();
            }
        }
        #endregion



    }
}
