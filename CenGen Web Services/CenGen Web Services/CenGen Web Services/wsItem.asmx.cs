﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Configuration;
using System.ComponentModel;

namespace CenGen_Web_Services
{
    /// <summary>
    /// Summary description for wsItem
    /// </summary>
    [WebService(Namespace = "http://www.cengentech.com")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class wsItem : System.Web.Services.WebService
    {

        private string connString;
        CWSBLL.Decryption myDecryptor;

        //Create an instance of the WebConfigurationManager to read the AppSettings
        System.Configuration.Configuration configMgr;

        public wsItem()
        {
            //CODEGEN: This call is required by the ASP.NET Web Services Designer
            InitializeComponent();

            //Create Connection String for access to the database
            GetDbConnectionString();
        }

        #region Component Designer generated code

        //Required by the Web Services Designer 
        private IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion

        #region GetDbConnectionString
        private void GetDbConnectionString()
        {
            try
            {
                //Instantiate a new instance of the Decryption class
                //the encrypted string retrieved from the AppSettings Web.config file will be decrypted by this class
                myDecryptor = new CWSBLL.Decryption();

                //Instantiate a new instance of the appliation's property settings
                Properties.Settings mySettings = new Properties.Settings();

                //Set the connString variable equal to the ConnectionString setting
                connString = mySettings.ConnectionString;

                //The method DecryptString within the myDecryptor helper class is utilized to decrypt the encrypted string
                //stored in the connString string variable.
                connString = myDecryptor.DecryptString(connString);
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting database connection string from web.config file (Class | Method; wsCustomer | GetDbConnectionString). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion

        #region Stored Procedures, views, and functions required for this web service
        //p_GetItemQtyAvailable - stored procedure to get the selected item's quantity available
        #endregion

        #region GetItemQtyAvailable
        [WebMethod]
        public Decimal GetItemQtyAvailable(string DB_NAME, string ITEM_NO, string WHSE_CODE)
        {
            Decimal QtyAvailable = 0;
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;

            string CUST_ID = string.Empty;
            string SHIP2_ID = string.Empty;

            try
            {
                conn = new SqlConnection(connString);

                cmd = new SqlCommand("p_GetItemQtyAvailable", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ITEM_NO", SqlDbType.VarChar);
                cmd.Parameters["@ITEM_NO"].Value = ITEM_NO;
                cmd.Parameters.Add("@WHSE_CODE", SqlDbType.VarChar);
                cmd.Parameters["@WHSE_CODE"].Value = WHSE_CODE;
                cmd.Parameters.Add("@QtyAvailable", SqlDbType.Decimal);
                cmd.Parameters["@QtyAvailable"].Direction = ParameterDirection.Output;

                conn.Open();

                cmd.ExecuteNonQuery();

                QtyAvailable = Decimal.Parse(cmd.Parameters["@QtyAvailable"].Value.ToString());

                conn.Close();
                conn.Dispose();
                cmd.Dispose();

                return QtyAvailable;

            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving item quantity on hand (class | Method; wsItem | GetItemQtyAvailable). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting item quantity on hand (Class | Method; wsItem | GetItemQtyAvailable). " + ex.StackTrace);
            }
            finally
            {

            }
        }
        #endregion

        #region SyncItemData
        [WebMethod]
        public DataSet SyncItemData(string DB_NAME, DateTime LAST_SYNC_DATE)
        {
            DataSet dsSyncResults = new DataSet("SyncResults");

            //DataTable dtItemListDateUpdated = dsSyncResults.Tables.Add("ItemListDateUpdated");

            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataAdapter da;


            try
            {
                conn = new SqlConnection(connString);

                cmd = new SqlCommand("p_GetNewAndChangedItemPricing", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@lastSyncDate", SqlDbType.VarChar);
                cmd.Parameters["@lastSyncDate"].Value = LAST_SYNC_DATE;

                da = new SqlDataAdapter(cmd);

                da.Fill(dsSyncResults);

                dsSyncResults.Tables[0].TableName = "ItemListPriceDateCreated";
                dsSyncResults.Tables[1].TableName = "ItemListDateUpdated";
                dsSyncResults.Tables[2].TableName = "ItemQtyPriceDateCreated";
                dsSyncResults.Tables[3].TableName = "ItemQtyPriceDateUpdated";
                dsSyncResults.Tables[4].TableName = "ItemContractDateCreated";
                dsSyncResults.Tables[5].TableName = "ItemContractDateUpdated";

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                return dsSyncResults;


            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving item sync data (class | Method; wsItem | SyncItemData). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting item sync data (Class | Method; wsItem | SyncItemData). " + ex.StackTrace);
            }
            finally
            {

            }
        }
        #endregion


        /// <summary>
        /// Returns the Item information based on the ItemId (Guid) passed into the web method.
        /// </summary>
        /// <param name="ItemId"></param>
        /// <param name="DB_NAME"></param>
        /// <returns>json result set</returns>
        #region GetItemByGuid
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public Item GetItemByGuid(Guid ItemId, string DB_NAME)
        {

            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataReader dr = null;
            Item item = new Item();
            List<ItemExtendedDescription> xdescs = new List<ItemExtendedDescription>();
            List<WhseItem> whseItems = new List<WhseItem>();

            try
            {
                conn = new SqlConnection(connString);


                cmd = new SqlCommand("p_cds_GetItemByGuid", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@item_id", SqlDbType.UniqueIdentifier, 32);
                cmd.Parameters.Add("@error_message", SqlDbType.VarChar, 4000);
                cmd.Parameters["@error_message"].Direction = ParameterDirection.Output;

                cmd.Parameters["@item_id"].Value = ItemId;

                conn.Open();


                dr = cmd.ExecuteReader();

                item.ErrorMessage = cmd.Parameters["@error_message"].Value.ToString();

                while (dr.Read())
                {
                    item.ItemId = dr.GetGuid(0);
                    item.ItemNumber = dr.GetString(1);
                    item.Description = dr.GetString(2);
                    item.ListPrice = dr.GetDecimal(3);
                    item.StdCost = dr.GetDecimal(4);
                    item.LastCost = dr.GetDecimal(5);
                    item.UnitWeight = dr.GetDecimal(6);
                    item.UnitVolume = dr.GetDecimal(7);
                    item.DateAdded = dr.GetDateTime(8);
                    item.LastChangeDate = dr.GetDateTime(9);
                    item.SkuUM = dr.GetString(10);
                    item.QtyUM = dr.GetString(11);
                    item.QtyConv = dr.GetDecimal(12);
                    item.WarrantyMonths = dr.GetInt32(13);
                    item.IsTaxable = dr.GetString(14);
                    item.GroupCode = dr.GetString(15);
                    item.ExtendedDescription = xdescs;
                }

                dr.NextResult();

                while (dr.Read())
                {
                    ItemExtendedDescription xdesc = new ItemExtendedDescription();
                    xdesc.XdescId = dr.GetGuid(0);
                    xdesc.ItemId = dr.GetGuid(1);
                    xdesc.SequenceNumber = dr.GetInt32(2);
                    xdesc.LanguageCode = dr.GetString(3);
                    xdesc.Description = dr.GetTextReader(4).ReadToEnd();
                    xdesc.DescriptionPlainText = dr.GetString(5);
                    xdescs.Add(xdesc);
                }

                dr.NextResult();

                while (dr.Read())
                {
                    WhseItem whseItem = new WhseItem();
                    whseItem.ItemId = dr.GetGuid(0);
                    whseItem.WhseId = dr.GetGuid(1);
                    whseItem.WhseCode = dr.GetString(2);
                    whseItem.QtyOnHand = dr.GetDecimal(3);
                    whseItem.CommitTodayQty = dr.GetDecimal(4);
                    whseItem.CommitLaterQty = dr.GetDecimal(5);
                    whseItem.QtyRework = dr.GetDecimal(6);
                    whseItem.QtyDamaged = dr.GetDecimal(7);
                    whseItem.AvgCost = dr.GetDecimal(8);
                    whseItem.AvgLeadTimeDays = dr.GetDecimal(9);
                    whseItem.OnOrderPO = dr.GetDecimal(10);
                    whseItem.OnOrderTransfers = dr.GetDecimal(11);
                    whseItems.Add(whseItem);
                }

                conn.Close();
                conn.Dispose();
                cmd.Dispose();

                return item;

            }
            catch (SqlException sqlex)
            {
                item.ErrorMessage = sqlex.Message;
                return item;
            }
            catch (Exception ex)
            {
                item.ErrorMessage = ex.Message;
                return item;
            }
        }
        #endregion

        /// <summary>
        /// Returns a collection of items where the item's last change date is greater than the date passed to the web method
        /// </summary>
        /// <param name="LastSyncDate"></param>
        /// <param name="DB_NAME"></param>
        /// <returns>json collection of items and their extended descriptions</returns>
        #region GetItemsChangedAsOfDate
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<Item> GetItemsChangedAsOfDate(DateTime AsOfDate, string DB_NAME)
        {

            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataReader dr = null;
            List<Item> items = new List<Item>();
            List<ItemExtendedDescription> xdescss = new List<ItemExtendedDescription>();
            List<WhseItem> whseItems = new List<WhseItem>();

            try
            {
                conn = new SqlConnection(connString);


                cmd = new SqlCommand("p_cds_GetItemsChangedAsOfDate", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@AsOfDate", SqlDbType.DateTime);
                cmd.Parameters.Add("@error_message", SqlDbType.VarChar, 4000);
                cmd.Parameters["@error_message"].Direction = ParameterDirection.Output;

                cmd.Parameters["@AsOfDate"].Value = AsOfDate;

                conn.Open();


                dr = cmd.ExecuteReader();

                if (cmd.Parameters["@error_message"].Value.ToString() != "")
                {
                    Item item = new Item();
                    item.ErrorMessage = cmd.Parameters["@error_message"].Value.ToString();
                    items.Add(item);
                    return items;
                }

                while (dr.Read())
                {
                    Item item = new Item();
                    List<ItemExtendedDescription> xdescs = new List<ItemExtendedDescription>();

                    item.ItemId = dr.GetGuid(0);
                    item.ItemNumber = dr.GetString(1);
                    item.Description = dr.GetString(2);
                    item.ListPrice = dr.GetDecimal(3);
                    item.StdCost = dr.GetDecimal(4);
                    item.LastCost = dr.GetDecimal(5);
                    item.UnitWeight = dr.GetDecimal(6);
                    item.UnitVolume = dr.GetDecimal(7);
                    item.DateAdded = dr.GetDateTime(8);
                    item.LastChangeDate = dr.GetDateTime(9);
                    item.SkuUM = dr.GetString(10);
                    item.QtyUM = dr.GetString(11);
                    item.QtyConv = dr.GetDecimal(12);
                    item.WarrantyMonths = dr.GetInt32(13);
                    item.IsTaxable = dr.GetString(14);
                    item.GroupCode = dr.GetString(15);
                    item.ExtendedDescription = xdescs;
                }

                dr.NextResult();

                while (dr.Read())
                {
                    ItemExtendedDescription xdesc = new ItemExtendedDescription();
                    xdesc.XdescId = dr.GetGuid(0);
                    xdesc.ItemId = dr.GetGuid(1);
                    xdesc.SequenceNumber = dr.GetInt32(2);
                    xdesc.LanguageCode = dr.GetString(3);
                    xdesc.Description = dr.GetTextReader(4).ReadToEnd();
                    xdesc.DescriptionPlainText = dr.GetString(5);
                    xdescss.Add(xdesc);
                }

                dr.NextResult();

                while (dr.Read())
                {
                    WhseItem whseItem = new WhseItem();
                    whseItem.ItemId = dr.GetGuid(0);
                    whseItem.WhseId = dr.GetGuid(1);
                    whseItem.WhseCode = dr.GetString(2);
                    whseItem.QtyOnHand = dr.GetDecimal(3);
                    whseItem.CommitTodayQty = dr.GetDecimal(4);
                    whseItem.CommitLaterQty = dr.GetDecimal(5);
                    whseItem.QtyRework = dr.GetDecimal(6);
                    whseItem.QtyDamaged = dr.GetDecimal(7);
                    whseItem.AvgCost = dr.GetDecimal(8);
                    whseItem.AvgLeadTimeDays = dr.GetDecimal(9);
                    whseItem.OnOrderPO = dr.GetDecimal(10);
                    whseItem.OnOrderTransfers = dr.GetDecimal(11);
                    whseItems.Add(whseItem);
                }
                
                    foreach (ItemExtendedDescription xdesc in xdescss)
                    {
                        foreach (Item item in items)
                        {
                            if (item.ItemId == xdesc.ItemId)
                                item.ExtendedDescription.Add(xdesc);
                        }
                    }

                    foreach (WhseItem whseitem in whseItems)
                    {
                        foreach (Item item in items)
                        {
                            if (item.ItemId == whseitem.ItemId)
                                item.WarehouseItem.Add(whseitem);
                        }
                    }

                conn.Close();
                conn.Dispose();
                cmd.Dispose();

                return items;

            }
            catch (SqlException sqlex)
            {
                Item item = new Item();
                item.ErrorMessage = sqlex.Message;
                items.Add(item);
                return items;
            }
            catch (Exception ex)
            {
                Item item = new Item();
                item.ErrorMessage = ex.Message;
                items.Add(item);
                return items;
            }
            finally
            {


            }
        }
        #endregion

    }
}

