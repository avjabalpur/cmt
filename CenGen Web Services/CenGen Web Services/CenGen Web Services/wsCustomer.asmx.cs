﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using System.Collections;
using System.ComponentModel;

namespace CenGen_Web_Services
{
    /// <summary>
    /// Summary description for wsCustomer
    /// </summary>
    [WebService(Namespace = "http://www.cengentech.com")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class wsCustomer : System.Web.Services.WebService
    {

        #region Class Properties
        //initialize the error logging parameters
        private string sErrorClass = "wsCustomer";
        private string sErrorMethod = string.Empty;
        private string sErrorStep = string.Empty;

        private const int ItemMasterTable = 0;
        private const int WhseItemMasterTable = 1;
        private const int ShipToTable = 2;
        private const int ItemReferencePriceTable = 3;

        private string connString;
        CWSBLL.Decryption myDecryptor;

        #endregion

        #region Constructors
        public wsCustomer()
        {
            //CODEGEN: This call is required by the ASP.NET Web Services Designer
            InitializeComponent();

            //Create Connection String for access to the database
            GetDbConnectionString();
        }
        #endregion

        #region Component Designer generated code

        //Required by the Web Services Designer 
        private IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion

        #region List of Stored Procedures required for this class
        //p_GetCustomerDetails
        //p_GetCustomerAging
        //p_GetCustomerAgingAsOfDate
        //p_GetCustomerKardex - NOTE: requires view vw_ar_cust_kardex (most recent 100 records)
        //p_GetCustomerDocumentStatus - NOTE: requires view vw_ar_cust_document_status  (most recent 100 records)
        //p_GetCustomerAccountDetails - NOTE: requires view vw_ar_cust_account_details
        //p_AR_Cust_Insert - NOTE: requires stored procedure p_AR_CUST_Select_CUST_ID and function fn_next_cust_no
        //p_AR_ShipTo_Insert - NOTE: requires new table AR_SHIP2_TEMPLATES
        //p_AR_CUST_Select_CUST_ID
        //p_AR_ShipTo_Update
        //p_AR_Cust_Standard_Insert
        //p_AR_BillTo_Insert
        //p_AR_BillTo_Update
        //p_GetCustomerItemPriceData
        //p_GetCustomerContract
        //p_GetCustomerContracts
        //p_AR_ShipTo_Dept_Insert
        //p_AR_ShipTo_Dept_Update
        //p_GetZipCodeDtls
        //p_GetCustomerContact
        //p_CO_CONTACTS_Insert
        //p_CO_CONTACTS_Update
        //p_CO_CONTACTS_PHONE_Update
        //p_GetCustID
        #endregion

        #region GetDbConnectionString
        private void GetDbConnectionString()
        {
            try
            {
                //Instantiate a new instance of the Decryption class
                //the encrypted string retrieved from the AppSettings Web.config file will be decrypted by this class
                myDecryptor = new CWSBLL.Decryption();

                //Instantiate a new instance of the appliation's property settings
                CenGen_Web_Services.Properties.Settings mySettings = new Properties.Settings();
                
                //Set the connString variable equal to the ConnectionString setting
                connString = mySettings.ConnectionString;

                //The method DecryptString within the myDecryptor helper class is utilized to decrypt the encrypted string
                //stored in the connString string variable.
                connString = myDecryptor.DecryptString(connString);
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting database connection string from web.config file (Class | Method; wsCustomer | GetDbConnectionString). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion

        #region GetCustomerDetailsByCustNo
        [WebMethod]
        public DataSet GetCustomerDetailsByCustNo(string DB_NAME, string CUST_NO)
        {
            DataSet dsCustDtls;
            string CUST_ID = "";
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;


            try
            {
                conn = new SqlConnection(connString);
                conn.Open();

                cmd = new SqlCommand("p_GetCustID", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_NO", SqlDbType.VarChar);
                cmd.Parameters["@CUST_NO"].Value = CUST_NO;

                //Defines the return parameter sent back from the sql statement
                cmd.Parameters.Add("@CUST_ID", SqlDbType.UniqueIdentifier);
                cmd.Parameters["@CUST_ID"].Direction = System.Data.ParameterDirection.Output;

                //executes the stored procedure and returns a value indicating the number of effected records which should be 1 when successful and 0 when not
                int retVal = cmd.ExecuteNonQuery();

                //stores the cust_id returned from the stored procedure, will be used to identify the customer when the ship to record is created
                CUST_ID = cmd.Parameters["@CUST_ID"].Value.ToString();

                conn.Close();
                conn.Dispose();
                cmd.Dispose();

                dsCustDtls = getCustomerDetails(DB_NAME, CUST_ID);
                return dsCustDtls;

            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | GetCustomerDetails). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | GetCustomerDetails). " + ex.StackTrace);
            }
            finally
            {

            }
        }
        #endregion

        #region GetCustomerDetails
        [WebMethod]
        public DataSet GetCustomerDetails(string DB_NAME, string CUST_ID)
        {
            DataSet dsCustDtls;


            try
            {


                dsCustDtls = getCustomerDetails(DB_NAME, CUST_ID);
                return dsCustDtls;

            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | GetCustomerDetails). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | GetCustomerDetails). " + ex.StackTrace);
            }
            finally
            {

            }
        }
        #endregion

        #region private DataSet getCustomerDetails(string DB_NAME, string CUST_ID)
        private DataSet getCustomerDetails(string DB_NAME, string CUST_ID)
        {
            DataSet dsCustDtls = new DataSet("CustomerDetails");
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataAdapter da;

            try
            {


                conn = new SqlConnection(connString);

                cmd = new SqlCommand("p_GetCustomerDetails", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;

                da = new SqlDataAdapter(cmd);

                da.Fill(dsCustDtls);

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                return dsCustDtls;

            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | GetCustomerDetails). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | GetCustomerDetails). " + ex.StackTrace);
            }
            finally
            {

            }
        }
        #endregion

        #region GetCustomerContact
        [WebMethod]
        public DataSet GetCustomerContact(string DB_NAME, string CUST_ID, string FIRST_NAME, string LAST_NAME)
        {
            DataSet dsCustContact = new DataSet("CustContact");
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataAdapter da;

            string FirstName = null;
            string LastName = null;

            if (FIRST_NAME != "")
                FirstName = FIRST_NAME;

            if (LAST_NAME != "")
                LastName = LAST_NAME;


            try
            {
                conn = new SqlConnection(connString);
                conn.Open();

                cmd = new SqlCommand("p_GetCustomerContact", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;
                cmd.Parameters.Add("@FIRST_NAME", SqlDbType.VarChar);
                cmd.Parameters["@FIRST_NAME"].Value = FirstName;
                cmd.Parameters.Add("@LAST_NAME", SqlDbType.VarChar);
                cmd.Parameters["@LAST_NAME"].Value = LastName;

                da = new SqlDataAdapter(cmd);

                da.Fill(dsCustContact);

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                if (dsCustContact.Tables[0] != null)
                    dsCustContact.Tables[0].TableName = "Contact";

                if (dsCustContact.Tables[1] != null)
                    dsCustContact.Tables[1].TableName = "Phone";

                return dsCustContact;
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer contact information (class | Method; wsCustomer | GetCustomerContact). " + sqlex.Message + "; Base Exception: " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer contact information (Class | Method; wsCustomer | GetCustomerContact). " + ex.Message);
            }
            finally
            { }
        }
        #endregion

        #region GetCustomerShipToLocations
        [WebMethod]
        public DataSet GetCustomerShipToLocations(string DB_NAME, string CUST_ID)
        {
            DataSet dsShipTos = new DataSet("ShipToLocations");
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataAdapter da;

            try
            {


                conn = new SqlConnection(connString);

                cmd = new SqlCommand("p_GetCustomerShipTos", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;

                da = new SqlDataAdapter(cmd);

                da.Fill(dsShipTos);

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                return dsShipTos;

            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer shipto records (class | Method; wsCustomer | GetCustomerShipToLocations). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer shipto records (Class | Method; wsCustomer | GetCustomerShipToLocations). " + ex.StackTrace);
            }
            finally
            {

            }
        }
        #endregion

        #region GetCustomerAgingAsOfDate
        [WebMethod]
        public DataSet GetCustomerAgingAsOfDate(string DB_NAME, string CUST_ID, string AS_OF_DATE)
        {
            DataSet dsCustAging = new DataSet("CustomerAging");
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataAdapter da;

            try
            {


                conn = new SqlConnection(connString);

                cmd = new SqlCommand("p_GetCustomerAgingAsOfDate", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;
                cmd.Parameters.Add("@AS_OF_DATE", SqlDbType.DateTime);
                cmd.Parameters["@AS_OF_DATE"].Value = AS_OF_DATE;

                da = new SqlDataAdapter(cmd);

                da.Fill(dsCustAging);

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                return dsCustAging;

            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | GetCustomerAgingAsOfDate). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | GetCustomerAgingAsOfDate). " + ex.StackTrace);
            }
            finally
            {

            }
        }
        #endregion

        #region GetCustomerKardex
        [WebMethod]
        public DataSet GetCustomerKardex(string DB_NAME, string CUST_ID)
        {
            DataSet dsCustKardex = new DataSet("CustomerKardex");
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataAdapter da;

            try
            {


                conn = new SqlConnection(connString);

                cmd = new SqlCommand("p_GetCustomerKardex", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;

                da = new SqlDataAdapter(cmd);

                da.Fill(dsCustKardex);

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                return dsCustKardex;

            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | GetCustomerKardex). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | GetCustomerKardex). " + ex.StackTrace);
            }
            finally
            {

            }
        }
        #endregion

        #region GetCustomerDocumentStatus
        [WebMethod]
        public DataSet GetCustomerDocumentStatus(string DB_NAME, string CUST_ID)
        {
            DataSet dsCustDocStat = new DataSet("CustomerKardex");
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataAdapter da;

            try
            {


                conn = new SqlConnection(connString);

                cmd = new SqlCommand("p_GetCustomerDocumentStatus", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;

                da = new SqlDataAdapter(cmd);

                da.Fill(dsCustDocStat);

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                return dsCustDocStat;

            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | GetCustomerDocumentStatus). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | GetCustomerDocumentStatus). " + ex.StackTrace);
            }
            finally
            {

            }
        }
        #endregion

        #region GetCustomerAccountDetails
        [WebMethod]
        public DataSet GetCustomerAccountDetails(string DB_NAME, string CUST_ID, string SHIP2_ID)
        {
            DataSet dsCustAcctDtls = new DataSet("CustomerAccountDetails");
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataAdapter da;

            try
            {


                conn = new SqlConnection(connString);

                cmd = new SqlCommand("p_GetCustomerAccountDetails", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;
                cmd.Parameters.Add("@SHIP2_ID", SqlDbType.VarChar);
                cmd.Parameters["@SHIP2_ID"].Value = SHIP2_ID;

                da = new SqlDataAdapter(cmd);

                da.Fill(dsCustAcctDtls);

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                return dsCustAcctDtls;

            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | GetCustomerAccountDetails). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | GetCustomerAccountDetails). " + ex.StackTrace);
            }
            finally
            {

            }
        }
        #endregion

        #region GetCustomerContract
        [WebMethod]
        public DataSet GetCustomerContract(string DB_NAME, string CUST_ID, string SHIP2_ID, string CONTRACT)
        {
            DataSet dsCustomerContract = new DataSet("Contract");
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataAdapter da;

            try
            {
                conn = new SqlConnection(connString);
                conn.Open();

                cmd = new SqlCommand("p_GetCustomerContract", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;
                cmd.Parameters.Add("@SHIP2_ID", SqlDbType.VarChar);
                cmd.Parameters["@SHIP2_ID"].Value = SHIP2_ID;
                cmd.Parameters.Add("@CONTRACT", SqlDbType.VarChar);
                cmd.Parameters["@CONTRACT"].Value = CONTRACT;

                da = new SqlDataAdapter(cmd);

                da.Fill(dsCustomerContract);

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                return dsCustomerContract;
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer contract (class | Method; wsCustomer | GetCustomerContract). " + sqlex.Message + "; Base Exception: " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer contract (Class | Method; wsCustomer | GetCustomerContract). " + ex.Message);
            }
            finally
            { }
        }
        #endregion

        #region GetCustomerContracts
        [WebMethod]
        public DataSet GetCustomerContracts(string DB_NAME, string CUST_ID, string SHIP2_ID, int ShowAll)
        {
            DataSet dsCustomerContracts = new DataSet("Contracts");
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataAdapter da;

            try
            {
                conn = new SqlConnection(connString);
                conn.Open();

                cmd = new SqlCommand("p_GetCustomerContracts", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;
                cmd.Parameters.Add("@SHIP2_ID", SqlDbType.VarChar);
                cmd.Parameters["@SHIP2_ID"].Value = SHIP2_ID;
                cmd.Parameters.Add("@ShowAll", SqlDbType.Int);
                cmd.Parameters["@ShowAll"].Value = ShowAll;


                da = new SqlDataAdapter(cmd);

                da.Fill(dsCustomerContracts);

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                return dsCustomerContracts;
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer contracts (class | Method; wsCustomer | GetCustomerContracts). " + sqlex.Message + "; Base Exception: " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer contracts (Class | Method; wsCustomer | GetCustomerContracts). " + ex.Message);
            }
            finally
            { }
        }
        #endregion

        #region GetCustomerItemPrice
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public CustomerPriceResults GetCustomerItemPrice(Guid SHIP2_ID, Guid ProductGuid, Decimal Qty, string DB_NAME)
        {

            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            CustomerPriceResults cpr = new CustomerPriceResults();

            //Create a timer to capture the start time and end time of the process so as to measure the length of time
            //it takes to run the whole process
            //			DateTime startDateTime = DateTime.Now;
            //			DateTime endDateTime; 


            try
            {
                conn = new SqlConnection(connString);

                cmd = new SqlCommand("p_cds_sf_oe_pricing_engine", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@ship2_id", SqlDbType.UniqueIdentifier);
                cmd.Parameters.Add("@item_id", SqlDbType.UniqueIdentifier, 32);
                cmd.Parameters.Add("@qty", SqlDbType.Decimal);
                cmd.Parameters.Add("@period", SqlDbType.SmallInt);
                //cmd.Parameters.Add("@promo_item_id", SqlDbType.UniqueIdentifier, 32);
                cmd.Parameters.Add("@is_fractional", SqlDbType.Char, 1);
                cmd.Parameters.Add("@customer_price", SqlDbType.Decimal);
                cmd.Parameters.Add("@stockaid_price", SqlDbType.Decimal);
                cmd.Parameters.Add("@contract_price", SqlDbType.Decimal);
                cmd.Parameters.Add("@quantity_price", SqlDbType.Decimal);
                cmd.Parameters.Add("@product_group_price", SqlDbType.Decimal);
                cmd.Parameters.Add("@price_column_price", SqlDbType.Decimal);
                cmd.Parameters.Add("@promo_price", SqlDbType.Decimal);
                cmd.Parameters.Add("@list_price", SqlDbType.Decimal);
                cmd.Parameters.Add("@reference_price", SqlDbType.Decimal);
                cmd.Parameters.Add("@price_source", SqlDbType.Char, 2);
                cmd.Parameters.Add("@qty_available", SqlDbType.Decimal);
                cmd.Parameters.Add("@qty_message", SqlDbType.VarChar, 1000);
                cmd.Parameters.Add("@proc_no", SqlDbType.Int);
                cmd.Parameters.Add("@error_code", SqlDbType.Int);

                cmd.Parameters["@ship2_id"].Value = SHIP2_ID;
                cmd.Parameters["@item_id"].Value = ProductGuid;
                cmd.Parameters["@qty"].Value = Qty;
                cmd.Parameters["@period"].Value = DateTime.Today.Month;
                //cmd.Parameters["@promo_item_id"].Value = "";
                cmd.Parameters["@is_fractional"].Value = 'N';

                cmd.Parameters["@customer_price"].Direction = ParameterDirection.Output;
                cmd.Parameters["@stockaid_price"].Direction = ParameterDirection.Output;
                cmd.Parameters["@contract_price"].Direction = ParameterDirection.Output;
                cmd.Parameters["@quantity_price"].Direction = ParameterDirection.Output;
                cmd.Parameters["@product_group_price"].Direction = ParameterDirection.Output;
                cmd.Parameters["@price_column_price"].Direction = ParameterDirection.Output;
                cmd.Parameters["@promo_price"].Direction = ParameterDirection.Output;
                cmd.Parameters["@list_price"].Direction = ParameterDirection.Output;
                cmd.Parameters["@reference_price"].Direction = ParameterDirection.Output;
                cmd.Parameters["@price_source"].Direction = ParameterDirection.Output;
                cmd.Parameters["@qty_available"].Direction = ParameterDirection.Output;
                cmd.Parameters["@qty_message"].Direction = ParameterDirection.Output;
                cmd.Parameters["@proc_no"].Direction = ParameterDirection.Output;
                cmd.Parameters["@error_code"].Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                cpr.CustPrice = float.Parse(cmd.Parameters["@customer_price"].Value.ToString());
                cpr.ListPrice = float.Parse(cmd.Parameters["@list_price"].Value.ToString());
                cpr.ReferencePrice = float.Parse(cmd.Parameters["@reference_price"].Value.ToString());
                cpr.Message = cmd.Parameters["@qty_message"].Value.ToString();
                cpr.PriceSource = cmd.Parameters["@price_source"].Value.ToString();
                cpr.QtyAvailable = int.Parse(cmd.Parameters["@qty_available"].Value.ToString());
                cpr.ErrorCode = int.Parse(cmd.Parameters["@error_code"].Value.ToString());

                conn.Close();
                conn.Dispose();
                cmd.Dispose();

                return cpr;

            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | GetCustomerDetails). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | GetCustomerDetails). " + ex.StackTrace);
            }
            finally
            {


            }
        }
        #endregion

        #region GetZipCodeDtlID
        [WebMethod]
        public DataSet GetZipCodeDtls(string DB_NAME, string ZIP)
        {
            DataSet dsZipCodeDtls = new DataSet("ZipCodeDtls");
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataAdapter da;

            try
            {
                conn = new SqlConnection(connString);
                conn.Open();

                cmd = new SqlCommand("p_GetZipCodeDtls", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ZIP", SqlDbType.VarChar);
                cmd.Parameters["@ZIP"].Value = ZIP;

                da = new SqlDataAdapter(cmd);

                da.Fill(dsZipCodeDtls);

                conn.Close();
                conn.Dispose();
                cmd.Dispose();
                da.Dispose();

                return dsZipCodeDtls;
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving zip code details (class | Method; wsCustomer | GetZipCodeDtls). " + sqlex.Message + "; Base Exception: " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting zip code details (Class | Method; wsCustomer | GetZipCodeDtls). " + ex.Message);
            }
            finally
            { }
        }
        #endregion

        #region InsertNewCustomer
        [WebMethod]
        public DataSet InsertNewCustomer(string DB_NAME, string TEMPLATE_CUST_NO, string CUST_NAME, string XREF_NAME,
                                         string SHIP_NO, string SHIPTO_NAME, string ADDRESS1, string ADDRESS2, string ZIP_CODE_DTL_ID, string ZIP4, string TEL_NO,
                                         string FAX_NO, string WEB_ADDRESS, string TAX_EXEMPT_NO, string COMMENTS, string EMAIL_ADDRESS, string FED_EX_ACCT, string UPS_ACCT,
                                         string DEFAULT_CARRIER, string DEFAULT_CARRIER_SERVICE_CODE, string BILL_TO_NAME, string BILL_TO_ADDRESS1, string BILL_TO_ADDRESS2,
                                         string BILL_TO_ZIP_CODE_DTL_ID, string BILL_TO_ZIP4, string BILL_TO_TEL_NO, string BILL_TO_FAX_NO, string BILL_TO_WEB_ADDRESS,
                                         string BILL_TO_EMAIL_ADDRESS, string DEPT, string DESCRIPTION, string ATTN, string DEPT_TEL_NO, string DEPT_FAX_NO, string IS_MAILED, string DEPT_EMAIL_ADDRESS)
        {
            string TemplateCustomerID = string.Empty;
            string CustomerNumber = string.Empty;
            string CustomerID = string.Empty;
            string ShipToID = string.Empty;
            string ShipToNumber = string.Empty;
            string ShipToDeptID = string.Empty;
            string resultVal = string.Empty;
            string CustomerAccount = string.Empty;
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;
            DataSet dsNewCustomer = new DataSet("NewCustomer");
            DataTable dtNewCustomer = new DataTable("NewCustomer");
            dsNewCustomer.Tables.Add(dtNewCustomer);
            DataRow drNewCustomer;

            dtNewCustomer.Columns.Add("CUST_ID", System.Type.GetType("System.String"));
            dtNewCustomer.Columns.Add("CUST_NO", System.Type.GetType("System.String"));
            dtNewCustomer.Columns.Add("SHIP2_ID", System.Type.GetType("System.String"));
            dtNewCustomer.Columns.Add("SHIP_NO", System.Type.GetType("System.String"));
            dtNewCustomer.Columns.Add("DEPT_ID", System.Type.GetType("System.String"));
            dtNewCustomer.Columns.Add("ErrorMessage", System.Type.GetType("System.String"));

            drNewCustomer = dtNewCustomer.NewRow();

            try
            {
                TemplateCustomerID = GetCustID(connString, TEMPLATE_CUST_NO);

                //Place check for TemplateCustomerID being null here
                if (TemplateCustomerID == null)
                {
                    drNewCustomer["ErrorMessage"] = "Error - Template customer number does not exist.";
                    return dsNewCustomer;
                }
                conn = new SqlConnection(connString);
                conn.Open();

                cmd = new SqlCommand("p_AR_Cust_Insert", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TEMPLATE_CUST_NO", SqlDbType.VarChar);
                cmd.Parameters["@TEMPLATE_CUST_NO"].Value = TEMPLATE_CUST_NO;
                cmd.Parameters.Add("@CUST_NAME", SqlDbType.VarChar);
                cmd.Parameters["@CUST_NAME"].Value = CUST_NAME;
                cmd.Parameters.Add("@XREF_NAME", SqlDbType.VarChar);
                cmd.Parameters["@XREF_NAME"].Value = XREF_NAME;

                //Defines the return parameter sent back from the sql statement
                cmd.Parameters.Add("@CUST_NO", SqlDbType.VarChar, 8);
                cmd.Parameters["@CUST_NO"].Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.UniqueIdentifier);
                cmd.Parameters["@CUST_ID"].Direction = System.Data.ParameterDirection.Output;

                //executes the stored procedure and returns a value indicating the number of effected records which should be 1 when successful and 0 when not
                int retVal = cmd.ExecuteNonQuery();

                //stores the cust_id returned from the stored procedure, will be used to identify the customer when the ship to record is created
                CustomerNumber = cmd.Parameters["@CUST_NO"].Value.ToString();
                CustomerID = cmd.Parameters["@CUST_ID"].Value.ToString();

                conn.Close();
                conn.Dispose();
                cmd.Dispose();

                if (retVal == 1)
                {
                    ShipToID = ARShipTo_Insert(TemplateCustomerID, CustomerID, SHIP_NO, SHIPTO_NAME, ADDRESS1, ADDRESS2, ZIP_CODE_DTL_ID, ZIP4, TEL_NO, FAX_NO, WEB_ADDRESS, TAX_EXEMPT_NO,
                        COMMENTS, EMAIL_ADDRESS, FED_EX_ACCT, UPS_ACCT, DEFAULT_CARRIER, DEFAULT_CARRIER_SERVICE_CODE);
                }
                else
                {
                    drNewCustomer["ErrorMessage"] = "Error creating customer record";
                    return dsNewCustomer;

                }

                if (ShipToID.StartsWith("Error"))
                {
                    drNewCustomer["ErrorMessage"] = "Error Creating Ship To record";
                    return dsNewCustomer;
                }
                else
                    resultVal = ARCustStandard_Insert(TemplateCustomerID, CustomerID, ShipToID);
                if (resultVal == "SUCCESS")
                {
                    resultVal = ARBillTo_Insert(CustomerID, BILL_TO_NAME, BILL_TO_ADDRESS1, BILL_TO_ADDRESS2, BILL_TO_ZIP_CODE_DTL_ID, BILL_TO_ZIP4,
                                                BILL_TO_TEL_NO, BILL_TO_FAX_NO, BILL_TO_WEB_ADDRESS, BILL_TO_EMAIL_ADDRESS);
                }
                else
                {
                    drNewCustomer["ErrorMessage"] = "Error creating Bill-To, please contact Technical Support!";
                    return dsNewCustomer;
                }

                if (String.Compare(resultVal, "SUCCESS") == 0 && DEPT != null && String.Compare(DEPT, "") != 0)
                {
                    ShipToDeptID = ARShipToDept_Insert(DB_NAME, ShipToID, DEPT, DESCRIPTION, ATTN, DEPT_TEL_NO, DEPT_FAX_NO, IS_MAILED, DEPT_EMAIL_ADDRESS);
                }

                if (resultVal == "SUCCESS")
                {
                    drNewCustomer["CUST_ID"] = CustomerID;
                    drNewCustomer["CUST_NO"] = CustomerNumber;
                    drNewCustomer["SHIP2_ID"] = ShipToID;
                    drNewCustomer["SHIP_NO"] = SHIP_NO;
                    drNewCustomer["DEPT_ID"] = ShipToDeptID;

                    dtNewCustomer.Rows.Add(drNewCustomer);

                    return dsNewCustomer;
                }
                else
                {
                    drNewCustomer["ErrorMessage"] = "Unknown Error, please contact Technical Support!";
                    return dsNewCustomer;
                }
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error retrieving customer details (class | Method; wsCustomer | InsertNewCustomer). " + sqlex.GetBaseException());
                //return CustomerID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting customer details (Class | Method; wsCustomer | InsertNewCustomer). " + ex.Message);
                //return CustomerID;
            }
            finally
            {

            }
        }
        #endregion

        #region InsertNewShipTo
        [WebMethod]
        public DataSet InsertNewShipTo(string DB_NAME, string TEMPLATE_CUST_NO, string CUST_ID, string SHIP_NO, string SHIPTO_NAME, string ADDRESS1, string ADDRESS2, string ZIP_CODE_DTL_ID,
                                      string ZIP4, string TEL_NO, string FAX_NO, string WEB_ADDRESS, string TAX_EXEMPT_NO, string COMMENTS,
                                      string EMAIL_ADDRESS, string FED_EX_ACCT, string UPS_ACCT, string DEFAULT_CARRIER, string DEFAULT_CARRIER_SERVICE_CODE,
                                      string DEPT, string DESCRIPTION, string ATTN, string DEPT_TEL_NO, string DEPT_FAX_NO, string IS_MAILED, string DEPT_EMAIL_ADDRESS)
        {
            try
            {
                connString += "Database=" + DB_NAME + ";";
                string TemplateCustomerID = GetCustID(connString, TEMPLATE_CUST_NO);
                string ShipToID = string.Empty;
                string ShipToDeptID = string.Empty;

                DataSet dsNewShipTo = new DataSet("NewShipTo");
                DataTable dtNewShipTo = new DataTable("NewShipTo");
                dsNewShipTo.Tables.Add(dtNewShipTo);
                DataRow drNewShipTo;

                dtNewShipTo.Columns.Add("SHIP2_ID", System.Type.GetType("System.String"));
                dtNewShipTo.Columns.Add("DEPT_ID", System.Type.GetType("System.String"));
                dtNewShipTo.Columns.Add("ErrorMessage", System.Type.GetType("System.String"));

                drNewShipTo = dtNewShipTo.NewRow();

                ShipToID = ARShipTo_Insert(TemplateCustomerID, CUST_ID, SHIP_NO, SHIPTO_NAME, ADDRESS1, ADDRESS2, ZIP_CODE_DTL_ID, ZIP4, TEL_NO, FAX_NO, WEB_ADDRESS, TAX_EXEMPT_NO,
                    COMMENTS, EMAIL_ADDRESS, FED_EX_ACCT, UPS_ACCT, DEFAULT_CARRIER, DEFAULT_CARRIER_SERVICE_CODE);

                if (ShipToID != null && String.Compare(ShipToID, "") != 0 && DEPT != null && String.Compare(DEPT, "") != 0)
                {
                    ShipToDeptID = ARShipToDept_Insert(DB_NAME, ShipToID, DEPT, DESCRIPTION, ATTN, DEPT_TEL_NO, DEPT_FAX_NO, IS_MAILED, DEPT_EMAIL_ADDRESS);
                }

                if (ShipToID != null && String.Compare(ShipToID, "") != 0)
                {
                    drNewShipTo["SHIP2_ID"] = ShipToID;
                    drNewShipTo["DEPT_ID"] = ShipToDeptID;

                    dtNewShipTo.Rows.Add(drNewShipTo);

                    return dsNewShipTo;
                }
                else
                {
                    drNewShipTo["ErrorMessage"] = "Unknown Error, please contact Technical Support!";
                    return dsNewShipTo;
                }
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error insert new ship-to record (class | Method; wsCustomer | InsertNewShipTo). " + sqlex.GetBaseException());
                //return CustomerID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error insert new ship-to record (class | Method; wsCustomer | InsertNewShipTo). " + ex.Message);
                //return CustomerID;
            }
            finally
            {

            }
        }
        #endregion

        #region InsertNewShipToDept
        [WebMethod]
        public string InsertNewShipToDept(string DB_NAME, string SHIPTO_ID, string DEPT, string DESCRIPTION, string ATTN, string TEL_NO, string FAX_NO, string IS_MAILED, string EMAIL_ADDRESS)
        {
            try
            {
                string ShipToDeptID = string.Empty;

                ShipToDeptID = ARShipToDept_Insert(DB_NAME, SHIPTO_ID, DEPT, DESCRIPTION, ATTN, TEL_NO, FAX_NO, IS_MAILED, EMAIL_ADDRESS);

                return ShipToDeptID;
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error inserting new shipto department record (class | Method; wsCustomer | InsertNewShipToDept). " + sqlex.GetBaseException());
                //return CustomerID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error inserting new shipto department record (class | Method; wsCustomer | InsertNewShipToDept). " + ex.Message);
                //return CustomerID;
            }
            finally
            {

            }
        }
        #endregion

        #region InsertNewCustomerContact
        [WebMethod]
        public string InsertNewCustomerContact(string DB_NAME, string CUST_ID, string SHIPTO_ID, string FIRST_NAME, string LAST_NAME, string MIDDLE_NAME, string TITLE, string TEL_NO, string EXTENSION_NO, string FAX_NO, string EMAIL_ADDRESS)
        {
            try
            {
                string ContactID = "";

                string ShipToID = null;
                string MiddleName = null;
                string Title = null;
                string TelNo = null;
                string Extension = null;
                string FaxNo = null;
                string EmailAddress = null;

                if (SHIPTO_ID != "")
                    ShipToID = SHIPTO_ID;

                if (MIDDLE_NAME != "")
                    MiddleName = MIDDLE_NAME;

                if (TITLE != "")
                    Title = TITLE;

                if (TEL_NO != "")
                    TelNo = TEL_NO;

                if (EXTENSION_NO != "")
                    Extension = EXTENSION_NO;

                if (FAX_NO != "")
                    FaxNo = FAX_NO;

                if (EMAIL_ADDRESS != "")
                    EmailAddress = EMAIL_ADDRESS;


                ContactID = COContact_Insert(DB_NAME, CUST_ID, ShipToID, FIRST_NAME, LAST_NAME, MiddleName, Title, TelNo, Extension, FaxNo, EmailAddress);

                return ContactID;
            }
            catch (SqlException sqlex)
            {
                throw new Exception("SQL error inserting new shipto department record (class | Method; wsCustomer | InsertNewShipToDept). " + sqlex.GetBaseException());
                //return CustomerID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error inserting new shipto department record (class | Method; wsCustomer | InsertNewShipToDept). " + ex.Message);
                //return CustomerID;
            }
            finally
            {

            }
        }
        #endregion

        #region GetCustID
        private string GetCustID(string connString, string CUST_NO)
        {
            string TemplateCustomerID = string.Empty;
            SqlConnection conn;
            SqlCommand cmd;

            try
            {
                conn = new SqlConnection(connString);
                conn.Open();

                cmd = new SqlCommand("p_AR_CUST_Select_CUST_ID", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_NO", SqlDbType.VarChar);
                cmd.Parameters["@CUST_NO"].Value = CUST_NO;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.UniqueIdentifier);
                cmd.Parameters["@CUST_ID"].Direction = System.Data.ParameterDirection.Output;

                int i = cmd.ExecuteNonQuery();

                //stores the cust_id returned from the stored procedure, will be used to identify the template customer record
                TemplateCustomerID = cmd.Parameters["@CUST_ID"].Value.ToString();

                conn.Close();
                conn.Dispose();
                cmd.Dispose();

                return TemplateCustomerID;
            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error getting template customer ID (class | Method; wsCustomer | GetCustID). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - error getting template customer ID (Class | Method; wsCustomer | GetCustID). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion

        #region ARShip2_Insert()
        private string ARShipTo_Insert(string TEMPLATE_CUST_ID, string CUST_ID, string SHIP_NO, string SHIPTO_NAME, string ADDRESS1, string ADDRESS2, string ZIP_CODE_DTL_ID, string ZIP4, string TEL_NO,
                                    string FAX_NO, string WEB_ADDRESS, string TAX_EXEMPT_NO, string COMMENTS, string EMAIL_ADDRESS, string FED_EX_ACCT, string UPS_ACCT,
                                    string DEFAULT_CARRIER, string DEFAULT_CARRIER_SERVICE_CODE)
        {
            SqlConnection conn;
            SqlCommand cmd;
            string ShipToID = string.Empty;

            try
            {


                conn = new SqlConnection(connString);
                conn.Open();

                cmd = new SqlCommand("p_AR_ShipTo_Insert", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add("@TEMPLATE_CUST_ID", SqlDbType.UniqueIdentifier);
                cmd.Parameters.Add("@TEMPLATE_CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@TEMPLATE_CUST_ID"].Value = TEMPLATE_CUST_ID;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;
                cmd.Parameters.Add("@SHIP_NO", SqlDbType.VarChar);
                cmd.Parameters["@SHIP_NO"].Value = SHIP_NO;
                cmd.Parameters.Add("@SHIPTO_NAME", SqlDbType.VarChar);
                cmd.Parameters["@SHIPTO_NAME"].Value = SHIPTO_NAME;
                cmd.Parameters.Add("@ADDRESS1", SqlDbType.VarChar);
                cmd.Parameters["@ADDRESS1"].Value = ADDRESS1;
                cmd.Parameters.Add("@ADDRESS2", SqlDbType.VarChar);
                cmd.Parameters["@ADDRESS2"].Value = ADDRESS2;
                cmd.Parameters.Add("@ZIP_CODE_DTL_ID", SqlDbType.VarChar);
                cmd.Parameters["@ZIP_CODE_DTL_ID"].Value = ZIP_CODE_DTL_ID;

                cmd.Parameters.Add("@ZIP4", SqlDbType.VarChar);
                cmd.Parameters["@ZIP4"].Value = ZIP4;

                cmd.Parameters.Add("@TEL_NO", SqlDbType.VarChar);
                cmd.Parameters["@TEL_NO"].Value = TEL_NO;
                cmd.Parameters.Add("@FAX_NO", SqlDbType.VarChar);
                cmd.Parameters["@FAX_NO"].Value = FAX_NO;
                cmd.Parameters.Add("@WEB_ADDRESS", SqlDbType.VarChar);
                cmd.Parameters["@WEB_ADDRESS"].Value = WEB_ADDRESS;
                cmd.Parameters.Add("@TAX_EXEMPT_NO", SqlDbType.VarChar);
                cmd.Parameters["@TAX_EXEMPT_NO"].Value = TAX_EXEMPT_NO;
                cmd.Parameters.Add("@COMMENTS", SqlDbType.VarChar);
                cmd.Parameters["@COMMENTS"].Value = COMMENTS;
                cmd.Parameters.Add("@EMAIL_ADDRESS", SqlDbType.VarChar);
                cmd.Parameters["@EMAIL_ADDRESS"].Value = EMAIL_ADDRESS;
                cmd.Parameters.Add("@FED_EX_ACCT", SqlDbType.VarChar);
                cmd.Parameters["@FED_EX_ACCT"].Value = FED_EX_ACCT;
                cmd.Parameters.Add("@UPS_ACCT", SqlDbType.VarChar);
                cmd.Parameters["@UPS_ACCT"].Value = UPS_ACCT;
                cmd.Parameters.Add("@DEFAULT_CARRIER", SqlDbType.VarChar);
                cmd.Parameters["@DEFAULT_CARRIER"].Value = DEFAULT_CARRIER;
                cmd.Parameters.Add("@DEFAULT_CARRIER_SERVICE_CODE", SqlDbType.VarChar);
                cmd.Parameters["@DEFAULT_CARRIER_SERVICE_CODE"].Value = DEFAULT_CARRIER_SERVICE_CODE;

                //defines the return parameter sent back from the sql statement
                cmd.Parameters.Add("@SHIP2_ID", SqlDbType.UniqueIdentifier);
                cmd.Parameters["@SHIP2_ID"].Direction = System.Data.ParameterDirection.Output;

                int i = cmd.ExecuteNonQuery();


                //stores the cust_id returned from the stored procedure, will be used to identify the customer when the ship to record is created
                ShipToID = cmd.Parameters["@SHIP2_ID"].Value.ToString();

                conn.Close();
                conn.Dispose();
                cmd.Dispose();

                return ShipToID;
            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error retrieving customer details (class | Method; wsCustomer | ARShipTo_Insert). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - getting customer details (Class | Method; wsCustomer | ARShipTo_Insert). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion

        #region ARCustStandard_Insert()
        private string ARCustStandard_Insert(string TEMPLATE_CUST_ID, string CUST_ID, string SHIP2_ID)
        {
            SqlConnection conn;
            SqlCommand cmd;

            try
            {


                conn = new SqlConnection(connString);
                conn.Open();

                cmd = new SqlCommand("p_AR_CUST_STANDARD_Insert", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TEMPLATE_CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@TEMPLATE_CUST_ID"].Value = TEMPLATE_CUST_ID;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;
                cmd.Parameters.Add("@SHIP2_ID", SqlDbType.VarChar);
                cmd.Parameters["@SHIP2_ID"].Value = SHIP2_ID;

                int i = cmd.ExecuteNonQuery();

                conn.Close();
                conn.Dispose();
                cmd.Dispose();

                return "SUCCESS";
            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error creating customer standard record (class | Method; wsCustomer | ARCustStandard_Insert). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - creating customer standard record (Class | Method; wsCustomer | ARCustStandard_Insert). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion

        #region ARBillTo_Insert
        private string ARBillTo_Insert(string CustomerID, string BILL_TO_NAME, string BILL_TO_ADDRESS1, string BILL_TO_ADDRESS2,
                                      string BILL_TO_ZIP_CODE_DTL_ID, string BILL_TO_ZIP4, string BILL_TO_TEL_NO, string BILL_TO_FAX_NO,
                                      string BILL_TO_WEB_ADDRESS, string BILL_TO_EMAIL_ADDRESS)
        {
            SqlConnection conn;
            SqlCommand cmd;

            try
            {


                conn = new SqlConnection(connString);
                conn.Open();

                cmd = new SqlCommand("p_AR_BillTo_Insert", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CustomerID;
                cmd.Parameters.Add("@BILL_TO_NAME", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_NAME"].Value = BILL_TO_NAME;
                cmd.Parameters.Add("@BILL_TO_ADDRESS1", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_ADDRESS1"].Value = BILL_TO_ADDRESS1;
                cmd.Parameters.Add("@BILL_TO_ADDRESS2", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_ADDRESS2"].Value = BILL_TO_ADDRESS2;
                cmd.Parameters.Add("@BILL_TO_ZIP_CODE_DTL_ID", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_ZIP_CODE_DTL_ID"].Value = BILL_TO_ZIP_CODE_DTL_ID;
                cmd.Parameters.Add("@BILL_TO_ZIP4", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_ZIP4"].Value = BILL_TO_ZIP4;
                cmd.Parameters.Add("@BILL_TO_TEL_NO", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_TEL_NO"].Value = BILL_TO_TEL_NO;
                cmd.Parameters.Add("@BILL_TO_FAX_NO", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_FAX_NO"].Value = BILL_TO_FAX_NO;
                cmd.Parameters.Add("@BILL_TO_WEB_ADDRESS", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_WEB_ADDRESS"].Value = BILL_TO_WEB_ADDRESS;
                cmd.Parameters.Add("@BILL_TO_EMAIL_ADDRESS", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_EMAIL_ADDRESS"].Value = BILL_TO_EMAIL_ADDRESS;


                int i = cmd.ExecuteNonQuery();

                conn.Close();
                conn.Dispose();
                cmd.Dispose();

                return "SUCCESS";
            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error creating bill-to customer record (class | Method; wsCustomer | ARBillTo_Insert). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - creating bill-to customer record (Class | Method; wsCustomer | ARBillTo_Insert). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion

        #region ARShipToDept_Insert
        public string ARShipToDept_Insert(string DB_NAME, string SHIPTO_ID, string DEPT, string DESCRIPTION, string ATTN, string TEL_NO, string FAX_NO, string IS_MAILED, string EMAIL_ADDRESS)
        {
            string ShipToDeptID = string.Empty;

            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;

            try
            {
                //If IS_MAILED is not set to either Y or N, then set it to N because it is a required field
                if (String.Compare(IS_MAILED, "Y") != 0 && String.Compare(IS_MAILED, "N") != 0)
                {
                    IS_MAILED = "Y";
                }

                conn = new SqlConnection(connString);
                conn.Open();

                cmd = new SqlCommand("p_AR_ShipTo_Dept_Insert", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SHIP2_ID", SqlDbType.VarChar);
                cmd.Parameters["@SHIP2_ID"].Value = SHIPTO_ID;
                cmd.Parameters.Add("@DEPT", SqlDbType.VarChar);
                cmd.Parameters["@DEPT"].Value = DEPT;
                cmd.Parameters.Add("@DESCRIPTION", SqlDbType.VarChar);
                cmd.Parameters["@DESCRIPTION"].Value = DESCRIPTION;
                cmd.Parameters.Add("@ATTN", SqlDbType.VarChar);
                cmd.Parameters["@ATTN"].Value = ATTN;
                cmd.Parameters.Add("@TEL_NO", SqlDbType.VarChar);
                cmd.Parameters["@TEL_NO"].Value = TEL_NO;
                cmd.Parameters.Add("@FAX_NO", SqlDbType.VarChar);
                cmd.Parameters["@FAX_NO"].Value = FAX_NO;
                cmd.Parameters.Add("@IS_MAILED", SqlDbType.Char);
                cmd.Parameters["@IS_MAILED"].Value = IS_MAILED;
                cmd.Parameters.Add("@EMAIL_ADDRESS", SqlDbType.VarChar);
                cmd.Parameters["@EMAIL_ADDRESS"].Value = EMAIL_ADDRESS;

                //defines the return parameter sent back from the sql statement
                cmd.Parameters.Add("@DEPT_ID", SqlDbType.UniqueIdentifier);
                cmd.Parameters["@DEPT_ID"].Direction = System.Data.ParameterDirection.Output;

                int i = cmd.ExecuteNonQuery();


                //stores the dept_id returned from the stored procedure, will be sent back to the calling application so they have the uniqueidentifier for the record
                ShipToDeptID = cmd.Parameters["@DEPT_ID"].Value.ToString();

                conn.Close();
                conn.Dispose();
                cmd.Dispose();

                return ShipToDeptID;
            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error creating shipto department record (class | Method; wsCustomer | InsertNewShipToDept). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - creating shipto department record (Class | Method; wsCustomer | InsertNewShipToDept). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion

        #region UpdateShipToNameAndAddress
        [WebMethod]
        public string UpdateShipToNameAndAddress(string DB_NAME, string SHIP2_ID, string SHIPTO_NAME, string ADDRESS1, string ADDRESS2,
                                                 string ZIP_CODE_DTL_ID, string ZIP4, string TEL_NO, string FAX_NO, string WEB_ADDRESS,
                                                 string TAX_EXEMPT_NO, string COMMENTS, string EMAIL_ADDRESS, string FED_EX_ACCT, string UPS_ACCT,
                                                 string DEFAULT_CARRIER, string DEFAULT_SERVICE_CODE)
        {
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;

            conn = new SqlConnection(connString);
            conn.Open();


            try
            {
                cmd = new SqlCommand("p_AR_ShipTo_Update", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SHIP2_ID", SqlDbType.VarChar);
                cmd.Parameters["@SHIP2_ID"].Value = SHIP2_ID;
                cmd.Parameters.Add("@SHIPTO_NAME", SqlDbType.VarChar);
                cmd.Parameters["@SHIPTO_NAME"].Value = SHIPTO_NAME;
                cmd.Parameters.Add("@ADDRESS1", SqlDbType.VarChar);
                cmd.Parameters["@ADDRESS1"].Value = ADDRESS1;
                cmd.Parameters.Add("@ADDRESS2", SqlDbType.VarChar);
                cmd.Parameters["@ADDRESS2"].Value = ADDRESS2;
                cmd.Parameters.Add("@ZIP_CODE_DTL_ID", SqlDbType.VarChar);
                cmd.Parameters["@ZIP_CODE_DTL_ID"].Value = ZIP_CODE_DTL_ID;

                cmd.Parameters.Add("@ZIP4", SqlDbType.VarChar);
                cmd.Parameters["@ZIP4"].Value = ZIP4;

                cmd.Parameters.Add("@TEL_NO", SqlDbType.VarChar);
                cmd.Parameters["@TEL_NO"].Value = TEL_NO;
                cmd.Parameters.Add("@FAX_NO", SqlDbType.VarChar);
                cmd.Parameters["@FAX_NO"].Value = FAX_NO;
                cmd.Parameters.Add("@WEB_ADDRESS", SqlDbType.VarChar);
                cmd.Parameters["@WEB_ADDRESS"].Value = WEB_ADDRESS;
                cmd.Parameters.Add("@TAX_EXEMPT_NO", SqlDbType.VarChar);
                cmd.Parameters["@TAX_EXEMPT_NO"].Value = TAX_EXEMPT_NO;
                cmd.Parameters.Add("@COMMENTS", SqlDbType.VarChar);
                cmd.Parameters["@COMMENTS"].Value = COMMENTS;
                cmd.Parameters.Add("@EMAIL_ADDRESS", SqlDbType.VarChar);
                cmd.Parameters["@EMAIL_ADDRESS"].Value = EMAIL_ADDRESS;
                cmd.Parameters.Add("@FED_EX_ACCT", SqlDbType.VarChar);
                cmd.Parameters["@FED_EX_ACCT"].Value = FED_EX_ACCT;
                cmd.Parameters.Add("@UPS_ACCT", SqlDbType.VarChar);
                cmd.Parameters["@UPS_ACCT"].Value = UPS_ACCT;
                cmd.Parameters.Add("@DEFAULT_CARRIER", SqlDbType.VarChar);
                cmd.Parameters["@DEFAULT_CARRIER"].Value = DEFAULT_CARRIER;
                cmd.Parameters.Add("@DEFAULT_SERVICE_CODE", SqlDbType.VarChar);
                cmd.Parameters["@DEFAULT_SERVICE_CODE"].Value = DEFAULT_SERVICE_CODE;

                //executes the stored procedure and returns a value indicating the number of effected records which should be 1 when successful and 0 when not
                int retVal = cmd.ExecuteNonQuery();

                cmd.Dispose();

                if (retVal == 1 || retVal == 2)
                    return "SUCCESS";
                else
                    return "ERROR UPDATING SHIP-TO.";
            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error updating customer ship-to record (class | Method; wsCustomer | ARShipTo_Update). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - updating customer ship-to record (Class | Method; wsCustomer | ARShipTo_Update). " + ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

        }
        #endregion

        #region UpdateBillToNameAndAddress
        [WebMethod]
        public string UpdateBillToNameAndAddress(string DB_NAME, string CUST_ID, string BILL_TO_NAME, string BILL_TO_ADDRESS1, string BILL_TO_ADDRESS2,
            string BILL_TO_ZIP_CODE_DTL_ID, string BILL_TO_ZIP4, string BILL_TO_TEL_NO, string BILL_TO_FAX_NO, string BILL_TO_WEB_ADDRESS,
            string BILL_TO_EMAIL_ADDRESS)
        {
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;

            conn = new SqlConnection(connString);
            conn.Open();


            try
            {
                cmd = new SqlCommand("p_AR_BillTo_Update", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;
                cmd.Parameters.Add("@BILL_TO_NAME", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_NAME"].Value = BILL_TO_NAME;
                cmd.Parameters.Add("@BILL_TO_ADDRESS1", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_ADDRESS1"].Value = BILL_TO_ADDRESS1;
                cmd.Parameters.Add("@BILL_TO_ADDRESS2", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_ADDRESS2"].Value = BILL_TO_ADDRESS2;
                cmd.Parameters.Add("@BILL_TO_ZIP_CODE_DTL_ID", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_ZIP_CODE_DTL_ID"].Value = BILL_TO_ZIP_CODE_DTL_ID;

                cmd.Parameters.Add("@BILL_TO_ZIP4", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_ZIP4"].Value = BILL_TO_ZIP4;

                cmd.Parameters.Add("@BILL_TO_TEL_NO", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_TEL_NO"].Value = BILL_TO_TEL_NO;
                cmd.Parameters.Add("@BILL_TO_FAX_NO", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_FAX_NO"].Value = BILL_TO_FAX_NO;
                cmd.Parameters.Add("@BILL_TO_WEB_ADDRESS", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_WEB_ADDRESS"].Value = BILL_TO_WEB_ADDRESS;
                cmd.Parameters.Add("@BILL_TO_EMAIL_ADDRESS", SqlDbType.VarChar);
                cmd.Parameters["@BILL_TO_EMAIL_ADDRESS"].Value = BILL_TO_EMAIL_ADDRESS;

                //executes the stored procedure and returns a value indicating the number of effected records which should be 1 when successful and 0 when not
                int retVal = cmd.ExecuteNonQuery();

                cmd.Dispose();

                if (retVal == 1 || retVal == 2)
                    return "SUCCESS";
                else
                    return "ERROR UPDATING BILL-TO.";



            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error updating customer bill-to record (class | Method; wsCustomer | ARBillTo_Update). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - update customer bill-to record (Class | Method; wsCustomer | ARShipTo_Update). " + ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

        }
        #endregion

        #region UpdateARShipToDept
        [WebMethod]
        public string UpdateARShipToDept(string DB_NAME, string DEPT_ID, string DEPT, string DESCRIPTION, string ATTN, string TEL_NO,
            string FAX_NO, string IS_MAILED, string EMAIL_ADDRESS)
        {
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;

            conn = new SqlConnection(connString);
            conn.Open();


            try
            {
                cmd = new SqlCommand("p_AR_ShipToDept_Update", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@DEPT_ID", SqlDbType.VarChar);
                cmd.Parameters["@DEPT_ID"].Value = DEPT_ID;
                cmd.Parameters.Add("@DEPT", SqlDbType.VarChar);
                cmd.Parameters["@DEPT"].Value = DEPT;
                cmd.Parameters.Add("@DESCRIPTION", SqlDbType.VarChar);
                cmd.Parameters["@DESCRIPTION"].Value = DESCRIPTION;
                cmd.Parameters.Add("@ATTN", SqlDbType.VarChar);
                cmd.Parameters["@ATTN"].Value = ATTN;
                cmd.Parameters.Add("@TEL_NO", SqlDbType.VarChar);
                cmd.Parameters["@TEL_NO"].Value = TEL_NO;
                cmd.Parameters.Add("@FAX_NO", SqlDbType.VarChar);
                cmd.Parameters["@FAX_NO"].Value = FAX_NO;
                cmd.Parameters.Add("@IS_MAILED", SqlDbType.VarChar);
                cmd.Parameters["@IS_MAILED"].Value = IS_MAILED;
                cmd.Parameters.Add("@EMAIL_ADDRESS", SqlDbType.VarChar);
                cmd.Parameters["@EMAIL_ADDRESS"].Value = EMAIL_ADDRESS;

                //executes the stored procedure and returns a value indicating the number of effected records which should be 1 when successful and 0 when not
                int retVal = cmd.ExecuteNonQuery();

                cmd.Dispose();

                if (retVal == 1 || retVal == 2)
                    return "SUCCESS";
                else
                    return "ERROR UPDATING SHIPTO DEPT.";



            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error updating shipto department record (class | Method; wsCustomer | ARShipToDept_Update). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - update shipto department record (Class | Method; wsCustomer | ARShipToDept_Update). " + ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

        }
        #endregion

        #region UpdateCustomerContact
        [WebMethod]
        public string UpdateCustomerContact(string DB_NAME, string CONTACT_ID, string CUST_ID, string SHIPTO_ID, string FIRST_NAME, string LAST_NAME, string MIDDLE_NAME, string TITLE, string EMAIL_ADDRESS)
        {
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;

            conn = new SqlConnection(connString);
            conn.Open();

            string ShipToID = null;
            string MiddleName = null;
            string Title = null;
            string EmailAddress = null;

            if (SHIPTO_ID != "")
                ShipToID = SHIPTO_ID;

            if (MIDDLE_NAME != "")
                MiddleName = MIDDLE_NAME;

            if (TITLE != "")
                Title = TITLE;

            if (EMAIL_ADDRESS != "")
                EmailAddress = EMAIL_ADDRESS;


            try
            {
                cmd = new SqlCommand("p_CO_CONTACTS_Update", conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CONTACT_ID", SqlDbType.VarChar);
                cmd.Parameters["@CONTACT_ID"].Value = CONTACT_ID;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;
                cmd.Parameters.Add("@SHIP2_ID", SqlDbType.VarChar);
                cmd.Parameters["@SHIP2_ID"].Value = ShipToID;
                cmd.Parameters.Add("@FIRST_NAME", SqlDbType.VarChar);
                cmd.Parameters["@FIRST_NAME"].Value = FIRST_NAME;
                cmd.Parameters.Add("@LAST_NAME", SqlDbType.VarChar);
                cmd.Parameters["@LAST_NAME"].Value = LAST_NAME;
                cmd.Parameters.Add("@MIDDLE_NAME", SqlDbType.VarChar);
                cmd.Parameters["@MIDDLE_NAME"].Value = MiddleName;
                cmd.Parameters.Add("@TITLE", SqlDbType.VarChar);
                cmd.Parameters["@TITLE"].Value = Title;
                cmd.Parameters.Add("@EMAIL_ADDRESS", SqlDbType.VarChar);
                cmd.Parameters["@EMAIL_ADDRESS"].Value = EmailAddress;

                //executes the stored procedure and returns a value indicating the number of effected records which should be 1 when successful and 0 when not
                int retVal = cmd.ExecuteNonQuery();

                cmd.Dispose();

                if (retVal == 1 || retVal == 2)
                    return "SUCCESS";
                else
                    return "ERROR UPDATING CUSTOMER CONTACT.";



            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error updating customer contact record (class | Method; wsCustomer | UpdateCustomerContact). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - updating customer contact record (Class | Method; wsCustomer | UpdateCustomerContact). " + ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

        }
        #endregion

        #region UpdateCustomerContactPhone
        [WebMethod]
        public string UpdateCustomerContactPhone(string DB_NAME, string CONTACT_ID, string PHONE_ID, string PHONE_TYPE, string PHONE_NO, string EXTENSION_NO)
        {
            connString += "Database=" + DB_NAME + ";";
            SqlConnection conn;
            SqlCommand cmd;

            conn = new SqlConnection(connString);
            conn.Open();

            string ExtensionNo = null;

            if (EXTENSION_NO != "")
                ExtensionNo = EXTENSION_NO;


            try
            {
                cmd = new SqlCommand("p_CO_CONTACTS_PHONE_Update", conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CONTACT_ID", SqlDbType.VarChar);
                cmd.Parameters["@CONTACT_ID"].Value = CONTACT_ID;
                cmd.Parameters.Add("@PHONE_ID", SqlDbType.VarChar);
                cmd.Parameters["@PHONE_ID"].Value = PHONE_ID;
                cmd.Parameters.Add("@PHONE_TYPE", SqlDbType.VarChar);
                cmd.Parameters["@PHONE_TYPE"].Value = PHONE_TYPE;
                cmd.Parameters.Add("@PHONE_NO", SqlDbType.VarChar);
                cmd.Parameters["@PHONE_NO"].Value = PHONE_NO;
                cmd.Parameters.Add("@EXTENSION_NO", SqlDbType.VarChar);
                cmd.Parameters["@EXTENSION_NO"].Value = ExtensionNo;

                //executes the stored procedure and returns a value indicating the number of effected records which should be 1 when successful and 0 when not
                int retVal = cmd.ExecuteNonQuery();

                cmd.Dispose();

                if (retVal == 1 || retVal == 2)
                    return "SUCCESS";
                else
                    return "ERROR UPDATING CUSTOMER CONTACT PHONE NUMBER.";



            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error updating customer contact phone number (class | Method; wsCustomer | UpdateCustomerContactPhone). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - updating customer contact phone number (Class | Method; wsCustomer | UpdateCustomerContactPhone). " + ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

        }
        #endregion

        #region CalculateCustPrice
        private DataSet CalculateCustItemPrice(DataSet dsCustItemPriceData, Decimal QTY, string ITEM_NO)
        {
            CWSBLL.PriceCalculator myPriceCalculator = new CWSBLL.PriceCalculator();

            this.sErrorMethod = "CalculateCustPrice";

            //			bool QuantityPricingExists = false;
            //			bool ContractPricingExists = false;
            //			string CustomerNumber = string.Empty;
            //			string ShipToNumber = string.Empty;
            string WarehouseCode = string.Empty;
            string ItemNumber = string.Empty;
            string ItemID = string.Empty;
            string PriceSource = string.Empty;
            Decimal CustPrice = 0;
            Decimal ContractPrice = 0;
            Decimal QuantityPrice = 0;
            //Decimal ListPrice = 0;

            DataSet dsCustItemPrice = new DataSet("CustItemPrice");
            DataTable dtCustPrice = new DataTable("CustPrice");
            DataTable dtErrorMessage = new DataTable("ErrorMessage");

            this.sErrorStep = "Create Reference Data Tables";

            DataTable dtQtyPriceBrackets = dsCustItemPriceData.Tables[4];
            DataTable dtOEContractRecords = dsCustItemPriceData.Tables[5];
            DataTable dtOEContractRecordsQtyBrackets = dsCustItemPriceData.Tables[6];
            DataTable dtINQtyContractRecords = dsCustItemPriceData.Tables[7];

            dtCustPrice.Columns.Add("ITEM_NO", System.Type.GetType("System.String"));
            dtCustPrice.Columns.Add("ITEM_ID", System.Type.GetType("System.String"));
            dtCustPrice.Columns.Add("WHSE_CODE", System.Type.GetType("System.String"));
            //			dtCustPrice.Columns.Add("WHSE_ID", System.Type.GetType("System.String"));
            dtCustPrice.Columns.Add("PRICE_SOURCE", System.Type.GetType("System.String"));
            dtCustPrice.Columns.Add("CUST_PRICE", System.Type.GetType("System.Decimal"));
            dtCustPrice.Columns.Add("CONTRACT_PRICE", System.Type.GetType("System.Decimal"));
            dtCustPrice.Columns.Add("QUANTITY_PRICE", System.Type.GetType("System.Decimal"));
            dtCustPrice.Columns.Add("LIST_PRICE", System.Type.GetType("System.Decimal"));

            try
            {
                this.sErrorStep = "Assign the parameters passed back from the Item Master Data";

                //Assign the parameters passed back from the Item Master Data
                ItemID = dsCustItemPriceData.Tables[ItemMasterTable].Rows[ItemMasterTable]["item_id"].ToString();
                ItemNumber = dsCustItemPriceData.Tables[ItemMasterTable].Rows[ItemMasterTable]["item_no"].ToString();
                string ItemStatusID = dsCustItemPriceData.Tables[ItemMasterTable].Rows[ItemMasterTable]["status_id"].ToString();
                string OkToDiscount = dsCustItemPriceData.Tables[ItemMasterTable].Rows[ItemMasterTable]["ok_to_discount"].ToString();
                Decimal MaxDiscountPercentage = Decimal.Parse(dsCustItemPriceData.Tables[ItemMasterTable].Rows[0]["max_disc_percentage"].ToString());
                Decimal MinMarginPercentage = Decimal.Parse(dsCustItemPriceData.Tables[ItemMasterTable].Rows[0]["min_margin_percentage"].ToString());
                Decimal QtyConv = Decimal.Parse(dsCustItemPriceData.Tables[ItemMasterTable].Rows[0]["qty_conv"].ToString());
                myPriceCalculator.QtyOrdered = QTY;
                myPriceCalculator.Rounding = Int32.Parse(dsCustItemPriceData.Tables[ItemMasterTable].Rows[0]["rounding"].ToString());
                myPriceCalculator.NextCost = Decimal.Parse(dsCustItemPriceData.Tables[ItemMasterTable].Rows[0]["next_cost"].ToString());
                myPriceCalculator.StdCost = Decimal.Parse(dsCustItemPriceData.Tables[ItemMasterTable].Rows[0]["std_cost"].ToString());
                myPriceCalculator.ListPrice = Decimal.Parse(dsCustItemPriceData.Tables[ItemMasterTable].Rows[0]["list_price"].ToString());

                this.sErrorStep = "Assign the parameters passed back from Warehouse Item Master Data";
                //Assign the parameters passed back from Warehouse Item Master Data
                if (dsCustItemPriceData.Tables[WhseItemMasterTable].Rows.Count > 0)
                {
                    myPriceCalculator.ListPrice = Decimal.Parse(dsCustItemPriceData.Tables[WhseItemMasterTable].Rows[0]["list_price"].ToString());
                    myPriceCalculator.ActualCost = Decimal.Parse(dsCustItemPriceData.Tables[WhseItemMasterTable].Rows[0]["avg_cost"].ToString());
                }

                this.sErrorStep = "Assign the parameters passed back from the ship-to data";
                //Assign the parameters passed back from the ship-to data
                if (dsCustItemPriceData.Tables[ShipToTable].Rows.Count > 0)
                {
                    WarehouseCode = dsCustItemPriceData.Tables[ShipToTable].Rows[0]["whse_code"].ToString();
                    int ShipToDefaultQtyBracket = Int32.Parse(dsCustItemPriceData.Tables[ShipToTable].Rows[0]["qty_bracket"].ToString());
                    myPriceCalculator.IsBestPriceOK = dsCustItemPriceData.Tables[ShipToTable].Rows[0]["is_best_price_ok"].ToString();
                }

                this.sErrorStep = "Assign the reference price parameter passed back from the reference price data";
                //Assign the reference price parameter passed back from the reference price data
                if (dsCustItemPriceData.Tables[ItemReferencePriceTable].Rows.Count > 0)
                {
                    myPriceCalculator.ItemReferencePrice = Decimal.Parse(dsCustItemPriceData.Tables[ItemReferencePriceTable].Rows[0]["reference_price"].ToString());
                }

                this.sErrorStep = "Check for free good status";
                //First check to see if the item is a free good item.  If so, return zero as the price and forget trying to calculate anything else.
                if (String.Compare(ItemStatusID, "F") == 0)
                {
                    AddDataRowTo_dtCustPrice_DataTable(dtCustPrice, ItemID, ItemNumber, WarehouseCode, PriceSource, CustPrice, ContractPrice, QuantityPrice, myPriceCalculator.ListPrice);
                    return dsCustItemPrice;
                }

                //IsBestPriceOk: L - customer gets list price only
                //               N - customer gets the first price in the price hierarchy
                //               X - customer gets the best price including all contracts not just in priority ranking
                //               Y - customer gets the best price but use the contract priority to determine the contract price

                this.sErrorStep = "Check for Best Price = List ONLY and whether its Okay to discount";
                if (String.Compare(myPriceCalculator.IsBestPriceOK, "L") == 0 || String.Compare(OkToDiscount, "N") == 0)
                {
                    AddDataRowTo_dtCustPrice_DataTable(dtCustPrice, ItemID, ItemNumber, WarehouseCode, PriceSource, CustPrice, ContractPrice, QuantityPrice, myPriceCalculator.ListPrice);
                    return dsCustItemPrice;
                }

                this.sErrorStep = "Calculate Contract Price";
                //Calculate the Contract Price
                if (dtOEContractRecords.Rows.Count > 0)
                {
                    myPriceCalculator.CalculateContractPrice(dtOEContractRecords, dtOEContractRecordsQtyBrackets, dtINQtyContractRecords);
                }

                this.sErrorStep = "Calculate Quantity Discount Price";
                //Calculate the Quantity Discount Price
                if (dtQtyPriceBrackets.Rows.Count > 0)
                {
                    myPriceCalculator.CalculateQuantityPrice(dtQtyPriceBrackets);
                }

                this.sErrorStep = "Set Customer Price based on Best Parameter";
                if (String.Compare(myPriceCalculator.IsBestPriceOK, "N") == 0)
                {
                    this.sErrorStep = "Set Customer Price based on Best Price = N";

                    //Customer does not get best price so set the CustPrice equal to the first price in the pricing hierarchy
                    if (myPriceCalculator.ContractPrice > 0)
                    {
                        myPriceCalculator.CustPrice = myPriceCalculator.ContractPrice;
                        myPriceCalculator.PriceSource = "Contract";
                    }
                    else if (myPriceCalculator.QuantityPrice > 0)
                    {
                        myPriceCalculator.CustPrice = myPriceCalculator.QuantityPrice;
                        myPriceCalculator.PriceSource = "Qty Bracket";
                    }
                    else
                    {
                        myPriceCalculator.CustPrice = myPriceCalculator.ListPrice;
                        myPriceCalculator.PriceSource = "List Price";
                    }
                }
                else if (String.Compare(myPriceCalculator.IsBestPriceOK, "Y") == 0 || String.Compare(myPriceCalculator.IsBestPriceOK, "X") == 0)
                {
                    this.sErrorStep = "Set Customer Price when Best Price = Y or X";

                    if (myPriceCalculator.ContractPrice > 0)
                    {
                        myPriceCalculator.CustPrice = myPriceCalculator.ContractPrice;
                        myPriceCalculator.PriceSource = "Contract";
                    }
                    if (myPriceCalculator.QuantityPrice > 0 && myPriceCalculator.QuantityPrice < myPriceCalculator.ContractPrice)
                    {
                        myPriceCalculator.CustPrice = myPriceCalculator.QuantityPrice;
                        myPriceCalculator.PriceSource = "Qty Bracket";
                    }
                    if (myPriceCalculator.ListPrice > 0 && myPriceCalculator.ListPrice < CustPrice)
                    {
                        myPriceCalculator.CustPrice = myPriceCalculator.ListPrice;
                        myPriceCalculator.PriceSource = "List Price";
                    }
                }

                this.sErrorStep = "Add row to dtCustPrice data table";
                AddDataRowTo_dtCustPrice_DataTable(dtCustPrice, ItemID, ItemNumber, WarehouseCode, myPriceCalculator.PriceSource, myPriceCalculator.CustPrice, myPriceCalculator.ContractPrice, myPriceCalculator.QuantityPrice, myPriceCalculator.ListPrice);
                dsCustItemPrice.Tables.Add(dtCustPrice);
                return dsCustItemPrice;
            }
            catch (Exception ex)
            {
                CWSBLL.LogError.pLogError(ex, CWSBLL.LogError.FilePath, sErrorClass, sErrorMethod, sErrorStep, true, dsCustItemPrice);
                return dsCustItemPrice;
            }
        }
        #endregion

        #region CreatePriceRow
        private void AddDataRowTo_dtCustPrice_DataTable(DataTable dtCustPrice, string ItemID, string ItemNumber, string WarehouseCode, string PriceSource, Decimal CustomerPrice, Decimal ContractPrice, Decimal QuantityPrice, Decimal ListPrice)
        {
            //Create a new data row with the same schema as the dtCustPrice data table
            DataRow drCustPrice = dtCustPrice.NewRow();

            //Assign the values to each column within the new data row
            drCustPrice["ITEM_ID"] = ItemID;
            drCustPrice["ITEM_NO"] = ItemNumber;
            drCustPrice["WHSE_CODE"] = WarehouseCode;
            drCustPrice["PRICE_SOURCE"] = PriceSource;
            drCustPrice["CUST_PRICE"] = CustomerPrice;
            drCustPrice["CONTRACT_PRICE"] = ContractPrice;
            drCustPrice["QUANTITY_PRICE"] = QuantityPrice;
            drCustPrice["LIST_PRICE"] = ListPrice;

            //Add the new row to the table
            dtCustPrice.Rows.Add(drCustPrice);

        }
        #endregion

        #region COContact_Insert
        private string COContact_Insert(string DB_NAME, string CUST_ID, string ShipToID, string FIRST_NAME, string LAST_NAME, string MiddleName, string Title, string TelNo, string Extension, string FaxNo, string EmailAddress)
        {
            SqlConnection conn;
            SqlCommand cmd;

            string ContactID = "";

            try
            {
                connString += "Database=" + DB_NAME + ";";
                conn = new SqlConnection(connString);
                conn.Open();

                cmd = new SqlCommand("p_CO_CONTACTS_Insert", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CUST_ID", SqlDbType.VarChar);
                cmd.Parameters["@CUST_ID"].Value = CUST_ID;
                cmd.Parameters.Add("@SHIP2_ID", SqlDbType.VarChar);
                cmd.Parameters["@SHIP2_ID"].Value = ShipToID;
                cmd.Parameters.Add("@FIRST_NAME", SqlDbType.VarChar);
                cmd.Parameters["@FIRST_NAME"].Value = FIRST_NAME;
                cmd.Parameters.Add("@LAST_NAME", SqlDbType.VarChar);
                cmd.Parameters["@LAST_NAME"].Value = LAST_NAME;
                cmd.Parameters.Add("@MIDDLE_NAME", SqlDbType.VarChar);
                cmd.Parameters["@MIDDLE_NAME"].Value = MiddleName;
                cmd.Parameters.Add("@TITLE", SqlDbType.VarChar);
                cmd.Parameters["@TITLE"].Value = Title;
                cmd.Parameters.Add("@TEL_NO", SqlDbType.VarChar);
                cmd.Parameters["@TEL_NO"].Value = TelNo;
                cmd.Parameters.Add("@FAX_NO", SqlDbType.VarChar);
                cmd.Parameters["@FAX_NO"].Value = FaxNo;
                cmd.Parameters.Add("@EXTENSION_NO", SqlDbType.VarChar);
                cmd.Parameters["@EXTENSION_NO"].Value = Extension;
                cmd.Parameters.Add("@EMAIL_ADDRESS", SqlDbType.VarChar);
                cmd.Parameters["@EMAIL_ADDRESS"].Value = EmailAddress;
                cmd.Parameters.Add("@CONTACT_ID", SqlDbType.UniqueIdentifier);
                cmd.Parameters["@CONTACT_ID"].Direction = ParameterDirection.Output;

                int i = cmd.ExecuteNonQuery();

                if (i != null && i > 0)
                    ContactID = cmd.Parameters["@CONTACT_ID"].Value.ToString();

                conn.Close();
                conn.Dispose();
                cmd.Dispose();

                return ContactID;
            }
            catch (SqlException sqlex)
            {
                throw new Exception("Error 100 - SQL error creating customer contact record (class | Method; wsCustomer | COContact_Insert). " + sqlex.GetBaseException());
            }
            catch (Exception ex)
            {
                throw new Exception("Error 101 - creating customer contact record (Class | Method; wsCustomer | COContact_Insert). " + ex.Message);
            }
            finally
            {

            }
        }
        #endregion
    }
}
