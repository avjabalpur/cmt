﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CenGen_Web_Services
{
    public class Item
    {
        private Guid mItemId;
        private string mItemNumber;
        private string mDescription;
        private decimal mListPrice;
        private decimal mStdCost;
        private decimal mLastCost;
        private decimal mUnitWeight;
        private decimal mUnitVolume;
        private DateTime mDateAdded;
        private DateTime mLastChangeDate;
        private string mSkuUM;
        private string mQtyUM;
        private decimal mQtyConv;
        private int mWarrantyMonths;
        private string mIsTaxable;
        private string mGroupCode;
        private string mErrorMessage;
        private List<ItemExtendedDescription> mExtendedDescription = new List<ItemExtendedDescription>();
        private List<WhseItem> mWarehouseItem = new List<WhseItem>();


        /// <summary>
        /// The item's uniqueidentifier 
        /// </summary>
        public Guid ItemId
        {
            get { return mItemId; }
            set { mItemId = value; }
        }
        /// <summary>
        /// The item number
        /// </summary>
        public string ItemNumber
        {
            get { return mItemNumber; }
            set { mItemNumber = value; }
        }
        /// <summary>
        /// The item simple description 
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set { mDescription = value; }
        }
        /// <summary>
        /// The list price of an Sku UM of this item
        /// </summary>
        public decimal ListPrice
        {
            get { return mListPrice; }
            set { mListPrice = value; }
        }
        /// <summary>
        /// The standard cost of the product which typically includes an upcharge for carrying
        /// </summary>
        public decimal StdCost
        {
            get { return mStdCost; }
            set { mStdCost = value; }
        }
        /// <summary>
        /// The last cost that was paid to a vendor to purchase this item
        /// </summary>
        public decimal LastCost
        {
            get { return mLastCost; }
            set { mLastCost = value; }
        }
        /// <summary>
        /// The Sku UM weight of the product in pounds
        /// </summary>
        public decimal UnitWeight
        {
            get { return mUnitWeight; }
            set { mUnitWeight = value; }
        }
        /// <summary>
        /// The Sku UM Volume
        /// </summary>
        public decimal UnitVolume
        {
            get { return mUnitVolume; }
            set { mUnitVolume = value; }
        }
        /// <summary>
        /// Identifies the date the item was created in the ERP system
        /// </summary>
        public DateTime DateAdded
        {
            get { return mDateAdded; }
            set { mDateAdded = value; }
        }
        /// <summary>
        /// The last time the item data changed
        /// </summary>
        public DateTime LastChangeDate
        {
            get { return mLastChangeDate; }
            set { mLastChangeDate = value; }
        }
        /// <summary>
        /// Stock Keeping Unit of Measure (i.e. EA - each, BX - Box, CA - Case...)
        /// </summary>
        public string SkuUM
        {
            get { return mSkuUM; }
            set { mSkuUM = value; }
        }
        /// <summary>
        /// Most common Unit Of Measure (i.e. EA - each, BX - Box, CA - Case...)
        /// </summary>
        public string QtyUM
        {
            get { return mQtyUM; }
            set { mQtyUM = value; }
        }
        /// <summary>
        /// The conversion factor for x number of Sku Units of Measure to equal 1 Quantity Unit of Measure
        /// </summary>
        public decimal QtyConv
        {
            get { return mQtyConv; }
            set { mQtyConv = value; }
        }
        /// <summary>
        /// The number of months the product warranty covers
        /// </summary>
        public int WarrantyMonths
        {
            get { return mWarrantyMonths; }
            set { mWarrantyMonths = value; }
        }
        /// <summary>
        /// Determines whether the item is taxable or not
        /// </summary>
        public string IsTaxable
        {
            get { return mIsTaxable; }
            set { mIsTaxable = value; }
        }
        /// <summary>
        /// Product Group Code assigned to the item number
        /// </summary>
        public string GroupCode
        {
            get { return mGroupCode; }
            set { mGroupCode = value; }
        }
        /// <summary>
        /// Provides a means in which to communicate an error message to the calling application
        /// </summary>
        public string ErrorMessage
        {
            get { return mErrorMessage; }
            set { mErrorMessage = value; }
        }
        /// <summary>
        /// Provides the collection of extended description records
        /// </summary>
        public List<ItemExtendedDescription> ExtendedDescription
        {
            get { return mExtendedDescription; }
            set { mExtendedDescription = value; }
        }
        /// <summary>
        /// Provides the collection of warehouse item records. The warehouse item includes the quantity on hand, commit qantities, quantity being reworked,
        /// quantity damaged, quantity available for sell, average cost, average lead time days, quantity on purchase orders, and quantity being transferred 
        /// from another warehouse.
        /// </summary>
        public List<WhseItem> WarehouseItem
        {
            get { return mWarehouseItem; }
            set { mWarehouseItem = value; }
        }
    }
}