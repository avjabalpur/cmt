﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CenGen_Web_Services
{
    public class WhseItem
    {
        private Guid mItemId;
        private Guid mWhseId;
        private string mWhseCode;
        private decimal mQtyOnHand;
        private decimal mCommitTodayQty;
        private decimal mCommitLaterQty;
        private decimal mQtyRework;
        private decimal mQtyDamaged;
        private decimal mQtyAvailable;
        private decimal mAvgCost;
        private decimal mAvgLeadTimeDays;
        private decimal mOnOrderPO;
        private decimal mOnOrderTransfers;


        /// <summary>
        /// The item's uniqueidentifier (foreign key to the Item Master)
        /// </summary>
        public Guid ItemId
        {
            get { return mItemId; }
            set { mItemId = value; }
        }
        /// <summary>
        /// The warehouse's uniqueidentifier (foreign key to the Warehouse Master)
        /// </summary>
        public Guid WhseId
        {
            get { return mWhseId; }
            set { mWhseId = value; }
        }
        /// <summary>
        /// The warehouse's unique code
        /// </summary>
        public string WhseCode
        {
            get { return mWhseCode; }
            set { mWhseCode = value; }
        }
        /// <summary>
        /// The item's current quantity on hand in the specified warehouse
        /// </summary>
        public decimal QtyOnHand
        {
            get { return mQtyOnHand; }
            set { mQtyOnHand = value; }
        }
        /// <summary>
        /// The item's current quantity committed to orders that scheduled to ship today in the specified warehouse
        /// </summary>
        public decimal CommitTodayQty
        {
            get { return mCommitTodayQty; }
            set { mCommitTodayQty = value; }
        }
        /// <summary>
        /// The item's current quantity committed to orders that scheduled to ship after today's date in the specified warehouse
        /// </summary>
        public decimal CommitLaterQty
        {
            get { return mCommitLaterQty; }
            set { mCommitLaterQty = value; }
        }
        /// <summary>
        /// The item's current quantity committed to rework purchase orders in the specified warehouse
        /// </summary>
        public decimal QtyRework
        {
            get { return mQtyRework; }
            set { mQtyRework = value; }
        }
        /// <summary>
        /// The item's current quantity that is considered damaged in the specified warehouse
        /// </summary>
        public decimal QtyDamaged
        {
            get { return mQtyDamaged; }
            set { mQtyDamaged = value; }
        }
        /// <summary>
        /// The item's current quantity available for sell in the specified warehouse. It is calculated by the formula QtyOnHand - CommitTodayQty - 
        /// CommitLaterQty - QtyRework - QtyDamaged.
        /// </summary>
        public decimal QtyAvailable
        {
            get { return mQtyOnHand - mCommitTodayQty - mCommitLaterQty - mQtyDamaged - mQtyRework; }
            set { mQtyAvailable = value; }
        }
        /// <summary>
        /// The item's current average cost in the specified warehouse
        /// </summary>
        public decimal AvgCost
        {
            get { return mAvgCost; }
            set { mAvgCost = value; }
        }
        /// <summary>
        /// The item's average leadtime from the time a purchase order is released until it is recieved into the specified warehouse
        /// </summary>
        public decimal AvgLeadTimeDays
        {
            get { return mAvgLeadTimeDays; }
            set { mAvgLeadTimeDays = value; }
        }
        /// <summary>
        /// The item's quantity on released purchase orders for the specified warehouse
        /// </summary>
        public decimal OnOrderPO
        {
            get { return mOnOrderPO; }
            set { mOnOrderPO = value; }
        }
        /// <summary>
        /// The item's quantity on transfer from another warehouse for the specified warehouse
        /// </summary>
        public decimal OnOrderTransfers
        {
            get { return mOnOrderTransfers; }
            set { mOnOrderTransfers = value; }
        }
    }
}