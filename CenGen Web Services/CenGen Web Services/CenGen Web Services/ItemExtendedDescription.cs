﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CenGen_Web_Services
{
    public class ItemExtendedDescription
    {
        private Guid mXdescId;
        private Guid mItemId;
        private int mSequenceNumber;
        private string mLanguageCode;
        private string mDescription;
        private string mDescriptionPlainText;

        /// <summary>
        /// The item extened description primary key and uniqueidentifier for this record
        /// </summary>
        public Guid XdescId
        {
            get { return mXdescId; }
            set { mXdescId = value; }
        }
        /// <summary>
        /// The item's uniqueidentifier 
        /// </summary>
        public Guid ItemId
        {
            get { return mItemId; }
            set { mItemId = value; }
        }
        /// <summary>
        /// The sequence in which the descriptions should be displayed to the user based on language code
        /// </summary>
        public int SequenceNumber
        {
            get { return mSequenceNumber; }
            set { mSequenceNumber = value; }
        }
        /// <summary>
        /// Identifies which language the description is written in and for
        /// </summary>
        public string LanguageCode
        {
            get { return mLanguageCode; }
            set { mLanguageCode = value; }
        }
        /// <summary>
        /// The rich text version of the description
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set { mDescription = value; }
        }
        /// <summary>
        /// The description with the rich text formatting characters removed
        /// </summary>
        public string DescriptionPlainText
        {
            get { return mDescriptionPlainText; }
            set { mDescriptionPlainText = value; }
        }
    }
}