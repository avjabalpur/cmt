1. Customer File (Customer_Import.csv) -
----Check method InsertNewCustomer in the web service project and implement it in webapi.( Validate the fields)

Uploading the customers can be done manually or can be run on a daily cronjob. The daily process will upload all customers into the system. 
Currently each customer is define with the Master User (i.e. - name, surname) but this can be changed.
Since all your new customers (including the B2B customers) will be opened on the ERP system, the flow should be as follows:
(i) Order upload to the ERP (system will export all new customer data)
(ii) Create a new customer if customer_id field (in the Order.xml file) does not exist.
(iii) The new customer_id will be attached to the new B2B user for the next customer import into the system.
 
2. Address File (Address_Import.cv) -
---- Check method InsertNewShipTo in the web service and implement it in webapi.(Validate the fields)
Uploading the customer addresses can be done manually or can be run on a daily cronjob.
This assumes that all addresses will be updated first on the ERP system.
If a new address is opened during an order, we can do through a similar process to opening a new customer as defined above.
 
3. Order Integration (xml_Order_Export.xml) -
----Check SalesOrderInsert.rtf for this.
Upon the creation of a new order, the system will automatically generate an xml file (see the xml_Order_Export.xml as an example).
The upload of the file to a common FTP/SFTP server can be done. We can also send the xml file by a web-service.
The file contains all the data we the order has in the system, including: order lines, user details, billing and shipping address, etc..
 
4. Price Integration (Price.xml) - 
--- Check p_cds_oe_pricing_engine_wsAPI for this
Please see an example of an API request and response. This is a web-service (Rest) API.
 
5. Stock Integration (Stock.xml) - 
--- Check p_cds_get_whse_qty_available for this
Please see an example of an API request and response. This is a web-service (Rest) API.